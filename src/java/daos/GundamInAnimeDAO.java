/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.GundamInAnimeDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HiruK
 */
public class GundamInAnimeDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    List<GundamInAnimeDTO> list = new ArrayList<>();
    int pagingCount = 0;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public int getPagingCount() {
        return pagingCount;
    }

    public void setPagingCount(int pagingCount) {
        this.pagingCount = pagingCount;
    }

    public List<GundamInAnimeDTO> getList() {
        return list;
    }

    public void setList(List<GundamInAnimeDTO> list) {
        this.list = list;
    }

    /**
     * Lấy những gundam dựa trên anime id
     *
     * @param id
     */
    public void getGundam(String id) throws SQLException, ClassNotFoundException {
        GundamInAnimeDTO dto;
        int count = 0;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT top 10 id\n"
                    + "      ,name\n"
                    + "      ,avatar\n"
                    + "	  ,scale\n"
                    + "  FROM [dbo].[gundam_in_anime]  \n"
                    + "  where animeId=?";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            rs = preStm.executeQuery();
            while (rs.next()) {
                count++;
                dto = new GundamInAnimeDTO();
                dto.setId(rs.getInt("id"));
                dto.setAvatar(rs.getString("avatar"));
                dto.setScale(rs.getString("scale"));
                dto.setName(rs.getString("name"));
                list.add(dto);

            }

            System.out.println("count= " + list.size());
        } finally {
            closeConnection();
        }
    }

    /**
     * Hàm này sẽ tăng thêm cột click mỗi khi gundam in anime đc click
     *
     * @param id
     */
    public void saveClick(int id) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "update gundam_in_anime set click=? where id=? ";
            preStm = conn.prepareStatement(sql);
            preStm.setInt(1, id);
        } finally {
            closeConnection();
        }
    }

    /**
     * Hàm thực hiện phân trang
     *
     * @param id
     * @param start
     * @param total
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void getGundaminAnimeWithPaging(String id, int start, int total) throws SQLException, ClassNotFoundException {
        GundamInAnimeDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT id,name,scale,avatar, count(*) over() as countt FROM gundam_in_anime where gundam_in_anime.animeId=? ORDER BY gundam_in_anime.name OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            preStm.setInt(2, start);
            preStm.setInt(3, total);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new GundamInAnimeDTO();
                dto.setId(rs.getInt("id"));
                dto.setAvatar(rs.getString("avatar"));
                dto.setScale(rs.getString("scale"));
                dto.setName(rs.getString("name"));
                setPagingCount(rs.getInt("countt"));
                list.add(dto);

            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Cũng giống như hàm trên nhưng sắp xếp theo lượt click
     *
     * @param id
     * @param start
     * @param total
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void getGundaminAnimeWithPagingPrioritize(String id, int start, int total) throws SQLException, ClassNotFoundException {
        GundamInAnimeDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = " SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[scale]\n"
                    + "      ,[avatar]\n"
                    + "      ,[animeId]\n"
                    + "      ,[shortkey]\n"
                    + "	  ,count(*) over() as totalrecord\n"
                    + "  FROM (  select *,count(*) over() as totalrecord from gundam_in_anime where gundam_in_anime.animeId=?) as c\n"
                    + "  LEFT JOIN (SELECT click_info.gundaminanimeid, count(*) as numOf\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.gundaminanimeid is not null\n"
                    + "  group by click_info.gundaminanimeid having count(*)>1 ) as g on c.id = g.gundaminanimeid\n"
                    + "  order by g.numOf DESC  \n"
                    + "  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY ";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            preStm.setInt(2, start);
            preStm.setInt(3, total);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new GundamInAnimeDTO();
                dto.setId(rs.getInt("id"));
                dto.setAvatar(rs.getString("avatar"));
                dto.setScale(rs.getString("scale"));
                dto.setName(rs.getString("name"));
                dto.setShortKey(rs.getString("shortkey"));
                System.out.println("short key= "+dto.getShortKey());
                setPagingCount(rs.getInt("totalrecord"));
                list.add(dto);

            }
        } finally {
            closeConnection();
        }
    }
 
    /**
     * Trả về 1 list gundam có trong anime
     * @param list
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void getAllGundamInAnime(List<GundamInAnimeDTO> list) throws SQLException, ClassNotFoundException {
        GundamInAnimeDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[shortkey]\n"
                    + "  FROM [dbo].[gundam_in_anime]";
            preStm=conn.prepareStatement(sql);
            rs=preStm.executeQuery();
            while(rs.next()){
                 dto=new GundamInAnimeDTO();
                 dto.setId(rs.getInt("id"));
                 dto.setName(rs.getString("name"));
                 dto.setShortKey(rs.getString("shortkey"));
                 list.add(dto);
            }
        } finally {
            closeConnection();
        }
    }
    
        /**
         * Cập nhật giá trị shortkey sau khi có đc từ việc xử lí
         * @param shorkey
         * @param gundamID
         * @throws SQLException
         * @throws ClassNotFoundException 
         */
        public void insertShortkey(String shorkey, int gundamID) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
//            String sql = "insert into short_key(shortkey)values(?)";//Đưa shorkey vào bảng short_key
            String gundamInShopUpdateShortKey = "update gundam_in_anime set shortkey=? where id=?";//Tiện thể update cái short key của bảng gundam luôn
            preStm = conn.prepareStatement(gundamInShopUpdateShortKey);
            preStm.setString(1, shorkey);
            preStm.setInt(2, gundamID);
            int result = preStm.executeUpdate();
            if (result > 0) {
                System.out.println("Insert shortky ok");
            }
        } finally {
            closeConnection();
        }
    }
}
