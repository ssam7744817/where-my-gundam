package listener;

import crawler.jaxb.JAXBMarshalling;
import daos.AnimeDAO;
import daos.GundamDAO;
import daos.GundamModelTypeDAO;
import daos.GundamSlideShowDAO;
import daos.TopIdDAO;
import dtos.GundamModelTypeDTO;
import dtos.TopIdDTOList;
import generate.gundams.Gundam;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Web application lifecycle listener.
 *
 * @author HiruK
 */
public class MyServletListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        JAXBMarshalling marshalling = new JAXBMarshalling();
        GundamDAO dao = new GundamDAO();
        TopIdDAO daoTop = new TopIdDAO();
        GundamSlideShowDAO imageDao = new GundamSlideShowDAO();
        try {
            //Tạo ra 1 xml phục vụ cho việc search
            dao.getTop10v2();
            imageDao.getImageName();//Lấy ra tên của đống ảnh
            System.out.println("image list= " + imageDao.getList().size() + "name= " + imageDao.getList().get(0).getName());
            context.setAttribute("IMAGE", imageDao.getList());

         /**
          * Lấy ra 5 mô hình gundam được click nhiều nhất để show lên trang home của project
          * Còn file report thì fung hàm topDao.getTheResult() xuất hết cho người dùng
          * TẠM THỜI KO DÙNG NỮA
          */
//            daoTop.getTop5Result();
//            System.out.println("top 5 have size= "+daoTop.getTopFive().getTopId().size());
//            context.setAttribute("TESTTOP", daoTop.getTopFive().getTopId());

            /**
             * lưu nó vào trong context dưới dang string, sử dụng cái này để lỡ
             * khi xóa hết dữ liệu rồi thì thay vì show top id, ta sẽ show 1 cái
             * slide show để đến khi nào có đủ dữ liệu cho top id thì ta lại
             * show lên
             */
//            String list = marshalling.marshallerToTransferToStringByTransform(daoTop.getList(), TopIdDTOList.class);
            context.setAttribute("TOP", "");


            //Ta lấy cái type list để làm bộ lọc ở trang search
            GundamModelTypeDAO typeDao = new GundamModelTypeDAO();
            typeDao.getModelTypeName();
            List<GundamModelTypeDTO> typeList = typeDao.getList();
            context.setAttribute("TYPELIST", typeList);
            System.out.println("list type= " + typeList.size());
            
            //Lấy tên các anime và hình ảnh tương đương với nó
            AnimeDAO animeDao=new AnimeDAO();
            animeDao.getAll();
            System.out.println("anime list= "+animeDao.getAnimes().size());
            context.setAttribute("ANIMES", animeDao.getAnimes());
            

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MyServletListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MyServletListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
