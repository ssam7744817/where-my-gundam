/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.ClickInfoDTO;
import generate.report.Report;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javaclass.TypeUtil;

/**
 *
 * @author HiruK
 */
public class ClickInfoDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    HashMap<ClickInfoDTO, Integer> hostList = new HashMap<>();
    HashMap<ClickInfoDTO, Integer> typeList = new HashMap<>();

    Report report = new Report();

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public HashMap<ClickInfoDTO, Integer> getHostList() {
        return hostList;
    }

    public void setHostList(HashMap<ClickInfoDTO, Integer> hostList) {
        this.hostList = hostList;
    }

    public HashMap<ClickInfoDTO, Integer> getTypeList() {
        return typeList;
    }

    public void setTypeList(HashMap<ClickInfoDTO, Integer> typeList) {
        this.typeList = typeList;
    }

    /**
     * Hàm này sẽ lưu lại id đc click, để khi cần thì sẽ đếm số lần xuất hiện,DA
     * THANH CONG, hàm này trả về 1 string thông báo là click của anime hay
     * click của shop
     *
     * @param gundaminanime
     * @param gundamid
     */
    public void saveClick(String id, String gundamType, String gundamHost) throws SQLException, ClassNotFoundException {
        System.out.println("id= " + id);

        try {
            conn = MyConnection.getConnection();
            String sql = "";
            if (id.matches("[0-9]+") == true) {
                sql = "insert into click_info(gundaminanimeid) values(?)";
                preStm = conn.prepareStatement(sql);
                preStm.setInt(1, Integer.valueOf(id));
                preStm.executeUpdate();
                System.out.println("insert in gundam anime ok");
            } else {
                sql = "insert into click_info(gundamid,gundaminanimeid,type,host) values(?,NULL,?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, id);
                preStm.setInt(2, TypeUtil.convertStringTypeToNumber(gundamType));
                preStm.setInt(3, Integer.valueOf(gundamHost));
                preStm.executeUpdate();
                System.out.println("insert gundam in shop ok");
            }

        } finally {
            closeConnection();
        }
    }

    /**
     * Lấy ra lượt thống kê coi số lượt click trên từng cửa hàng
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void getGundamHostStatistics() throws SQLException, ClassNotFoundException {
        ClickInfoDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT click_info.host, count(click_info.host) as hostcount\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.host is not null\n"
                    + "  group by click_info.host\n"
                    + "  order by hostcount desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new ClickInfoDTO();
                dto.setHost(rs.getInt("host"));
                int number = rs.getInt("hostcount");
                hostList.put(dto, number);
            }
            System.out.println("click on host= " + hostList.size());
        } finally {
            closeConnection();
        }
    }

    /**
     * Lấy ra lượt click trên từng loại type
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void getGundamTypeStatistics() throws SQLException, ClassNotFoundException {
        ClickInfoDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT click_info.type, count(click_info.type) as typecount\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.type is not null\n"
                    + "  group by click_info.type\n"
                    + "  order by typecount desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new ClickInfoDTO();
                dto.setType(rs.getInt("type"));
                int number = rs.getInt("typecount");
                typeList.put(dto, number);
            }

        } finally {
            closeConnection();
        }
    }

}
