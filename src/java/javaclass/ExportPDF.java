/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.xmlgraphics.util.MimeConstants;
import org.xml.sax.SAXException;

/**
 *
 * @author HiruK
 */
public class ExportPDF{
    /**
     * parameter thứ 3 là đường dẫn file fdf
     * @param fo
     * @param response
     * @param filename
     * @throws TransformerException 
     */
   public void exportToPDF(HttpServletResponse response,String filename) throws TransformerException {
        try {
            OutputStream out = response.getOutputStream();
            FopFactory ff = FopFactory.newInstance();
            ff.setUserConfig(filename);
            FOUserAgent fua = ff.newFOUserAgent();
            Fop fop = ff.newFop(MimeConstants.MIME_PDF, fua, out);

            TransformerFactory tff = TransformerFactory.newInstance();
            Transformer trans = tff.newTransformer();
            File fo=new File(filename);
            StreamSource source = new StreamSource(fo);
            SAXResult result = new SAXResult(fop.getDefaultHandler());
            trans.transform(source, result);
        } catch (SAXException | IOException ex) {
            ex.printStackTrace();
        }
    }
}
