<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : newstylesheet.xsl
    Created on : April 1, 2020, 12:44 PM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:b="http://xml.netbeans.org/schema/books"
                xmlns="http://xml.netbeans.org/schema/books"
                version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <xsl:element name="books">
            <xsl:variable name="getName" select="//name/text()"/>
            <xsl:variable name="getAuthor" select="//author/text()"/>
            <xsl:if test="$getName = 'Harry Porter' ">
                <xsl:element name="book">
                    <xsl:element name="name">
                        <xsl:value-of select="books/book/name/text()"/>
                    </xsl:element>
                    <xsl:element name="author">
                        <xsl:value-of select="$getAuthor"/>
                    </xsl:element>
                </xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>

<!--            <xsl:for-each select="books/book">
    <xsl:call-template name="getInfo">
        <xsl:with-param name="getName" select="name/text()"/>
        <xsl:with-param name="getAuthor" select="author/text()"/>
    </xsl:call-template>
</xsl:for-each>-->
<!--    <xsl:template name="getInfo">
    <xsl:param name="getName"/>
    <xsl:param name="getAuthor"/>
    <xsl:element name="book">
        <xsl:element name="name">
            <xsl:value-of select="$getName"/>
        </xsl:element>
        <xsl:element name="author">
            <xsl:value-of select="$getAuthor"/>
        </xsl:element>
    </xsl:element>
</xsl:template>-->
