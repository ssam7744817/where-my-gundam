<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : gundam_k2t.xsl
    Created on : February 16, 2020, 9:46 AM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:t="http://xml.netbeans.org/schema/gundams"
                xmlns="http://xml.netbeans.org/schema/gundams">
    <xsl:output method="xml" omit-xml-declaration="no" indent="yes"/> 

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="t:gundams">
        <xsl:element name="gundams">
            <xsl:variable name="listDoc" select="document(t:link//@hr)"/>
            <xsl:variable name="host" select="t:link//@host"/>
            <!--Load vào từng trang gundam-->
            <xsl:variable name="gundamBandai" select="$listDoc//div[@class='menu_doc']//li[@class='li-sub']"/>
            <xsl:for-each select="$gundamBandai">
                <xsl:choose>
                    <!--Lấy phần link đầu tiên-->
                    <xsl:when test="a//@title='SD'">
                        <xsl:variable name="link" select="document(concat($host,a//@href))"/>
                        <xsl:call-template name="getLink">
                            <xsl:with-param name="type" select="'SD'"/>
                            <xsl:with-param name="host" select="$host"/>
                            <xsl:with-param name="link" select="$link"/>
                        </xsl:call-template>
                        
                        <!--Sau khi đã lấy ddc dữ liệu ở link thứ nhất thì bắt đầu lấy phần dữ liệu còn lại ở pagination-->
                        <xsl:for-each select="$link//div[@class='pagination']//ul[@class='pages']//li//a">
                            <xsl:if test=".//text()!='1' and .//text()!='First' and .//text()!='End'">
                                <xsl:variable name="page" select="@href"/>
                                <xsl:call-template name="getLink">
                                    <xsl:with-param name="type" select="'SD'"/>
                                    <xsl:with-param name="host" select="$host"/>
                                    <xsl:with-param name="link" select="document($page)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                        
                    </xsl:when>
                    <xsl:when test="a//@title='HG'">
                        <xsl:variable name="link" select="document(concat($host,a//@href))"/>
                        <xsl:call-template name="getLink">
                            <xsl:with-param name="type" select="'HG'"/>
                            <xsl:with-param name="host" select="$host"/>
                            <xsl:with-param name="link" select="$link"/>
                        </xsl:call-template>
                        <xsl:for-each select="$link//div[@class='pagination']//ul[@class='pages']//li//a">
                            <xsl:if test=".//text()!='1' and .//text()!='First' and .//text()!='End'">
                                <xsl:variable name="page" select="@href"/>
                                <xsl:call-template name="getLink">
                                    <xsl:with-param name="type" select="'HG'"/>
                                    <xsl:with-param name="host" select="$host"/>
                                    <xsl:with-param name="link" select="document($page)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="a//@title='RG'">
                        <xsl:variable name="link" select="document(concat($host,a//@href))"/>
                        <xsl:call-template name="getLink">
                            <xsl:with-param name="type" select="'RG'"/>
                            <xsl:with-param name="host" select="$host"/>
                            <xsl:with-param name="link" select="$link"/>
                        </xsl:call-template>
                        <xsl:for-each select="$link//div[@class='pagination']//ul[@class='pages']//li//a">
                            <xsl:if test=".//text()!='1' and .//text()!='First' and .//text()!='End'">
                                <xsl:variable name="page" select="@href"/>
                                <xsl:call-template name="getLink">
                                    <xsl:with-param name="type" select="'RG'"/>
                                    <xsl:with-param name="host" select="$host"/>
                                    <xsl:with-param name="link" select="document($page)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="a//@title='MG'">
                        <xsl:variable name="link" select="document(concat($host,a//@href))"/>
                        <xsl:call-template name="getLink">
                            <xsl:with-param name="type" select="'MG'"/>
                            <xsl:with-param name="host" select="$host"/>
                            <xsl:with-param name="link" select="$link"/>
                        </xsl:call-template>
                        <xsl:for-each select="$link//div[@class='pagination']//ul[@class='pages']//li//a">
                            <xsl:if test=".//text()!='1' and .//text()!='First' and .//text()!='End'">
                                <xsl:variable name="page" select="@href"/>
                                <xsl:call-template name="getLink">
                                    <xsl:with-param name="type" select="'MG'"/>
                                    <xsl:with-param name="host" select="$host"/>
                                    <xsl:with-param name="link" select="document($page)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="a//@title='PG'">
                        <xsl:variable name="link" select="document(concat($host,a//@href))"/>
                        <xsl:call-template name="getLink">
                            <xsl:with-param name="type" select="'PG'"/>
                            <xsl:with-param name="host" select="$host"/>
                            <xsl:with-param name="link" select="$link"/>
                        </xsl:call-template>
                        <xsl:for-each select="$link//div[@class='pagination']//ul[@class='pages']//li//a">
                            <xsl:if test=".//text()!='1' and .//text()!='First' and .//text()!='End'">
                                <xsl:variable name="page" select="@href"/>
                                <xsl:call-template name="getLink">
                                    <xsl:with-param name="type" select="'PG'"/>
                                    <xsl:with-param name="host" select="$host"/>
                                    <xsl:with-param name="link" select="document($page)"/>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>            
        </xsl:element>
    </xsl:template>
    
    <!--Lấy avatar loại lớn,KO DÙNG NỮA-->
    <xsl:template name="getBigAvatar">
        <xsl:param name="detailLink"/>
        <xsl:param name="host"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='slick2']//img//@src"/>
        <xsl:value-of select="concat($host,$outside)"/>
    </xsl:template>
    
    <!--Ta sẽ lấy ảnh chi tiết cho từng loại gundam-->
    <xsl:template name="getDetail">
        <xsl:param name="detailLink"/>
        <xsl:param name="host"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='slick2']//img"/>
        <xsl:for-each select="$outside">
            <xsl:element name="picture">
                <xsl:value-of select="concat($host,@src)"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <!--crawl từng phân trang-->
    <xsl:template name="getLink">
        <xsl:param name="type"/>
        <xsl:param name="link"/>
        <xsl:param name="host"/>
        <!--Cho vòng lập vào để lấy dữ liệu-->
        <!--<xsl:variable name="docLink" select="document($link)"/>-->
        <xsl:variable name="outsideElement" select="$link//div[@class='wap_item']//div[@class='item']"/>
        <xsl:for-each select="$outsideElement">
            <xsl:variable name="detailLink" select="concat($host,p[@class='sp_img zoom_hinh hover_sang1']//a//@href)"/>
            <xsl:variable name="avatar" select="concat($host,p[@class='sp_img zoom_hinh hover_sang1']//a//img//@src)"/>
            <xsl:variable name="name" select="p[@class='sp_img zoom_hinh hover_sang1']//a//@title"/>
            <xsl:variable name="priceInfo" select="p[@class='sp_gia']//span[@class='hienthigia']"/>
            <xsl:variable name="stock" select="div[@class='hethang']"/>
            <xsl:element name="gundam">
                <xsl:element name="name">
                    <xsl:value-of select="$name"/>
                </xsl:element>
                <xsl:element name="detailLink">
                    <xsl:value-of select="$detailLink"/>
                </xsl:element>
                <xsl:element name="avatar">
                    <xsl:value-of select="$avatar"/>
                </xsl:element>
                
                <xsl:element name="pictures">
                    <xsl:call-template name="getDetail">
                        <xsl:with-param name="detailLink" select="document($detailLink)"/>
                        <xsl:with-param name="host" select="$host"/>
                    </xsl:call-template>
                </xsl:element>
                
                <xsl:element name="price">
<!--                    <xsl:if test="$priceInfo//span[@class='giamoi motgia']">
                        <xsl:value-of select="text()"/>
                    </xsl:if>-->
                    <xsl:if test="$priceInfo//span[@class='giamoi ']">
                        <xsl:value-of select="$priceInfo//span[@class='giamoi ']//text()"/>
                    </xsl:if>
                    <xsl:if test="not($priceInfo//span[@class='giamoi '])">
                        <xsl:value-of select="$priceInfo//span//text()"/>
                    </xsl:if>
                </xsl:element>
                <xsl:element name="status">
                    <xsl:choose>
                        <xsl:when test="boolean($stock)">
                            <xsl:value-of select="'Hết hàng'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'Còn hàng'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
                <xsl:element name="type">
                    <xsl:value-of select="$type"/>
                </xsl:element>
                <xsl:element name="host">
                    <xsl:value-of select="$host"/>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
