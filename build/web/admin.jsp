<%-- 
    Document   : admin
    Created on : Mar 3, 2020, 1:19:04 PM
    Author     : HiruK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            /*Phần loader*/
            .loader {
                border: 10px solid #f3f3f3;
                border-radius: 50%;
                border-top: 10px solid #3498db;
                width: 50px;
                height: 50px;
                -webkit-animation: spin 2s linear infinite; /* Safari */
                animation: spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }


            table, th {
                border: 1px solid black;
            }
            th{padding: 10px;}

            * {box-sizing: border-box;}

            body {
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
            }

            /*Phần header*/
            .topnav {
                overflow: hidden;
                background-color: #343A40;
            }

            .topnav a {
                float: left;
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }

            .topnav a:hover {
                background-color:#f3f3f3 ;
                color: black;
            }

            .topnav a.active {
                background-color: #2196F3;
                color: white;
            }

            .topnav .search-container {
                float: right;
            }

            .topnav input[type=text] {
                padding: 6px;
                margin-top: 8px;
                font-size: 17px;
                border: none;
            }

            .topnav .search-container button {
                float: right;
                padding: 6px 10px;
                margin-top: 8px;
                margin-right: 16px;
                background: #ddd;
                font-size: 17px;
                border: none;
                cursor: pointer;
            }

            .topnav .search-container button:hover {
                background: green;
            }

            @media screen and (max-width: 600px) {
                .topnav .search-container {
                    float: none;
                }
                .topnav a, .topnav input[type=text], .topnav .search-container button {
                    float: none;
                    display: block;
                    text-align: left;
                    width: 100%;
                    margin: 0;
                    padding: 14px;
                }
                .topnav input[type=text] {
                    border: 1px solid #ccc;  
                }
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }

            /*Phần chỉnh paging nation*/
            .pagination {
                display: inline-block;
            }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }

            .pagination a.active {
                background-color: #0062cc;
                color: white;
            }

            .pagination a:hover:not(.active) {background-color: #ddd;}
        </style>
        <title>Admin Page</title>
    </head>
    <body onload="convertXmlToJavaScriptObject();numberWithCommas()">
        <div class="topnav" >
            <c:url var="crawl" value="DispatcherController">
                <c:param value="Crawl data" name="btAction"/>
            </c:url>
            <a class="" href="#" onclick="showLoader('${crawl}')">Crawl data</a>
            <!--<a href="DispatcherController?btAction=Clear all record" onclick="setZero()">Clear all record</a>-->
            <a href="#" onclick="setZero()">Clear all record</a>
            <!--<a href="#" onclick="updateNewTopIDList(), showAndHide()">Show Top Gundam</a>-->
            <a href="#" onclick="exportExcel()">Export Top Gundam model to Excel</a>
            <a href="ConvertPDFServlet" onclick="">See statistics in PDF</a>
            <div class="search-container">
                <form name="searchForm" action="DispatcherController" style="display: inline-block" onsubmit="return checkValueSearch()">
                    <input type="text" placeholder="Search by name" name="txtSearch">
                    <input type="hidden" name="txtPage" value="admin"/>
                    <button type="submit" name="btAction" value="Search" class="btn-dark">Search</button>

                </form>
                <!--<a href="list.jsp"><img  id="guestlist" src="img/icons8_list.png" style="display: inline-block;width: 30px;height: 30px;margin-right: 10px;color: green"/> </a>-->   
            </div>
        </div>
        <!--thanh loader-->
        <div class="loader" id="loading" style="display: none"></div>

        <!--Nếu như xóa thành công thì hiện chữ-->
        <c:if test="${requestScope.DELETE>0 or requestScope.DELETE==0}">
            <font style="color: blue" id="del"><p><h5>Xóa thành công</h5></p></font>
            </c:if>

    
    
    <!--Bảng này là chỉ dẫn màu-->
    <table style="border: none;margin-left: 10px;margin-top: 20px">
        <tr>
            <th style="background-color: #17a2b8;border: none" ></th>
            <th style="border: none">k2tgundam.com</th>
        </tr>
        <tr>
            <th style="background-color: indianred;border: none"></th>
            <th style="border: none">ymsgundam.com</th>
        </tr>
        <tr>
            <th style="background-color: #009966;border: none"></th>
            <th style="border: none">C3.com</th>
        </tr>
    </table>
<div>
        <p style="color: red">
            ${sessionScope.NAME}
        </p>
    </div>
    <c:set var="hostName" value=""/>
    <c:set var="icon" value=""/>
    <c:set var="searching" value="${requestScope.SEARCH}"/>

    <!--Phần div bên trái-->
    <div style="display: inline-block;vertical-align: top;text-align: left">
        <!--Phần dive show những kết quả tìm thấy-->
        <div id="showSearch">
            <c:if test="${not empty searching}">
                <p style="text-align: left;margin-left: 50px">----------------Kết quả tìm thấy-------------------</p>
                <c:forEach items="${searching}" var="item">
                    <c:choose>
                        <c:when test="${item.hostId==2}">
                            <c:set var="hostName" value="k2tgundam.com"/>
                            <c:set var="icon" value="${pageContext.request.contextPath}/img/k2t.PNG"/>

                        </c:when>
                        <c:when test="${item.hostId==3}">
                            <c:set var="hostName" value="ymsgundam.com"/>
                            <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>

                        </c:when>
                        <c:when test="${item.hostId==1}">
                            <c:set var="hostName" value="C3.com"/>
                            <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>

                        </c:when>
                    </c:choose> 
                    <div style="width: 600px" id="${item.id}">
                        <table style="margin-bottom: 10px;width: 100%">
                            <tr>
                                <th style="width: 30%">

                                    <img src="${item.avatar}" width="135px">
                                </th>
                                <th style="width: 60%">
                                    <!--<p>${item.name}<br/>${item.price}<br/>${item.typeId}</p>-->
                                    <table style="width: 100%;border: none">
                                        <tr>
                                            <td style="font-size: 14px;font-family: monospace;text-decoration: none"><a href="#" onclick="showUpdateTable('${item.id}', '${item.avatar}', '${item.name}', '${item.typeId}', '${item.price}', '${hostName}', '${item.detailLink}')">${item.name}</a></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px;font-weight: bold">${item.typeId}</td>
                                        </tr>
                                        <tr>
                                            <td class="money" style="color: red">${item.price}</td>
                                        </tr>
                                    </table>
                                    <!--<button style="" name="getAction"  class="btn btn-success" onclick="getRemoveOrUpdate('${item.id}', '${item.avatar}', '${item.name}', '${item.typeId}', '${item.price}', '${hostName}')" id="${item.id}btn">Add to suggest list</button>-->
                                    <!--function getRemoveOrUpdate(id,avatar,name,type,price,host) {-->

                                    <button style="background-color: green;margin-top: 5px;padding: 8px;color: white;border: 0px"><a style="text-decoration: none;color: white;font-size: 14px" href="${item.detailLink}" target="_blank">Go to shop page</a></button>
                                </th>

                                <c:if test="${hostName =='k2tgundam.com' }">
                                    <th style="background-color: #17a2b8;width: 2%">

                                    </th>
                                </c:if>
                                <c:if test="${hostName =='ymsgundam.com' }">
                                    <th style="background-color: indianred;width: 2%">

                                    </th>
                                </c:if>
                                <c:if test="${hostName =='C3.com' }">
                                    <th style="background-color: #009966;width: 2%">

                                    </th>
                                </c:if>

                            </tr>
                        </table>
                    </div>  
                </c:forEach>
            </c:if>


            <!--pagingnation-->
            <c:if test="${not empty requestScope.noOfPages}">
                <div class="pagination">
                    <a href="DispatcherController?pagging=${currentPage - 1}&txtPage=admin&btAction=Search&txtSearch=${param.txtSearch}">&laquo;</a>
                    <c:forEach begin="1" end="${requestScope.noOfPages}" var="i">
                        <c:choose>
                            <c:when test="${requestScope.currentPage eq i}">
                                <a class="active">${i}</a>
                            </c:when>
                            <c:otherwise>
                                <a href="DispatcherController?pagging=${i}&txtPage=admin&btAction=Search&txtSearch=${param.txtSearch}">${i}</a>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <a href="DispatcherController?pagging=${currentPage + 1}&txtPage=admin&btAction=Search&txtSearch=${param.txtSearch}">&raquo;</a>
                </div>
            </c:if>
        </div>

        <!--Phàn div show những top gundam-->
        <div id="top" style="display: none">
            <p style="text-align: left;margin-left: 50px">----Những mô hình ưa chuộng nhất----</p>
            <c:forEach items="${applicationScope.TESTTOP}" var="item">
                <c:choose>
                    <c:when test="${item.hostId=='http://k2tgundam.com/'}">
                        <c:set var="hostName" value="k2tgundam.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/k2t.PNG"/>
                    </c:when>

                    <c:when test="${item.hostId=='https://c3gundam.com/'}">
                        <c:set var="hostName" value="ymsgundam.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>
                    </c:when>

                    <c:when test="${item.hostId=='http://ymsgundam.com/'}">
                        <c:set var="hostName" value="C3.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>
                    </c:when>
                </c:choose> 
                <div style="width: 600px" id="${item.id}">
                    <table style="margin-bottom: 10px;width: 100%">
                        <tr>
                            <th style="width: 30%"><img src="${item.avatar}" width="100px"></th>
                            <th style="width: 70%">
                                <table style="width: 100%;border: none">
                                    <tr>
                                        <td style="font-size: 14px;font-family: monospace"><a href="${item.detailLink}">${item.name}</a></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px;font-weight: bold">${item.typeId}</td>
                                    </tr>
                                    <tr>
                                        <td class="money" style="color: red">${item.price}</td>
                                    </tr>
                                </table>
                            </th>
                            <c:if test="${hostName =='k2tgundam.com' }">
                                <th style="background-color: #17a2b8;width: 1%">

                                </th>
                            </c:if>
                            <c:if test="${hostName =='ymsgundam.com' }">
                                <th style="background-color: indianred;width: 1%">

                                </th>
                            </c:if>
                            <c:if test="${hostName =='C3.com' }">
                                <th style="background-color: #009966;width: 1%">

                                </th>
                            </c:if>
                        </tr>
                    </table>
                </div>  
            </c:forEach>
        </div>
    </div>

    <!--Phần div bên phải,tạm thời ko dùng-->    
    <div style="display: none">
        <p style="text-align: left;margin-left: 50px;padding: 0">----Những mô hình trong danh sách gợi ý---</p>
        <div style="overflow: scroll;height: 250px">
            <c:forEach items="${applicationScope.TESTTOP}" var="item">
                <c:choose>
                    <c:when test="${item.hostId=='http://k2tgundam.com/'}">
                        <c:set var="hostName" value="k2tgundam.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/k2t.PNG"/>
                    </c:when>

                    <c:when test="${item.hostId=='https://c3gundam.com/'}">
                        <c:set var="hostName" value="ymsgundam.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>
                    </c:when>

                    <c:when test="${item.hostId=='http://ymsgundam.com/'}">
                        <c:set var="hostName" value="C3.com"/>
                        <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>
                    </c:when>
                </c:choose> 
                <div style="width: 600px;margin-bottom: 10px;padding: 5px" id="${item.id}div" onclick="">
                    <!--<img src="${item.avatar}"  width="100px"/>-->
                    <div>
                        <p style="margin: 0;display: inline-block;font-weight: bold" onclick="showUpdateTable('${item.id}', '${item.name}', '${item.typeId}', '${hostName}', '${item.price}')">${item.name}</p>
                        <button style="display: inline-block;padding: 5px;color: white;background-color: red;border: 0">Remove</button>
                        <!---->
                    </div>
                </div>  
            </c:forEach>
        </div>
        <button style="width: 600px; background-color: #007bff;color: white;padding: 10px;border: 0" class="btn btn-primary ">Push the list</button>

    </div>

    <div id="updateTable" style="background-color: #ddd;width: 400px;height: max-content; margin-top: 10px;margin-bottom: 50px;padding: 10px;display: inline-block;position: fixed;margin-left: 20px">
        <div>
            <img id="gundamAvatar" src="img/1.png"  width="135px" style="margin-bottom: 10px"/>
            <div style="display: inline-block;margin-left: 12px;vertical-align: top">
                <p id="hostUpdate" style="margin: 0;font-weight: bold"><b>ymsgundam</b></p>
                <a id="gundamLink" target="_blank">Đi đến trang gốc -></a>
            </div>
        </div><br/>
        <input id="nameUpdate" style="width: 300px" type="text" value="Gundam 17" name="name"><br/><br/>
        <select>
            <c:forEach var="type" items="${applicationScope.TYPELIST}">
                <option id="typeUpdate" value="${type.id}">${type.name}</option>
            </c:forEach>
        </select><br/><br/>
        <input id="priceUpdate" type="number" value="120.000" name="price"><br/><br/>
        <input type="hidden" value="" id="idUpdate"/>
        <button style="width: 300px; background-color: #007bff;color: white;padding: 10px;border: 0;padding-left: 20px" onclick="updateInfo()">Update information</button>
    </div>


    <%--<%@include file="footer.jsp" %>--%>
    <!--    <div class="footer">
            <p>Footer</p>
        </div>-->
    <script>

        /**
         * Những var ko đổi
         * @param {type} name
         * @param {type} type
         * @param {type} host
         * @param {type} price
         * @returns {undefined}
         */
        var nameUpdate = document.getElementById("nameUpdate");
        var typeUpdate = document.getElementById("typeUpdate");
        var hostUpdate = document.getElementById("hostUpdate");
        var priceUpdate = document.getElementById("priceUpdate");
        var idUpdate = document.getElementById("idUpdate");
        var avatarShow = document.getElementById("gundamAvatar");
        var linkShow = document.getElementById("gundamLink");
        /**
         * 
         * hàm này có tác dụng mở lên bảng cập nhật thông tin cho những mô hình sẽ được gợi ý
         * @returns {undefined}
         */
        function showUpdateTable(id, avatar, name, type, price, host, link) {


            nameUpdate.value = name;
            typeUpdate.innerHTML = type;
            hostUpdate.innerHTML = host;
            priceUpdate.value = price;
            idUpdate.value = id;
            avatarShow.src = avatar;
            linkShow.href = link;
            /**
             * Nếu như display hiện ra thì mới có động tác gửi hàng để cập nhật
             * @returns {undefined}
             */
        }

        /**
         * Cập nhật thông tin của mô hình gundam đc chọn, nhưng thầy sẽ ko quan tâm đến CRUD
         * Phần này dư thừa 
         * @param {type} id
         * @param {type} name
         * @param {type} type
         * @param {type} price
         * @returns {undefined}
         */
        function updateInfo() {
            /**
             * Lấy thông tin của type
             */
            var value = typeUpdate.value;
            console.log(value);

            var xmlHttp = new XMLHttpRequest();
            var url = "UpdateServlet?id=" + idUpdate.value + "&name=" + nameUpdate.value + "&type=" + value + "&price=" + priceUpdate.value;

            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    console.log("Đã gửi đến update thành công");
                    alert(xmlHttp.responseText);
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi
        }


        /**
         * Hàm này có tách dụng export ra file excel
         * @type Element
         */
        function exportExcel() {
            var xmlHttp = new XMLHttpRequest();
            var url = "ExportServlet";
//                var xmlDom;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    alert(xmlHttp.responseText);
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi
        }

        //đây là root node
        var sugestList = document.createElement("listItem");


        /**
         * Khi mới load trang, ta sẽ lấy cái mảng mà chứa những item đc chọn để show cho người dùng
         * Nếu như mảng null thì ta sẽ tạo mới nó
         * @type Array|Object
         */
        var selectedGundam = JSON.parse(localStorage.getItem("list"));
        if (selectedGundam === null) {
            selectedGundam = [];
            console.log("null rồi");
        } else {
            console.log("đã có selected gundam và đây là độ dài= " + selectedGundam.length);
        }


        /**
         * Xử lí event hover, KO DÙNG NỮA
         * @returns {undefined}
         */
        function catchEventHover() {
            console.log("độ dài " + selectedGundam.length);
            for (i = 0; i < selectedGundam.length; i++) {
                var content = document.getElementById("content");
                var a = document.createElement("a");
                a.id = selectedGundam[i].id;
                a.href = "#";
                a.className = "inlist";
                var img = document.createElement("img");
                img.src = selectedGundam[i].avatar;
                img.style.width = "100px";
                img.style.height = "100px";
                img.style.display = "inline-block";

                var br = document.createElement("br");

                var p = document.createElement("p");
                p.appendChild(document.createTextNode(selectedGundam[i].name));
                p.style.display = "inline-block";
                p.style.marginLeft = "15px";
                p.style.fontWeight = "bold";

                var button = document.createElement("button");
                button.appendChild(document.createTextNode("Remove"));
                button.className = "btn badge-danger";

                p.appendChild(br);
                p.appendChild(button);

                a.appendChild(img);
                a.appendChild(p);

                content.appendChild(a);
            }
        }

        /**
         * Hàm này có tác dụng là thêm những gundam vào suggest list để user tiện theo dõi,KO DÙNG NỮA
         * @returns {undefined}
         */
        function createSelectedGundam(id, name, avatar) {
            var content = document.getElementById("content");
            var a = document.createElement("a");
            a.id = id;
            a.href = "#";
            a.className = "inlist";
            var img = document.createElement("img");
            img.src = avatar;
            img.style.width = "100px";
            img.style.height = "100px";
            img.style.display = "inline-block";

            var br = document.createElement("br");

            var p = document.createElement("p");
            p.appendChild(document.createTextNode(name));
            p.style.display = "inline-block";
            p.style.marginLeft = "15px";
            p.style.fontWeight = "bold";

            var button = document.createElement("button");
            button.style.backgroundColor = "red";
            button.appendChild(document.createTextNode("Remove"));

            p.appendChild(br);
            p.appendChild(button);

            a.appendChild(img);
            a.appendChild(p);

            content.appendChild(a);
        }

        /**
         * Hàm này dùng để xóa đi item ở cái menu khi user ko muốn suggest nữa,KO DÙNG NỮA
         * @returns {undefined}
         */
        function removeGumdamItemOutOfSuggestList(id) {
            var content = document.getElementById("content");
            var matchedList = document.getElementsByClassName("inlist");
            var co = document.getElementsByClassName("inlist");
            for (i = 0; i < matchedList.length; i++) {
                console.log("list là đay= " + matchedList[i].id);
                if (matchedList[i].id === id) {
                    content.removeChild(matchedList[i]);
                }
            }
        }

        /**
         * Hàm này dùng để kiểm tra xem có con gundam nào đc vào guess list ko, nếu có thì chuyển màu xanh
         * KO DÙNG HÀM NÀY NỮA
         * @returns {undefined}
         */
        function checkGundamInGuessList() {
            if (typeof (Storage) !== "undefined") {
                // Code for localStorage/sessionStorage.
                var list = sessionStorage.getItem("list");
                var iconList = document.getElementById("guestlist");
                if (list !== null) {
                    iconList.src = "img/icons8_list_green.png";
                } else {
                    iconList.src = "img/icons8_list.png";
                }
            } else {
                // Sorry! No Web Storage support..
            }
        }

        function saveItem(id, avatar, name, type, price, host) {

            var item = {
                id: "",
                name: "",
                price: "",
                avatar: "",
                type: "",
                host: ""
            };

            item.id = id;
            item.name = name;
            item.price = price;
            item.avatar = avatar;
            item.type = type;
            item.host = host;

            selectedGundam.push(item);

            if (typeof (Storage) !== "undefined") {
                console.log("Đã thành công");

                //Vì trong localStorage chỉ có lưu mỗi string thôi nên phải có bước convert object hay mảng thành string trước đã 
                localStorage.setItem("list", JSON.stringify(selectedGundam));

                //Sau khi thêm vào trong session storage rồi hãy gọi cái suggest list 
                createSelectedGundam(item.id, item.name, item.avatar);

            } else {
                // Sorry! No Web Storage support..
            }
        }

        /**
         *Hàm này dùng để xóa đi những item khi người dùng ko muốn trong list nữa, KO DÙNG NỮA
         * @returns {undefined}
         */
        function removeItem(id, name) {

            //Để lấy ra object hay array mình muốn, phải convert nó lại thành object

            var list = JSON.parse(localStorage.getItem("list"));
            console.log(list.length);
            for (i = 0; i < list.length; i++) {
                console.log(list[i].name);
            }
        }

        /**
         * hàm này dùng để thêm bớt những item đc gợi ý từ người admin, cho hết thông tin từ cái div đó vào luôn
         * Đã thành công
         * KO DÙNG NỮA
         * @param {type} id
         * @returns {undefined}
         */
        function getRemoveOrUpdate(id, avatar, name, type, price, host) {

            var string = "btn";//Thêm dòng này để phân biệt id của div và button
            /**
             * Nếu như button ko thêm dòng "btn" thì nó luôn trùng với cái id của div
             * còn nếu ta để getAction thì ko phân biệt đâu là button của div nào
             * kết hợp id+btn để tạo ra id độc nhất cho nó
             * @returns {undefined}
             */
            console.log("id button= " + id + string);

            var add = "Add to suggest list";
            var rm = "Remove";
            var btn = document.getElementById(id + string);//Lấy element button
            console.log(btn.textContent);
            var outSideDiv = document.getElementById(id);//Lấy element div
            if (btn.textContent === add && btn.className === "btn btn-success") {
                btn.className = "btn btn-danger";
                btn.textContent = rm;//Nếu value là chữ add thì đổi thành remove
                outSideDiv.style.opacity = 0.5;

                //Đang ở trạng thái bỏ vào list
                saveItem(id, avatar, name, type, price, host);
            } else {
                btn.className = "btn btn-success";
                btn.textContent = add;//còn ko thì vẫn giữ nguyên là chữ Add
                outSideDiv.style.opacity = 1;
                //                removeItem(id,name);
                removeGumdamItemOutOfSuggestList(id);
            }
        }

        /**
         * Hàm này dùng để hiện và ẩn show cái top id
         * @returns {undefined}
         */
        function showAndHide() {
            var searchItem = document.getElementById("showSearch");
            var x = document.getElementById("top");

            //Nếu như show top id thì kiểm tra xem có đang show kết quả tỉm kiếm ko, nếu có thì giấu nó đi
            if (x.style.display === "none") {
                searchItem.style.display = "none";
                x.style.display = "block";
            } else {
                searchItem.style.display = "block";
                x.style.display = "none";
            }
        }

        /**
         * Mỗi lần khi user nhấp vào show top list id, thì lúc này ta sẽ lấy lên danh sách mới nhất luôn
         * @returns {undefined}
         */
        function updateNewTopIDList() {
            var xmlHttp = new XMLHttpRequest();
            var url = "GetTheTopIDServlet";
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    console.log("Đã updata danh sách những top id rùi đấy")
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi
        }

        var array = [];//Tạo 1 array để lưu những gundam object đc chuyển từ hàm convertXmlToJavaScriptObject, thuận tiện cho việc thêm, xóa, sửa

        /**
         * Hàm này sẽ gửi về 1 cái XMLHttpRequest về server yêu cầu crawl dữ liệu về
         * @param {type} urlCrawl
         * @returns {undefined}
         */
        function showLoader(urlCrawl) {
            //CHỗ này là để hiện tắt cái loader
            var x = document.getElementById('loading');
            if (x.style.display === "none") {
                console.log("đã vào loader");
                x.style.display = "block";
            }//Kết thúc phần loader
            
            
            //Bắt đầu việc tạo ra XMlHttpRequest 
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    x.style.display = "none"//Khi đã crawl xong thì tắt cái loader và hiện cái alert
                    alert("Đã cào xong dữ liệu");

                }
            };
            xmlHttp.open("GET", urlCrawl, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi
        }
        /**
         * Hàm này sẽ yêu cầu server xóa hết dữ liệu trong DB đi,
         * Tránh trùng lập kết quả khi crawl dữ liệu mới về
         * @returns {undefined}
         */
        function setZero() {
            var show = document.getElementById("del");
            var xmlHttp = new XMLHttpRequest();
            var url = "DeleteServlet";
            var xmlDom;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    alert("Đã làm trống dữ liệu");
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi

        }

        /*
         * Hàm này dùng để convert xml thành javascript object, KO DÙNG NỮA
         * Đã thành công
         
         * @returns {undefined}             */
        function convertXmlToJavaScriptObject() {

            //Chuyển String thành DOM
            var xmlString = '${applicationScope.TOP}';
            var xmlDOc;

            var paser = new DOMParser();
            var xml = paser.parseFromString(xmlString, "text/xml");


            var path = "/items/item";
            var nodes = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null);
            var result = nodes.iterateNext();//Duyệt từng node đã có đc từ xpath
            while (result) {
                var item = {
                    id: "",
                    name: "",
                    price: "",
                    avatar: "",
                    link: "",
                    type: "",
                    firstDateAppearance: "",
                    numberOfTimeClicked: ""
                };
                item.name = result.getElementsByTagName("name")[0].childNodes[0].nodeValue;//Vì bây h current node là item nên chỉ có duy nhất 1 name node, ta viết thế này sẽ dễ lấy
                item.id = result.getElementsByTagName("id")[0].childNodes[0].nodeValue;
                item.price = result.getElementsByTagName("price")[0].childNodes[0].nodeValue;
                item.avatar = result.getElementsByTagName("avatar")[0].childNodes[0].nodeValue;
                item.link = result.getElementsByTagName("link")[0].childNodes[0].nodeValue;
                item.firstDateAppearance = result.getElementsByTagName("firstDateAppearance")[0].childNodes[0].nodeValue;
                console.log(item.firstDateAppearance);
                item.numberOfTimeClicked = result.getElementsByTagName("numberOfTimeClicked")[0].childNodes[0].nodeValue;

                array.push(item);

                //Đi tới node tiếp theo sau khi duyệt ở node hiện tại
                //Giống như thầy Khánh nói, khi duyệt node thì luôn phải bắt đầu ở ndoe mới 
                result = nodes.iterateNext();
            }
            console.log(array.length);
            for (i = 0; i < array.length; i++) {
                console.log(array[i].name);
            }
        }



        /**
         * functin này sẽ tạo ra 1 cái table, KO DÙNG NỮA
         * Đây là cách rất dở 
         * Sẽ có 1 cách là dùng stylesheet+xml để render ra html mà ko cần phải thưc hiện
         * cách khỉ ho cò gáy này, nó nằm trong file search.jsp
         */
        function creatTable() {
            for (i = 0; i < array.length; i++) {


                var body = document.getElementsByTagName('body')[0];
                var div = document.createElement('div');
                div.setAttribute('id', array[i].id);
                div.style.width = "600px";

                //tạo cái table
                var table = document.createElement('table');
                table.style.marginBottom = "10px";
                table.style.width = "100%";
                var tr = document.createElement('tr');


                var firstTh = document.createElement("th");
                firstTh.style.width = "30%";
                var img = document.createElement('img');
                img.setAttribute('width', '150px');
                img.setAttribute('src', array[i].avatar);
                firstTh.appendChild(img);


                //Cái th thứ 2
                var secontTh = document.createElement('th');
                secontTh.style.width = "70%";

                var pFirst = document.createElement('p');
                pFirst.appendChild(document.createTextNode(array[i].name));//Lấy name
                var br = document.createElement('br');
                pFirst.appendChild(br);

                pFirst.appendChild(document.createTextNode(array[i].price));//lấy giá
                var br = document.createElement('br');
                pFirst.appendChild(br);

                pFirst.appendChild(document.createTextNode(array[i].type));//lấy type

                //node p thứ 2
                var pSecond = document.createElement('p');
                var add = document.createElement('button');
                add.setAttribute('class', 'btn-success');
                add.innerHTML = "Add";

                var remove = document.createElement('button');
                remove.innerHTML = "Remove";
                remove.setAttribute('class', 'btn-danger');
                //                    remove.onclick= () => removeDiv(array[i].id);
                // Tạo ra id để bỏ vào
                var idNode = array[i].id;
                console.log(idNode);
                remove.onclick = function () {
                    console.log("id của node= " + idNode);
                    removeDiv(idNode);
                };
                //                    remove.onclick=removeDiv(array[i].id);
                remove.style.marginLeft = "10px";

                //tạo ra node id
                var infoId = document.createElement("idnode");
                infoId.appendChild(document.createTextNode(array[i].id));
                infoId.style.display = "none";
                pSecond.appendChild(add);
                pSecond.appendChild(remove);

                //add 2 cái p vào cái th thứ 2
                secontTh.appendChild(pFirst);
                secontTh.appendChild(pSecond);

                tr.appendChild(firstTh);
                tr.appendChild(secontTh);
                table.appendChild(tr);
                div.appendChild(table);
                div.appendChild(infoId);
                body.appendChild(div);
            }

        }


        /**
         * KO DÙNG HÀM NÀY
         * @returns {undefined}
         */
        function tableCreate() {
            var body = document.getElementsByTagName('body')[0];
            var tbl = document.createElement('table');
            tbl.style.width = '100%';
            tbl.setAttribute('border', '1');
            var tbdy = document.createElement('tbody');
            for (var i = 0; i < 3; i++) {
                var tr = document.createElement('tr');
                for (var j = 0; j < 2; j++) {
                    if (i == 2 && j == 1) {
                        break
                    } else {
                        var td = document.createElement('td');
                        td.appendChild(document.createTextNode('\u0020'))
                        i == 1 && j == 1 ? td.setAttribute('rowSpan', '2') : null;
                        tr.appendChild(td)
                    }
                }
                tbdy.appendChild(tr);
            }
            tbl.appendChild(tbdy);
            body.appendChild(tbl)
        }




        /**
         * Hàm này dùng để test gửi xmlHttpRequest về để nhận data mà ko cần load trang
         * Đã thành công
         * @returns {undefined}
         */
        function testing() {
            var xmlHttp = new XMLHttpRequest();
            var url = "GetTheTopIDServlet?name=sam";
            var xmlDom;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    xmlDom = xmlHttp.responseXML;//responseXML là dữ liệu thuộc kiểu XML
                    if (xmlDom != null) {
                        var x = xmlDom.getElementsByTagName("name");
                        console.log(x.length)
                        for (var i = 0; i < x.length; i++) {
                            console.log("Day la value= " + x[i].childNodes[0].nodeValue);
                        }
                    } else {
                        console.log("null bỏ mẹ rùi");
                    }

                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null);//Bắt đầu gửi request đi
        }
    </script>
    <script>
        /**
         * Hàm này sẽ remove table mà ta chọn dựa trên id, KO DÙNG HÀM NÀY
         * @param {type} id
         * @returns {undefined}
         */
        function removeDiv(id) {
            console.log("hehe= " + id);

        }
    </script>
    <script>

        /**
         * Chuyển đổi số thành định dạng đẹp hơn, định dạng số ban đầu là 750000
         * Sau khi qua hàm này: 750.000đ
         * @param {type} x
         * @returns {String}
         */
        function numberWithCommas() {
            var money = document.getElementsByClassName("money");
            for (i = 0; i < money.length; i++) {
                if (money[i].textContent === "0") {
                    money[i].textContent = "Liên hệ";
                } else {
                    money[i].textContent = money[i].textContent.replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "đ";
                }
            }
        }

        /**
         * Hàm này sẽ kiểm tra nội dung search của người dùng, hạn chế search ra quá nhiều record
         * @returns {undefined}
         */
        function checkValueSearch() {
            var searchInput = document.forms["searchForm"]["txtSearch"].value.toLowerCase();
            console.log(searchInput);
            if (searchInput === "gundam") {
                alert("Bạn đưa ra từ quá rộng, hay cụ thể hơn bằng tên");
                return false;
            }
        }
    </script>
</body>
</html>
