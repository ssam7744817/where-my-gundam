/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.TopIdDTO;
import dtos.TopIdDTOList;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import outputenum.TypeEnum;

/**
 *
 * @author HiruK
 */
public class TopIdDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    TopIdDTOList list = new TopIdDTOList();
    TopIdDTOList topFive = new TopIdDTOList();

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public TopIdDTOList getTopFive() {
        return topFive;
    }

    public void setTopFive(TopIdDTOList topFive) {
        this.topFive = topFive;
    }

    public TopIdDTOList getList() {
        return list;
    }

    public void setList(TopIdDTOList list) {
        this.list = list;
    }

    public boolean saveTopID(String id, Date date) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sqlFirst = "insert into topid(id,dateAppearance)values(?,?)";
            preStm = conn.prepareStatement(sqlFirst);
            preStm.setString(1, id);
            preStm.setDate(2, date);
            int result = preStm.executeUpdate();
            if (result > 0) {
                System.out.println("save ok");
                return true;
            } else {
                return false;
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Hàm dùng để lấy những gundam đc click nhiều nhất
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void getTheResult() throws ClassNotFoundException, SQLException {
        TopIdDTO topId;

        try {
            conn = MyConnection.getConnection();
            String sql = "select  topId.id as Id,gundam.name as name,gundam.price as price,gundam.avatar as avatar, gundam.detailLink as link,gundam.typeId as type, min(topId.dateAppearance) as firstDateAppearance, count (topId.dateAppearance) as numberOfTimeClicked,gundam.hostId as hostId from topId join gundam  on gundam.id=topId.id group by topId.id ,gundam.name,gundam.typeId,gundam.avatar,gundam.detailLink,gundam.price,gundam.hostId";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                topId = new TopIdDTO();
                topId.setId(rs.getString("Id"));
                topId.setFirstDateAppearance(rs.getDate("firstDateAppearance").toString());
                topId.setType(changeType(rs.getInt("type")));
                topId.setNumberOfTimeClicked(rs.getInt("numberOfTimeClicked"));
                topId.setName(rs.getString("name"));
                topId.setAvatar(rs.getString("avatar"));
                topId.setLink(rs.getString("link"));
                topId.setPrice(rs.getInt("price"));
                topId.setHostId(rs.getInt("hostId"));
                list.getTopId().add(topId);
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Hàm dùng để lấy top 5 gundam được pick nhiều nhất
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void getTop5Result() throws ClassNotFoundException, SQLException {
        TopIdDTO topId;

        try {
            conn = MyConnection.getConnection();
            String sql = "select top 5  topId.id as Id,gundam.name as name,gundam.price as price,gundam.avatar as avatar, gundam.detailLink as link,gundam.typeId as type, min(topId.dateAppearance) as firstDateAppearance, count (topId.dateAppearance) as numberOfTimeClicked,gundam.hostId as hostId from topId join gundam  on gundam.id=topId.id group by topId.id ,gundam.name,gundam.typeId,gundam.avatar,gundam.detailLink,gundam.price,gundam.hostId order by numberOfTimeClicked desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                topId = new TopIdDTO();
                topId.setId(rs.getString("Id"));
                topId.setFirstDateAppearance(rs.getDate("firstDateAppearance").toString());
                topId.setType(changeType(rs.getInt("type")));
                topId.setNumberOfTimeClicked(rs.getInt("numberOfTimeClicked"));
                topId.setName(rs.getString("name"));
                topId.setAvatar(rs.getString("avatar"));
                topId.setLink(rs.getString("link"));
                topId.setPrice(rs.getInt("price"));
                topId.setHostId(rs.getInt("hostId"));
                topFive.getTopId().add(topId);
            }
        } finally {
            closeConnection();
        }
    }

    public static void main(String[] args) {
        TopIdDAO dao = new TopIdDAO();
        try {
            dao.getTheResult();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TopIdDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(TopIdDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Chuyển số thành loại gundam
     *
     * @param type
     * @return
     */
    public String changeType(int type) {
        String number = "";
        if (type == 1) {
            number = TypeEnum.HG.toString();
        } else if (type == 2) {
            number = TypeEnum.RG.toString();
        } else if (type == 3) {
            number = TypeEnum.MG.toString();
        } else if (type == 4) {
            number = TypeEnum.SD.toString();
        } else if (type == 5) {
            number = TypeEnum.PG.toString();
        }
        return number;
    }
}
