/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.jaxb;

import daos.GundamDAO;
import generate.gundams.Gundam;
import generate.gundams.Gundams;
import java.io.File;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *CÁI CLASS NÀY VỚI MỤC ĐÍCH CHỈ ĐỂ TEST THÔI, KO DÙNG NỮA
 * @author HiruK
 */
public class UnmarshallingWithPicture {

    /**
     * Chỉ dùng để test
     */
    public Gundams creatGundamObjectWithDOMWithFile() throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Gundams.class);
        Unmarshaller u = jc.createUnmarshaller();
        File f = new File("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml");
        Gundams gundam = (Gundams) u.unmarshal(f);
        return gundam;
    }

    public static void main(String[] args) {
        try {
            UnmarshallingWithPicture test = new UnmarshallingWithPicture();
            Gundams gundams = test.creatGundamObjectWithDOMWithFile();

            GundamDAO dao = new GundamDAO();
            System.out.println("lengh= " + gundams.getGundam().size());
            for (Gundam gundam : gundams.getGundam()) {
                System.out.println("price= " + gundam.getPrice());
                dao.insert(gundam);

            }

        } catch (JAXBException ex) {
            Logger.getLogger(UnmarshallingWithPicture.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UnmarshallingWithPicture.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UnmarshallingWithPicture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
