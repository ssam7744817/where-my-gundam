/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import daos.GundamDAO;
import daos.GundamModelTypeDAO;
import daos.GundamSlideShowDAO;
import dtos.GundamDTO;
import dtos.GundamModelTypeDTO;
import dtos.GundamSlideShowDTO;
import generate.gundams.Gundam;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HiruK
 */
public class HomeServlet extends HttpServlet {

    private final String HOME = "index.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = "";
        try {
            GundamModelTypeDAO typeDao = new GundamModelTypeDAO();
            GundamSlideShowDAO imgDao = new GundamSlideShowDAO();
            GundamDAO dao = new GundamDAO();
            typeDao.getModelTypeName();
            imgDao.getImageName();
            dao.getTop12();
            List<GundamModelTypeDTO> typeList = typeDao.getList();
            List<GundamSlideShowDTO> slideList = imgDao.getList();
            List<GundamDTO> gundamList = dao.getJaxbList().getGundam();
            
            if (!slideList.isEmpty() && !typeList.isEmpty()) {
                request.setAttribute("TYPELIST", typeList);
                request.setAttribute("GUNDAM", gundamList);
                System.out.println("length= " + slideList.size());
                url = HOME;
            }
            request.getRequestDispatcher(url).forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /**
             * Nếu ta sử dụng requestScope, để muốn dùng nó sau khi đã response
             * thì phải dùng requestDispatcher giữ lại cái request mới show hàng
             * đc, hoặc ta dùng session Nếu dùng response.sendRedirec thì ko thể
             * dùng request,setAttribute
             */
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
