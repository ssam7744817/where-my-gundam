/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;
import java.sql.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author HiruK
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "item", propOrder = {
    "id",
    "name",
    "price",
    "avatar",
    "link",
    "type",
    "firstDateAppearance",
    "numberOfTimeClicked",
    "hostId"
})
public class TopIdDTO implements Serializable {

    private String id;
    private String name;
    private String type;
    private String firstDateAppearance;
    private int numberOfTimeClicked;
    private String link;
    private String avatar;
    private int price;
    private int hostId;

    public TopIdDTO() {

    }

    public TopIdDTO(String id, String name, String type, String firstDateAppearance, int numberOfTimeClicked) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.firstDateAppearance = firstDateAppearance;
        this.numberOfTimeClicked = numberOfTimeClicked;
    }

    public int getHostId() {
        return hostId;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }

    public String getId() {
        return id;
    }

    public String getLink() {
        return link;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstDateAppearance() {
        return firstDateAppearance;
    }

    public void setFirstDateAppearance(String firstDateAppearance) {
        this.firstDateAppearance = firstDateAppearance;
    }

    public int getNumberOfTimeClicked() {
        return numberOfTimeClicked;
    }

    public void setNumberOfTimeClicked(int numberOfTimeClicked) {
        this.numberOfTimeClicked = numberOfTimeClicked;
    }

}
