/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.ClickInfoDTO;
import dtos.GundamDTO;
import dtos.GundamDTOList;
import dtos.ViewDTO;
import generate.gundams.Gundam;
import generate.report.Detailinfo;
import generate.report.Hostinfo;
import generate.report.Report;
import generate.report.Typeinfo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaclass.Interaction;
import javaclass.TypeUtil;
import outputenum.HostEnum;
import outputenum.TypeEnum;

/**
 *
 * @author HiruK
 */
public class GundamDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    private TopIdDAO topDao = new TopIdDAO();
    private List<Gundam> list = new ArrayList<Gundam>();
    int pagingCount = 0;
    private GundamDTOList jaxbList = new GundamDTOList();
    private HashMap<GundamDTO, Interaction> export = new HashMap<>();
    private List<generate.report.Detailinfo> topGundamList = new ArrayList<>();

    public HashMap<GundamDTO, Interaction> getExport() {
        return export;
    }

    public void setExport(HashMap<GundamDTO, Interaction> export) {
        this.export = export;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public int getPagingCount() {
        return pagingCount;
    }

    public void setPagingCount(int pagingCount) {
        this.pagingCount = pagingCount;
    }

    public List<Detailinfo> getTopGundamList() {
        return topGundamList;
    }

    public void setTopGundamList(List<Detailinfo> topGundamList) {
        this.topGundamList = topGundamList;
    }

    /**
     *
     * @param gundamType
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public int saveCrawlGundamInfoToDB(Gundam gundamType) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "insert into gundam(name,detailLink,avatar,price,statusId,typeId,hostId,id)values(?,?,?,?,?,?,?,?)";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, gundamType.getName());
            preStm.setString(2, gundamType.getDetailLink());
            preStm.setString(3, gundamType.getAvatar());
            preStm.setInt(4, Integer.valueOf(gundamType.getPrice()));
            preStm.setInt(5, changeStatusToInt(gundamType.getStatus()));
            preStm.setInt(6, changeType(gundamType.getType()));
            preStm.setInt(7, changeHost(gundamType.getHost()));
            preStm.setString(8, UUID.randomUUID().toString());
            int result = preStm.executeUpdate();
            if (result > 0) {
                System.out.println("Thành công");
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    public int changeStatusToInt(String status) {
        int number = 0;
        if (status.equals("Còn hàng")) {
            number = 1;
        }
        return number;
    }

    /**
     * Đổi host thành số
     *
     * @param host
     * @return
     */
    public int changeHost(String host) {
        int number = 0;
        if (host.equals(HostEnum.YMS.toString())) {
            number = 3;
        } else if (host.equals(HostEnum.K2T.toString())) {
            number = 2;
        } else if (host.equals(HostEnum.C3.toString())) {
            number = 1;
        }
        return number;
    }

    /**
     * Chuyển loại gundam thành số
     *
     * @param type
     * @return
     */
    public int changeType(String type) {
        int number = 0;
        if (type.equals(TypeEnum.HG.toString())) {
            number = 1;
        } else if (type.equals(TypeEnum.MG.toString())) {
            number = 3;
        } else if (type.equals(TypeEnum.PG.toString())) {
            number = 5;
        } else if (type.equals(TypeEnum.RG.toString())) {
            number = 2;
        } else if (type.equals(TypeEnum.SD.toString())) {
            number = 4;
        }
        return number;
    }

//    /**
//     * Dùng cho việc testing thui
//     *
//     * @param args
//     */
//    public static void main(String[] args) {
//        GundamDAO dAO = new GundamDAO();
//        crawler.jaxb.JAXBUnmarshallingGundams jaxb = new JAXBUnmarshallingGundams();
//        try {
//            for (GundamOutputEnum output : GundamOutputEnum.values()) {
//                Gundams gundams = jaxb.creatGundamObject(output.toString());
//                dAO.setList(gundams.getGundam());
//                for (Gundam gundam : dAO.getList()) {
//                    dAO.saveCrawlGundamInfoToDB(gundam);
//                }
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(GundamDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(GundamDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (JAXBException ex) {
//            Logger.getLogger(GundamDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        for (GundamOutputEnum output : GundamOutputEnum.values()) {
//            System.out.println("output : " + output);
//        }
//    }
    public void getTop12() throws ClassNotFoundException, SQLException {
        GundamDTO gundam;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT top 12 [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,[statusId]\n"
                    + "      ,[typeId]\n"
                    + "      ,[hostId]\n"
                    + "      ,[id]\n"
                    + "  FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                jaxbList.getGundam().add(gundam);
            }
        } finally {
            closeConnection();
        }
    }

    //Lấy hết tất cả gundam lên
    public void getAll() throws ClassNotFoundException, SQLException {
        GundamDTO gundam;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,[statusId]\n"
                    + "      ,[typeId]\n"
                    + "      ,[hostId]\n"
                    + "      ,[id]\n"
                    + "  FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                jaxbList.getGundam().add(gundam);
            }
        } finally {
            closeConnection();
        }
    }

    public GundamDTOList getJaxbList() {
        return jaxbList;
    }

    public void setJaxbList(GundamDTOList jaxbList) {
        this.jaxbList = jaxbList;
    }

    public void getTop10v2() throws ClassNotFoundException, SQLException {
        GundamDTO gundam;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT top 10 [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,[statusId]\n"
                    + "      ,[typeId]\n"
                    + "      ,[hostId]\n"
                    + "      ,[id]\n"
                    + "  FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                jaxbList.getGundam().add(gundam);
            }
        } finally {
            closeConnection();
        }
    }

    public void searchGundam(String search) throws ClassNotFoundException, SQLException {
        try {
            conn = MyConnection.getConnection();
            preStm = conn.prepareStatement("SELECT [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,[statusId]\n"
                    + "      ,[typeId]\n"
                    + "      ,[hostId]\n"
                    + "      ,[id]\n"
                    + "	  \n"
                    + "  FROM [dbo].[gundam]\n"
                    + "  where gundam.name like ?");
            preStm.setString(1, "%" + search + "%");
            rs = preStm.executeQuery();
            GundamDTO gundam;
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                gundam.setHostId(rs.getString("hostId"));
                gundam.setTypeId(rs.getString("typeId"));
                gundam.setStatusId(rs.getString("statusId"));
                jaxbList.getGundam().add(gundam);
//                if (jaxbList.getGundam() != null) {
//                    System.out.println("list length in dao :" + jaxbList.getGundam().size());
//                }
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * tham số start để cho ta biết ta đang ở trang thứ mấy tham số total cho ta
     * biết mỗi trang ta muốn lấy bao nhiêu record
     *
     * @param search
     * @param start
     * @param total
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void searchGundamWithPaging(String search, int start, int total) throws ClassNotFoundException, SQLException {
        try {
            conn = MyConnection.getConnection();
            preStm = conn.prepareStatement("SELECT *, count(*) over() as countt FROM gundam where gundam.name  like ? ORDER BY gundam.name OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");
            preStm.setString(1, "%" + search + "%");
            preStm.setInt(2, start);
            preStm.setInt(3, total);
            rs = preStm.executeQuery();
            GundamDTO gundam;
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                gundam.setHostId(rs.getString("hostId"));
                gundam.setTypeId(topDao.changeType(rs.getInt("typeId")));
                gundam.setStatusId(rs.getString("statusId"));
                setPagingCount(rs.getInt("countt"));
                jaxbList.getGundam().add(gundam);

            }
            if (jaxbList.getGundam() != null) {
                System.out.println("list length in dao :" + jaxbList.getGundam().size());
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Giống như hàm trên nhưng ưu tiên những gundam đc click nhiều nhất
     *
     * @param search
     * @param start
     * @param total
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void searchGundamWithPagingPrioprity(String search, int start, int total) throws ClassNotFoundException, SQLException {
        try {
            conn = MyConnection.getConnection();
            preStm = conn.prepareStatement("SELECT [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,[typeId]\n"
                    + "      ,[hostId]\n"
                    + "      ,[id]\n"
                    + "	  ,count(*) over() as totalrecord\n"
                    + "  FROM (select *,count(*) over() as totalrecord from gundam where gundam.name like ?) as c\n"
                    + "  LEFT JOIN (\n"
                    + "  SELECT click_info.gundamid, count(*) as numOf\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.gundamid is not null\n"
                    + "  group by click_info.gundamid having count(*)>1 \n"
                    + "  ) as g on c.id = g.gundamid\n"
                    + "  order by g.numOf DESC  \n"
                    + "  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY ");
            preStm.setString(1, "%" + search + "%");
            preStm.setInt(2, start);
            preStm.setInt(3, total);
            rs = preStm.executeQuery();
            GundamDTO gundam;
            while (rs.next()) {
                gundam = new GundamDTO();
                gundam.setId(rs.getString("id"));
                gundam.setDetailLink(rs.getString("detailLink"));
                gundam.setAvatar(rs.getString("avatar"));
                gundam.setPrice(rs.getString("price"));
                gundam.setName(rs.getString("name"));
                gundam.setHostId(rs.getString("hostId"));
                gundam.setTypeId(topDao.changeType(rs.getInt("typeId")));
                setPagingCount(rs.getInt("totalrecord"));
                jaxbList.getGundam().add(gundam);

            }
            if (jaxbList.getGundam() != null) {
                System.out.println("list length in dao :" + jaxbList.getGundam().size());
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Lấy tên để phục vụ cho việc search
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void getAllName() throws ClassNotFoundException, SQLException {
        GundamDTO dTO;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [name]\n"
                    + "\n"
                    + "  FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dTO = new GundamDTO();
                dTO.setName(rs.getString("name"));
                jaxbList.getGundam().add(dTO);
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * @return the list
     */
    public List<Gundam> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<Gundam> list) {
        this.list = list;
    }

    /**
     * Đây sẽ là hàm insert vào DB mới
     *
     * @param gundamType
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void insert(Gundam gundamType) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "insert into gundam(name,detailLink,avatar,price,statusId,typeId,hostId,id)values(?,?,?,?,?,?,?,?)";
            String sqlDetailPic = "insert into picture(id,picture)values(?,?)";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, gundamType.getName());
            preStm.setString(2, gundamType.getDetailLink());
            preStm.setString(3, gundamType.getAvatar());
            preStm.setInt(4, Integer.valueOf(gundamType.getPrice()));
            preStm.setInt(5, changeStatusToInt(gundamType.getStatus()));
            preStm.setInt(6, changeType(gundamType.getType()));
            preStm.setInt(7, changeHost(gundamType.getHost()));
            String id = UUID.randomUUID().toString();
            preStm.setString(8, id);
            int result = preStm.executeUpdate();

//            Đây là phần để ảnh vào
            for (int i = 0; i < gundamType.getPictures().getPicture().size(); i++) {
                String pic = gundamType.getPictures().getPicture().get(i);
                preStm = conn.prepareStatement(sqlDetailPic);
                preStm.setString(1, id);
                preStm.setString(2, pic);
                preStm.executeUpdate();
            }
            if (result > 0) {
                System.out.println("Success");
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Xóa hết tất cả record ở database
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public int deleteAll() throws ClassNotFoundException, SQLException {
        int result = 0;
        try {
            conn = MyConnection.getConnection();
            String sql = "DELETE FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            result = preStm.executeUpdate();
            if (result > 0) {
                System.out.println("Delete Ok");
            }
        } finally {
            closeConnection();
        }
        return result;
    }

    /**
     * Dùng để đếm số record trong db, để hiện lên trang admin
     *
     * @return
     */
    public int countRecord() throws SQLException, ClassNotFoundException {
        int restul = 0;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT COUNT(gundam.id)\n"
                    + "  FROM [dbo].[gundam]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            if (rs.next()) {
                restul = rs.getInt(1);
            }
        } finally {
            closeConnection();
        }
        return restul;
    }

    public boolean update(String id, String name, String typeid, String price) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "update gundam set name=?,typeId=?,price=? where id=?";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, name);
            preStm.setInt(2, Integer.valueOf(typeid));
            preStm.setInt(3, Integer.valueOf(price));
            preStm.setString(4, id);
            int result = preStm.executeUpdate();
            if (result > 0) {
                return true;
            }
        } finally {
            closeConnection();
        }
        return false;
    }

    /**
     * CHọn ra 1 danh sách mà chỉ những gundam có lượt view và click >0
     */
    public void getListHaveViewAndClick() throws SQLException, ClassNotFoundException {
        GundamDTO dto;
        int view;//chứa số lần view
        int click;//chứa số lần click
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,t.type\n"
                    + "      ,h.host\n"
                    + "      ,[id]\n"
                    + "	  ,g.numberoftimeclicked\n"
                    + "	  ,v.numberofview\n"
                    + "  FROM [dbo].[gundam] as c\n"
                    + "  JOIN (SELECT click_info.gundamid, count(*) as numberoftimeclicked\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.gundamid is not null\n"
                    + "  group by click_info.gundamid )\n"
                    + "  as g on c.id = g.gundamid\n"
                    + "  left join (\n"
                    + "  select viewpage.gundaminshopid,COUNT(*) numberofview\n"
                    + "  from viewpage\n"
                    + "  where viewpage.gundaminshopid is not null\n"
                    + "  group by viewpage.gundaminshopid\n"
                    + "  ) as v on c.id=v.gundaminshopid\n"
                    + "  left join(\n"
                    + "  select gundam_model_type.name as type,gundam_model_type.typeId\n"
                    + "  from gundam_model_type\n"
                    + "  )as t on t.typeId=c.typeId\n"
                    + "   left join(\n"
                    + "  select gundam_web_host.name as host,gundam_web_host.hostId\n"
                    + "  from gundam_web_host\n"
                    + "  )as h on h.hostId=c.hostId\n"
                    + "  order by g.numberoftimeclicked desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new GundamDTO();
                dto.setId(rs.getString("id"));
                dto.setName(rs.getString("name"));
                dto.setDetailLink(rs.getString("detailLink"));
                dto.setAvatar(rs.getString("avatar"));
                dto.setHostId(rs.getString("host"));
                dto.setTypeId(rs.getString("type"));
                dto.setPrice(String.valueOf(rs.getInt("price")));

                //Điều kiện ở dưới để chặn trường hợp null
                if (rs.getString("numberofview") == null) {
                    view = 0;
                } else {
                    view = Integer.valueOf(rs.getString("numberofview"));
                }
                if (rs.getString("numberoftimeclicked") == null) {
                    click = 0;
                } else {
                    click = Integer.valueOf(rs.getString("numberoftimeclicked"));
                }
                export.put(dto, new Interaction(view, click));
                jaxbList.getGundam().add(dto);

            }
        } finally {
            closeConnection();
        }
    }

    /**
     * CHọn ra 1 danh sách mà chỉ những gundam có lượt view và click >0
     */
    public void getListHaveViewAndClickPDFversion(Report report) throws SQLException, ClassNotFoundException {
        generate.report.Detailinfo dto;
        int view;//chứa số lần view
        int click;//chứa số lần click
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [name]\n"
                    + "      ,[detailLink]\n"
                    + "      ,[avatar]\n"
                    + "      ,[price]\n"
                    + "      ,t.type\n"
                    + "      ,h.host\n"
                    + "      ,[id]\n"
                    + "	  ,g.numberoftimeclicked\n"
                    + "	  ,v.numberofview\n"
                    + "  FROM [dbo].[gundam] as c\n"
                    + "  JOIN (SELECT click_info.gundamid, count(*) as numberoftimeclicked\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.gundamid is not null\n"
                    + "  group by click_info.gundamid )\n"
                    + "  as g on c.id = g.gundamid\n"
                    + "  left join (\n"
                    + "  select viewpage.gundaminshopid,COUNT(*) numberofview\n"
                    + "  from viewpage\n"
                    + "  where viewpage.gundaminshopid is not null\n"
                    + "  group by viewpage.gundaminshopid\n"
                    + "  ) as v on c.id=v.gundaminshopid\n"
                    + "  left join(\n"
                    + "  select gundam_model_type.name as type,gundam_model_type.typeId\n"
                    + "  from gundam_model_type\n"
                    + "  )as t on t.typeId=c.typeId\n"
                    + "   left join(\n"
                    + "  select gundam_web_host.name as host,gundam_web_host.hostId\n"
                    + "  from gundam_web_host\n"
                    + "  )as h on h.hostId=c.hostId\n"
                    + "  order by g.numberoftimeclicked desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new Detailinfo();
                dto.setId(rs.getString("id"));
                dto.setGundamName(rs.getString("name"));
                dto.setPrice(rs.getString("price"));
                dto.setType(rs.getString("type"));
                dto.setPage(rs.getString("host"));

                //Điều kiện ở dưới để chặn trường hợp null
                if (rs.getString("numberofview") == null) {
                    view = 0;
                } else {
                    view = Integer.valueOf(rs.getString("numberofview"));
                }
                if (rs.getString("numberoftimeclicked") == null) {
                    click = 0;
                } else {
                    click = Integer.valueOf(rs.getString("numberoftimeclicked"));
                }
                dto.setViewCount(String.valueOf(view));
                dto.setClickCount(String.valueOf(click));
                report.getModelGundam().add(dto);//set danh sách các gundam host nhất vào

            }

            //Lấy thống kê host
            Hostinfo hostinfo;
            sql = "SELECT c.host, c.hostclickcount,v.hostviewcount\n"
                    + "  FROM (\n"
                    + "  SELECT click_info.host, count(click_info.host) as hostclickcount\n"
                    + "  from click_info\n"
                    + "  where click_info.host is not null\n"
                    + "  group by click_info.host\n"
                    + "  ) as c\n"
                    + " left join (\n"
                    + "  SELECT viewpage.host,count(viewpage.host) as hostviewcount\n"
                    + "  FROM [dbo].[viewpage]\n"
                    + "    where viewpage.host is not null\n"
                    + "  group by viewpage.host\n"
                    + " ) as v\n"
                    + " on v.host=c.host";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                hostinfo = new Hostinfo();
                hostinfo.setClickCount(String.valueOf(rs.getInt("hostclickcount")));
                hostinfo.setViewCount(String.valueOf(rs.getInt("hostviewcount")));
                hostinfo.setHostName(TypeUtil.convertNumberHostToString(rs.getInt("host")));
                report.getHost().add(hostinfo);
            }

            //Lấy thống kê type
            Typeinfo typeinfo;
            sql = "SELECT click_info.type, count(click_info.type) as typecount\n"
                    + "  FROM [dbo].[click_info]\n"
                    + "  where click_info.type is not null\n"
                    + "  group by click_info.type\n"
                    + "  order by typecount desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                typeinfo = new Typeinfo();
                typeinfo.setTypeName(TypeUtil.convertNumberTypeToString(rs.getInt("type")));
                typeinfo.setClickCount(String.valueOf(rs.getInt("typecount")));
                report.getType().add(typeinfo);
            }
        } finally {
            closeConnection();
        }
    }

    /**
     * Xóa hết tất cả record, làm lại từ đầu =)
     */
    public int clearAllRecord() throws SQLException, ClassNotFoundException {
        int result = 0;
        try {
            conn = MyConnection.getConnection();
            String sql = "delete from gundam_in_anime\n"
                    + "DELETE FROM [dbo].[anime]\n"
                    + "delete from picture\n"
                    + "Delete from click_info\n"
                    + "Delete from viewpage\n"
                    + "delete from gundam";
            preStm = conn.prepareStatement(sql);
            result = preStm.executeUpdate();
            if (result > 0) {
                System.out.println("All record have been delete");
            }
        } finally {
            closeConnection();
        }
        return result;
    }

    public static void main(String[] args) {
        GundamDAO dao = new GundamDAO();
        try {
//            dao.searchGundamWithPaging("Gundam Astray", 1, 5);
            boolean result = dao.update("5a3edae2-ee46-41f8-b245-097498213ec4", "halo", "1", "12");
            System.out.println("result= " + result);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GundamDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GundamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Đưa vào những short key tốt nhất ở bảng short key và gundam
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */

}
