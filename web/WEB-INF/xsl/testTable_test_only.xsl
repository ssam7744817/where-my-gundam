<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : testTable.xsl
    Created on : April 4, 2020, 5:15 PM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
            <div>
                <xsl:apply-templates select="Report"/>
            </div>
        </xsl:template>
        <xsl:template match="Report">
                <table border="1">
                    <thead>
                        <tr>
                            <th style="background: #33CCCC" colspan="7">
                                <xsl:value-of select="'Danh sách những mô hình gundam được chọn nhiều nhất'"/>
                            </th>  
                        </tr>
                        <tr>
                            <th style="background:#FFCC00 ">ID Numbers</th>
                            <th style="background:#FFCC00 ">The name of gundam</th>
                            <th style="background:#FFCC00 ">Price</th>
                            <th style="background:#FFCC00 ">Type</th>
                            <th style="background:#FFCC00 ">Page</th>
                            <th style="background:#FFCC00 ">The number of occurrences</th>
                            <th style="background:#FFCC00 ">Number of visits to sales</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="//ModelGundam">
                            <tr>
                                <td>
                                    <xsl:value-of select="id//text()"/>
                                </td>
                                <td>
                                    <xsl:value-of select="gundamName//text()"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
<!--                   <table border="1">
                    <thead>
                        <tr>
                            <th style="background: #CCFFCC" colspan="3">
                                <xsl:value-of select="'Thống kê số lượt click và view đến trang bán hàng trên từng mô hình gundam'"/>
                            </th>  
                        </tr>
                        <tr>     
                            <th style="background:#FFCC00 ">Host name</th>
                            <th style="background:#FFCC00 ">Number of clicks</th>
                            <th style="background:#FFCC00 ">Number of page views</th>     
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="//g:host">
                            <tr>
                                <td>
                                    <xsl:value-of select="g:hostName/text()"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <table border="1">
                    <thead>
                        <tr>
                            <th style="background: #CCFFCC" colspan="2">
                                <xsl:value-of select="'Thống kê số lượt click trên từng kiểu mô hình'"/>

                        </th>  
                    </tr>
                    <tr>
                        <th style="background:#FFCC00 ">Gundam Type</th>
                        <th style="background:#FFCC00 ">Number of clicks</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:for-each select="//g:Type">
                        <tr>
                            <td>
                                <xsl:value-of select="g:typeName/text()"/>
                            </td>
                            <td>
                                <xsl:value-of select="g:clickCount/text()"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>-->
    </xsl:template>

</xsl:stylesheet>
