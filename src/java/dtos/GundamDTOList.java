/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import com.sun.xml.bind.XmlAccessorFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author HiruK
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
    "gundam"
})
@XmlRootElement(name = "gundams")
public class GundamDTOList implements Serializable{
    @XmlElement(required = true)
    private List<GundamDTO>gundam;
    public List<GundamDTO>getGundam(){
        if(gundam==null){
            gundam=new ArrayList<>();
        }
        return gundam;
    }
}
