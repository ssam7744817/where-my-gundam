/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import daos.ClickInfoDAO;
import daos.GundamDAO;
import daos.TopIdDAO;
import daos.ViewDAO;
import dtos.ClickDTO;
import dtos.ClickInfoDTO;
import dtos.GundamDTO;
import dtos.TopIdDTO;
import dtos.ViewDTO;
import generate.report.Report;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author HiruK
 */
public class ExportExcel {

    public void generateExcel(File xmlDoc, String[] tagName, String[] description) {
        try {
            // Creating a workbook
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet spreadSheet = wb.createSheet("spreadSheet");

            spreadSheet.setColumnWidth(0, (256 * 25));
            spreadSheet.setColumnWidth(1, (256 * 25));

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(xmlDoc);
            NodeList nodeList = document.getElementsByTagName("stmt");
            //creating rows
            for (int i = 0; i < tagName.length; i++) {
                HSSFRow row = spreadSheet.createRow(i);
                HSSFCell cellHeader = row.createCell(0);
                //set des
                cellHeader.setCellValue(description[i]);

                //get value and set to cell
                for (int j = 0; j < nodeList.getLength(); j++) {
                    HSSFCell cellValue = row.createCell(j + 1);
                    //set cell value

                    String value = ((Element) (nodeList.item(j))).getElementsByTagName(tagName[i]).item(0).getFirstChild().getTextContent();
                    cellValue.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                    cellValue.setCellValue(value);
                }
            }
            //Output
            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\HiruK\\Documents\\NetBeansProjects\\ExportToExcel\\src\\java\\xls\\IncomeStatementv2.xls"));
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
        }

    }

    /**
     * Đây là hàm sử dụng chính trong project này
     *
     * @param xmlDoc
     * @param tagName
     * @param description
     * @param name
     * @param ouput
     */
    public void generateExcelTest(File xmlDoc, String[] tagName, String[] description, String name, String ouput) {
        try {

            //tạo border
            // Creating a workbook
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet spreadSheet = wb.createSheet("spreadSheet");

            //Tạo ra doc để có thể duyệt cây dom
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlDoc);
            NodeList nodeList = document.getElementsByTagName("stmt");

            int count = 0;
            //row ở vị trí 0 sẽ là tên tiêu đề của bảng với background màu vàng và chữ nằm ở giữa

            HSSFRow Name = spreadSheet.createRow(count);
            Cell lastCell = Name.createCell(0);
            lastCell.setCellValue(name);

            //làm cho tiêu đề thành chữ đậm
            //Lam cho cai tieu de nam o giua và biến background thành màu vàng và chữ bên trong là chữ đậm
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cellStyle.setFont(creatFont(wb, null, Font.BOLDWEIGHT_BOLD));
            lastCell.setCellStyle(cellStyle);//Kết thúc set nền màu

            //tạo cái background màu cho cái columnm
            CellStyle columnCellStyle = wb.createCellStyle();
            columnCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            columnCellStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
            columnCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            columnCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);

            //mergin cho tiêu đề
            spreadSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));

            //Tạo style cho value của count
            CellStyle countStyle = wb.createCellStyle();
            countStyle.setFont(creatFont(wb, HSSFColor.RED.index, Font.BOLDWEIGHT_BOLD));
            countStyle.setAlignment(CellStyle.ALIGN_CENTER);

            //tạo style của toàn bộ cell để nó căn giữa và background màu trắng
            CellStyle center = wb.createCellStyle();
            center.setAlignment(CellStyle.ALIGN_CENTER);

            count++;
            HSSFRow row = spreadSheet.createRow(count);//Row đầu tiên là để tên của các tag
            count++;

            //creating rows
            for (int i = 0; i < tagName.length; i++) {
                HSSFCell cellheader = row.createCell(i);
                cellheader.setCellValue(description[i]);
                cellheader.setCellStyle(columnCellStyle);
            }

            for (int j = 0; j < nodeList.getLength(); j++) {
                HSSFRow rowNode = spreadSheet.createRow(count);
                count++;
                //Vòng lập bên trong để duyệt lại des
                for (int k = 0; k < tagName.length; k++) {
                    String value = ((Element) (nodeList.item(j))).getElementsByTagName(tagName[k]).item(0).getFirstChild().getTextContent();
                    System.out.println("tag name= " + tagName[k]);
                    if (tagName[k].trim().equals("researchandevelopment")) {
                        HSSFCell cell = rowNode.createCell(k);
                        cell.setCellStyle(countStyle);
                        cell.setCellValue(value);
                    } else {
                        HSSFCell cell = rowNode.createCell(k);
                        cell.setCellStyle(center);
                        cell.setCellValue(value);
                    }
//                    spreadSheet.autoSizeColumn(count);
                }

            }
            for (int i = 0; i < tagName.length; i++) {
                spreadSheet.autoSizeColumn(i);

            }

            //Output
            FileOutputStream outputStream = new FileOutputStream(new File(ouput + "\\IncomeStatementv2.xls"));
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
        }

    }

    /**
     * Cũng giống như hàm chính, nhưng bây h ta sẽ lấy dữ liệu từ java object ĐÃ
     * THÀNH CÔNG
     *
     * @param xmlDoc
     * @param tagName
     * @param description
     * @param name
     * @param ouput
     */
//    public boolean generateExcelTestWithJava(List<TopIdDTO> list) {
//        boolean result = true;
//        String[] description = {"ID numbers", "The name of gundam", "Price", "The first day appears", "The number of occurrences"};
//        try {
//
//            //tạo border
//            // Creating a workbook
//            HSSFWorkbook wb = new HSSFWorkbook();
//            HSSFSheet spreadSheet = wb.createSheet("spreadSheet");
//
//            int count = 0;//Hàm count này cho ta vị trí của row, bắt đầu với row =0
//            //row ở vị trí 0 sẽ là tên tiêu đề của bảng với background màu vàng và chữ nằm ở giữa
//
//            HSSFRow Name = spreadSheet.createRow(count);
//            Cell lastCell = Name.createCell(0);
//            lastCell.setCellValue("Danh sách những mô hình gundam được chọn nhiều nhất");
//
//            //làm cho tiêu đề thành chữ đậm
//            //Lam cho cai tieu de nam o giua và biến background thành màu vàng và chữ bên trong là chữ đậm và cỡ chữ 14
//            //tạo riêng 1 cái font cho tên report
//            Font font = wb.createFont();
//            font.setFontHeightInPoints((short) 14);
////            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//
//            CellStyle cellStyle = wb.createCellStyle();
//            cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
//            cellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
//            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
//            cellStyle.setFont(font);
//            lastCell.setCellStyle(cellStyle);//Kết thúc set nền màu
//
//            //tạo cái background màu cho cái columnm
//            CellStyle columnCellStyle = wb.createCellStyle();
//            columnCellStyle.setFont(creatFont(wb, Short.MIN_VALUE, Short.MAX_VALUE));
//            columnCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
//            columnCellStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
//            columnCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
//            columnCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            columnCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//            columnCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            columnCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
//
//            //mergin cho tiêu đề
//            spreadSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
//
//            //Tạo style cho value của count
//            CellStyle countStyle = wb.createCellStyle();
//            countStyle.setFont(creatFont(wb, HSSFColor.RED.index, Font.BOLDWEIGHT_BOLD));
//            countStyle.setAlignment(CellStyle.ALIGN_CENTER);
//
//            //tạo style của toàn bộ cell để nó căn giữa và background màu trắng
//            CellStyle center = wb.createCellStyle();
//            center.setAlignment(CellStyle.ALIGN_CENTER);
//
//            count++;// row= 1
//            HSSFRow row = spreadSheet.createRow(count);//Row đầu tiên là để tên của các tag
//            count++;//row= 2
//
//            //creating rows tiêu đề dựa trên list description
//            for (int i = 0; i < description.length; i++) {
//                HSSFCell cellheader = row.createCell(i);//cell cho header
//                cellheader.setCellValue(description[i]);
//                cellheader.setCellStyle(columnCellStyle);
//            }
//
//            //tạo ra row cho những dữ liệu
//            for (int j = 0; j < list.size(); j++) {
//                //Lấy ra từng object
//                TopIdDTO dto = list.get(j);
//
//                HSSFRow rowNode = spreadSheet.createRow(count);
//                count++;//cứ mỗi 1 object trong list thì sẽ tạo ra 1 row mới
//
//                //Viết như thế này để ta biết đầu ra sẽ đi vào tiêu đề nào
//                for (int k = 0; k < description.length; k++) {
//                    HSSFCell cell = rowNode.createCell(k);
//                    cell.setCellStyle(center); //mặc định sẽ là cell loại center hết
//
//                    switch (description[k]) {
//                        case "ID numbers":
//                            cell.setCellValue(dto.getId());
//                            break;
//                        case "The name of gundam":
//                            cell.setCellValue(dto.getName());
//                            break;
//                        case "Price":
//                            cell.setCellValue(dto.getPrice());
//                            break;
//                        case "The first day appears":
//                            cell.setCellValue(dto.getFirstDateAppearance());
//                            break;
//                        case "The number of occurrences"://Riêng với cell thể hiện lượt click thì ta cho nó thêm màu đỏ
//                            cell.setCellStyle(countStyle);
//                            cell.setCellValue(dto.getNumberOfTimeClicked());
//                            break;
//                    }
//                }
//            }
//
//            //Sau khi đã đổ dữ liệu xong xuôi thì ta dùng lệnh này để format độ dài của column dựa trên cell có nhiều chữ nhất
//            for (int i = 0; i < description.length; i++) {
//                spreadSheet.autoSizeColumn(i);
//
//            }
//
//            //Output ra file cho người dùng
//            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\HiruK\\Desktop\\Report\\Danh sách những gundam hot nhất.xls"));
//            wb.write(outputStream);
//            outputStream.flush();
//            outputStream.close();
//
//        } catch (Exception e) {
//            result = false;
//        }
//        return result; //Nếu true thì quá trình ra file ko có vấn đề
//    }
    /**
     * BÂY H DÙNG HÀM NÀY
     *
     * @param list
     * @return
     */
    public boolean generateExcelTestWithJavaVersion2(HashMap<GundamDTO, Interaction> list) {
        boolean result = true;
//        String[] description = {"ID numbers", "The name of gundam", "Price", "The first day appears", "The number of occurrences"};
        String[] description = {"ID numbers", "The name of gundam", "Price", "Type", "Page", "The number of occurrences", "Number of visits to sales"};

        try {

            //tạo border
            // Creating a workbook
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet spreadSheet = wb.createSheet("spreadSheet");

            int count = 0;//Hàm count này cho ta vị trí của row, bắt đầu với row =0
            //row ở vị trí 0 sẽ là tên tiêu đề của bảng với background màu vàng và chữ nằm ở giữa

            HSSFRow Name = spreadSheet.createRow(count);
            Cell lastCell = Name.createCell(0);
            lastCell.setCellValue("Danh sách những mô hình gundam được chọn nhiều nhất");

            //làm cho tiêu đề thành chữ đậm
            //Lam cho cai tieu de nam o giua và biến background thành màu vàng và chữ bên trong là chữ đậm và cỡ chữ 14
            //tạo riêng 1 cái font cho tên report
            Font font = wb.createFont();
            font.setFontHeightInPoints((short) 14);
//            font.setBoldweight(Font.BOLDWEIGHT_BOLD);

            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            cellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cellStyle.setFont(font);
            lastCell.setCellStyle(cellStyle);//Kết thúc set nền màu

            //tạo cái background màu cho cái columnm
            CellStyle columnCellStyle = wb.createCellStyle();
            columnCellStyle.setFont(creatFont(wb, Short.MIN_VALUE, Short.MAX_VALUE));
            columnCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
            columnCellStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
            columnCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            columnCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            columnCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);

            //mergin cho tiêu đề
            spreadSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));

            //Tạo style cho value của click
            CellStyle countStyle = wb.createCellStyle();
            countStyle.setFont(creatFont(wb, HSSFColor.RED.index, Font.BOLDWEIGHT_BOLD));
            countStyle.setAlignment(CellStyle.ALIGN_CENTER);

            //Tạo style cho value của view
            CellStyle viewStyle = wb.createCellStyle();
            viewStyle.setFont(creatFont(wb, HSSFColor.LIGHT_BLUE.index, Font.BOLDWEIGHT_BOLD));
            viewStyle.setAlignment(CellStyle.ALIGN_CENTER);

            //tạo style của toàn bộ cell để nó căn giữa và background màu trắng
            CellStyle center = wb.createCellStyle();
            center.setAlignment(CellStyle.ALIGN_CENTER);

            count++;// row= 1
            HSSFRow row = spreadSheet.createRow(count);//Row đầu tiên là để tên của các tag
            count++;//row= 2

            //creating rows tiêu đề dựa trên list description
            for (int i = 0; i < description.length; i++) {
                HSSFCell cellheader = row.createCell(i);//cell cho header
                cellheader.setCellValue(description[i]);
                cellheader.setCellStyle(columnCellStyle);
            }

            //tạo ra row cho những dữ liệu
            for (Map.Entry entry : list.entrySet()) {
                //Lấy ra từng object
                GundamDTO dto = (GundamDTO) entry.getKey();
                Interaction value = (Interaction) entry.getValue();

                HSSFRow rowNode = spreadSheet.createRow(count);
                count++;//cứ mỗi 1 object trong list thì sẽ tạo ra 1 row mới

                //Viết như thế này để ta biết đầu ra sẽ đi vào tiêu đề nào
                for (int k = 0; k < description.length; k++) {
                    HSSFCell cell = rowNode.createCell(k);
                    cell.setCellStyle(center); //mặc định sẽ là cell loại center hết

                    switch (description[k]) {
                        case "ID numbers":
                            cell.setCellValue(dto.getId());
                            break;
                        case "The name of gundam":
                            cell.setCellValue(dto.getName());
                            break;
                        case "Price":
                            cell.setCellValue(Integer.valueOf(dto.getPrice()));
                            break;
                        case "Type":
                            cell.setCellValue(dto.getTypeId());
                            break;
                        case "Page":
                            cell.setCellValue(dto.getHostId());
                            break;
                        case "The number of occurrences"://Riêng với cell thể hiện lượt click thì ta cho nó thêm màu đỏ
                            cell.setCellStyle(countStyle);
                            cell.setCellValue(value.getClick());
                            break;
                        case "Number of visits to sales"://Riêng với cell thể hiện lượt click thì ta cho nó thêm màu đỏ
                            cell.setCellStyle(viewStyle);
                            cell.setCellValue(value.getView());
                            break;
                    }
                }

            }

            //Kết thúc phần thống kê trên
            //Phần cho thống kê số lượt click và view đến trang bán hàng từng loại gundam
            HSSFRow rowbottom = spreadSheet.createRow(count + 2);
            int number = rowbottom.getRowNum();
            System.out.println("number= " + number);
            spreadSheet.addMergedRegion(new CellRangeAddress(count + 2, count + 2, 0, 2));

            //tạo style cho cái tiêu đề
            CellStyle thongkeCellStyle = wb.createCellStyle();
            thongkeCellStyle.setFont(creatFont(wb, Short.MIN_VALUE, Short.MAX_VALUE));
            thongkeCellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
            thongkeCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

            HSSFCell cell = rowbottom.createCell(0);
            cell.setCellValue("Thống kê số lượt click và view đến trang bán hàng trên từng mô hình gundam");
            cell.setCellStyle(thongkeCellStyle);

            //Phần header
            number++;
            HSSFRow rowBottomHeade = spreadSheet.createRow(number);
            CellStyle typeBottomStyle = wb.createCellStyle();
            typeBottomStyle.setFont(creatFont(wb, Short.MIN_VALUE, Short.MAX_VALUE));
            typeBottomStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
            typeBottomStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            typeBottomStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            typeBottomStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            typeBottomStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            typeBottomStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);

            String[] arr = new String[]{"Gundam Host", "Number of clicks", "Number of page views"};
            for (int d = 0; d < arr.length; d++) {
                HSSFCell typeHeader = rowBottomHeade.createCell(d);
                typeHeader.setCellValue(arr[d]);
                typeHeader.setCellStyle(typeBottomStyle);
            }

            //Tạo style cho cell tiêu đề
            CellStyle hostHeader = wb.createCellStyle();
            hostHeader.setFont(creatFont(wb, null, Font.BOLDWEIGHT_BOLD));

            //Tạo style cho cái host view value
            CellStyle hostValue = wb.createCellStyle();
            hostValue.setAlignment(CellStyle.ALIGN_LEFT);
            hostValue.setFont(creatFont(wb, HSSFColor.BLUE.index, Font.BOLDWEIGHT_BOLD));

            //Tạo style cho cái host click value
            CellStyle hostClickValue = wb.createCellStyle();
            hostClickValue.setAlignment(CellStyle.ALIGN_LEFT);
            hostClickValue.setFont(creatFont(wb, HSSFColor.RED.index, Font.BOLDWEIGHT_BOLD));

            //Lấy ra thống kê ở lượt view
            ViewDAO dao = new ViewDAO();
            dao.getViewOnEachPage();
            
            //Lấy ra thống kê ở lượt click
            ClickInfoDAO clickDao = new ClickInfoDAO();
            clickDao.getGundamHostStatistics();
            
            //Bỏ dữ liệu vào từng cột tương ứng
//            HashMap<ViewDTO, Integer> listView = dao.getList();
            HashMap<ClickInfoDTO, Integer> listView = clickDao.getHostList();
            for (Map.Entry entry : listView.entrySet()) {
                ClickInfoDTO dto = (ClickInfoDTO) entry.getKey();
                number++;
                HSSFRow next = spreadSheet.createRow(number);
                HSSFCell cell0 = next.createCell(0);
                cell0.setCellStyle(hostHeader);
                HSSFCell cell1 = next.createCell(1);
                cell1.setCellStyle(hostClickValue);
                HSSFCell cell2 = next.createCell(2);

                //Vấn đề của cái này là nếu lượt view chưa xuất hiện thì ko show dữ liệu lên =(
                //Bây h ta đổi ngược lại
                cell0.setCellValue(TypeUtil.convertNumberHostToString(dto.getHost()));
                cell1.setCellValue((int) entry.getValue());
                for (Map.Entry view : dao.getList().entrySet()) {
                    ViewDTO viewDetail = (ViewDTO) view.getKey();
                    if (viewDetail.getHost() == dto.getHost()) {
                        cell2.setCellValue((int) view.getValue());
                        cell2.setCellStyle(hostValue);
                    }
                }
            }
            //Kết thúc phần này

            //Thống kê số số lượt click trên từng loại mô hình gundam
            HSSFRow typeBottom = spreadSheet.createRow(number + 2);
            int numberForType = typeBottom.getRowNum();
            System.out.println("number= " + number);
            spreadSheet.addMergedRegion(new CellRangeAddress(number + 2, number + 2, 0, 1));
            
           

            cell = typeBottom.createCell(0);
            cell.setCellValue("Thống kê số lượt click trên từng kiểu mô hình");
            cell.setCellStyle(thongkeCellStyle);
            
            //Bỏ tiêu đề vào
            numberForType++;
            HSSFRow typeBottomHeade = spreadSheet.createRow(numberForType);
             arr = new String[]{"Gundam Type", "Number of clicks"};
            for (int d = 0; d < arr.length; d++) {
                HSSFCell typeHeader = typeBottomHeade.createCell(d);
                typeHeader.setCellValue(arr[d]);
                typeHeader.setCellStyle(typeBottomStyle);
            }

            //Lấy ra thống kê 
            clickDao.getGundamTypeStatistics();
            HashMap<ClickInfoDTO, Integer> typeList = clickDao.getTypeList();
            for (Map.Entry entry : typeList.entrySet()) {
                ClickInfoDTO dto = (ClickInfoDTO) entry.getKey();
                numberForType++;
                HSSFRow next = spreadSheet.createRow(numberForType);
                HSSFCell cell0 = next.createCell(0);
                cell0.setCellStyle(hostHeader);
                HSSFCell cell1 = next.createCell(1);
                cell1.setCellStyle(hostClickValue);

                cell0.setCellValue(TypeUtil.convertNumberTypeToString(dto.getType()));
                cell1.setCellValue((int) entry.getValue());
            }
            
            //Sau khi đã đổ dữ liệu xong xuôi thì ta dùng lệnh này để format độ dài của column dựa trên cell có nhiều chữ nhất
            for (int i = 0; i < description.length; i++) {
                spreadSheet.autoSizeColumn(i);

            }
            //Output ra file cho người dùng
            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\HiruK\\Desktop\\Report\\Danh sách những gundam hot nhất.xls"));
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }
        return result; //Nếu true thì quá trình ra file ko có vấn đề
    }

    /**
     * tạo kiểu chữ
     *
     * @param font
     * @param wb
     * @param color
     * @param textStyle
     */
    public Font creatFont(Workbook wb, Short color, Short textStyle) {
        Font font = wb.createFont();
        if (color != null) {
            font.setColor(color);
        }
        if (textStyle != null) {
            font.setBoldweight(textStyle);
        }
        return font;
    }

    public void mergingCell() throws FileNotFoundException, IOException {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("new sheet");
        Row row = sheet.createRow(1);
        Cell cell = row.createCell(1);
        cell.setCellValue("This is a test of merging");
        sheet.addMergedRegion(new CellRangeAddress(
                1, //first row (0-based)
                1, //last row  (0-based)
                1, //first column (0-based)
                2 //last column  (0-based)
        ));
// Write the output to a file
        try (OutputStream fileOut = new FileOutputStream(new File("C:\\Users\\HiruK\\Desktop\\Cong thức toán về kiểm định và ước lượng\\IncomeStatementv.xls"))) {
            wb.write(fileOut);
        }

    }

    public void generateExcelTestV2(File xmlDoc, String[] tagName, String[] description, String name) {
        try {
            // Creating a workbook
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet spreadSheet = wb.createSheet("spreadSheet");

            spreadSheet.setColumnWidth(0, (256 * 25));
            spreadSheet.setColumnWidth(1, (256 * 25));

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(xmlDoc);
            NodeList nodeList = document.getElementsByTagName("stmt");
            int count = 0;
            HSSFRow Name = spreadSheet.createRow(count);
            Name.createCell(0).setCellValue(name);
            count++;
            HSSFRow row = spreadSheet.createRow(count);//Row đầu tiên là để tên của các tag
            count++;
            //creating rows
            for (int i = 0; i < tagName.length; i++) {
                HSSFCell cellheader = row.createCell(i);
                cellheader.setCellValue(description[i]);

            }
            for (int j = 0; j < nodeList.getLength(); j++) {
                HSSFRow rowNode = spreadSheet.createRow(count);
                count++;
                //Vòng lập bên trong để duyệt lại des
                for (int k = 0; k < tagName.length; k++) {
                    String value = ((Element) (nodeList.item(j))).getElementsByTagName(tagName[k]).item(0).getFirstChild().getTextContent();
                    rowNode.createCell(k).setCellValue(value);
                }

            }
            //Output
            FileOutputStream outputStream = new FileOutputStream(new File("C:\\Users\\HiruK\\Desktop\\Cong thức toán về kiểm định và ước lượng\\IncomeStatementv2.xls"));
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
        }

    }

    public static void main(String[] args) {
        File xmlDoc = new File("C:\\Users\\HiruK\\Documents\\NetBeansProjects\\ExportToExcel\\src\\java\\xml\\IncomeStatements.xml");
        ExportExcel excel = new ExportExcel();

//        String[] tagsName = {"year", "costofrevenue", "prices", "thefirstdayappear", "researchandevelopment"};
        String[] description = {"ID numbers", "The name of gundam", "Price", "Type", "Host", "The number of occurrences", "Number of visits to sales"};
//        excel.generateExcelTest(xmlDoc, tagsName, description, "Tổng hơp ID", "C:\\Users\\HiruK\\Desktop\\Cong thức toán về kiểm định và ước lượng");
//        System.out.println("Creat ok");

        //Chuẩn bị một java object list
//        TopIdDAO dao = new TopIdDAO();
        GundamDAO dao = new GundamDAO();
        try {
            dao.getListHaveViewAndClick();
            excel.generateExcelTestWithJavaVersion2(dao.getExport());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExportExcel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ExportExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
