/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import daos.GundamDAO;
import daos.GundamInAnimeDAO;
import dtos.GundamInAnimeDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class này sẽ cho ta một hàm để cắt đi cái chuỗi và đưa ra từ khóa tìm kiếm
 * ngắn gọn nhất
 *
 * @author HiruK
 */
public class AdvancedString {

    /**
     * Hàm này sẽ cut string dựa trên khoảng cách
     *
     * @param name
     */
    public static String[] cutString(String name) {
        String[] output = name.split("\\s");
        for (int i = 0; i < output.length; i++) {
            System.out.println("name= " + output[i]);
        }
        return output;
    }

    /**
     * Hàm trả về chuỗi mà khi search sẽ cho ra kết quả ít nhất, cũng đồng nghĩa
     * là kết quả gần chính xác nhất
     *
     * @param strings
     */
    public static String findTheShortKey(String[] strings) {
        GundamDAO dao = new GundamDAO();
        String shortkey = "";
        int number = 0;
        HashMap<String, Integer> map = new HashMap<>();
        Stack<String> stack = new Stack<>();
        /**
         * Cái hashmap này có nhiệm vụ là lưu những search value cho ra kết quả
         * Sau quá trình lọc, ta sẽ lấy ra value nào có ít kết quả nhất để set
         * min
         */
        for (int i = 0; i < strings.length; i++) {

            if (strings[i].equals("(Gundam") || strings[i].equals("Model") || strings[i].endsWith("Kits)")
                    || strings[i].equals("(HG)")
                    || strings[i].equals("(PG)")
                    || strings[i].equals("(RG)")
                    || strings[i].equals("(MG)")
                    || strings[i].equals("(SD)")
                    || strings[i].equals("(1/100)")
                    || strings[i].equals("(1/144)")
                    || strings[i].equals("(1/60)")) {
            } else {
                try {
                    dao.getJaxbList().getGundam().clear();//Làm sạch cái list
                    dao.searchGundam(strings[i]);
                    number = dao.getJaxbList().getGundam().size();//Lấy ra số lượng search đc

                    if (number > 0 && i == 0) {
                        //Nếu kết quả bằng 0 thì khỏi lưu
                        map.put(strings[i], number);
                        stack.push(strings[i]);
                    }

                    System.out.println("size before= " + number + " " + strings[i]);
                    if (number > 0) {//Nếu như số lượng lớn hơn 0 thì tiến hành nối chuỗi để thu nhỏ số lượng lại
                        map.put(strings[i], number);//Kếu quả của chuỗi nối bằng 0 thì cũng ko lưu
                        if (stack.empty()) {
                            shortkey = shortkey + strings[i];
                        } else {
                            shortkey = stack.peek() + " " + strings[i];

                        }

                        dao.getJaxbList().getGundam().clear();//Làm sạch cái list mỗi khi đc search

                        dao.searchGundam(shortkey);
                        number = dao.getJaxbList().getGundam().size();

                        System.out.println("size short= " + number + " " + shortkey);
//                            if(number==0){
//                                stack.pop();
//                            }
                        if (number > 0) {
                            map.put(shortkey, number);//Kếu quả của chuỗi nối bằng 0 thì cũng ko lưu
                            stack.push(shortkey);
                        }

                    }

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AdvancedString.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AdvancedString.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        if (shortkey.equalsIgnoreCase("Gundam")) {
            System.out.println("Đã set null");
            shortkey = null;
        }
        System.out.println("short key lúc này= " + shortkey);
        /**
         * Tìm ra search value có kết quả tìm kiếm thấp nhất
         */
        Entry<String, Integer> min = null;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(" entry key legth = " + entry.getKey().length() + " and value= " + entry.getValue() + " and name= " + entry.getKey());

            if (min == null) {
                if (!entry.getKey().equalsIgnoreCase("gundam")) {
                    System.out.println("Vào null nè ");
                    min = entry;
                    shortkey = min.getKey();
                }
            } else {
                System.out.println("min key lenght= " + min.getKey().length() + " and value= " + min.getValue() + " and name= " + min.getKey());
                if ((min.getValue() >= entry.getValue())) {
                    System.out.println("min before= " + min.getKey());
                    if (entry.getKey().length() > min.getKey().length()) {
                        if (!entry.getKey().equalsIgnoreCase("gundam")) {
                            min = entry;
                        }
                    }
                    if (min.getValue() > 0) {
                        System.out.println("Éo trùng");
                        shortkey = min.getKey();
                    }
                    System.out.println("min after= " + min.getKey());
                }
            }

        }

        return shortkey;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        try {
            List<GundamInAnimeDTO> gundamInAnimeList = new ArrayList<>();
            GundamInAnimeDAO dao = new GundamInAnimeDAO();
            dao.getAllGundamInAnime(gundamInAnimeList);
            for (int i = 0; i < gundamInAnimeList.size(); i++) {
                String[] strings = AdvancedString.cutString(gundamInAnimeList.get(i).getName());
                String shoString = AdvancedString.findTheShortKey(strings);
                if (shoString != null && shoString != "") {
                    /**
                     * Làm thế này để loại những gundam in anime ko có shortkey
                     */
                    dao.insertShortkey(shoString, gundamInAnimeList.get(i).getId());
                    list.add(shoString + " and gundam name= ");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdvancedString.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AdvancedString.class.getName()).log(Level.SEVERE, null, ex);
        }

//        List<String> list = new ArrayList<>();
//            String[] arra = {"OZ-19MASX Gundam Griepe (HG) (Gundam Model Kits)","XXXG-01S2 Gundam Nataku (HG) (Gundam Model Kits)","RX-178+FXA-05D Super Gundam (HGUC) (Gundam Model Kits)"};
////        String[] arra = {"XXXG-01W Wing Gundam EW (RG) (Gundam Model Kits)", "GX-9900-DV Gundam X Divider (Gundam Model Kits)", "(Gundam Model Kits)", "Petitgguy Lockon Stratos Green & Placard (HGPG) (Gundam Model Kits)", "HuangGai Gouf & Six Combining Weapons Set B (SD) (Gundam Model Kits)", "DongZhuo Zaku & BuDuiBing (DongZhuo Forces) (SD) (Gundam Model Kits)", "MS-06S ZAKU II (HGUC) (Gundam Model Kits)", "Full Armor Unicorn Gundam (RG) (Gundam Model Kits)", "RX-178 Mk-II Ver.2.0 Titans (MG) (Gundam Model Kits)"};
////            String[] arra = {"Petitgguy Lockon Stratos Green & Placard (HGPG) (Gundam Model Kits)"};
//        for (int i = 0; i < arra.length; i++) {
//            String[] strings = AdvancedString.cutString(arra[i]);
//            String shoString = AdvancedString.findTheShortKey(strings);
//            if (shoString != null && shoString != "") {
//
//                list.add(shoString + " and gundam name= ");
//            }
//        }
//        for (String strng : list) {
//            System.out.println("This is short key= " + strng);
//        }
    }
}
