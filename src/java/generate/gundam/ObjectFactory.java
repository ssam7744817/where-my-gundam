
package generate.gundam;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generate.gundam package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Gundam_QNAME = new QName("http://xml.netbeans.org/schema/gundam", "gundam");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generate.gundam
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Gundam }
     * 
     */
    public Gundam createGundam() {
        return new Gundam();
    }

    /**
     * Create an instance of {@link Pictures }
     * 
     */
    public Pictures createPictures() {
        return new Pictures();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gundam }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xml.netbeans.org/schema/gundam", name = "gundam")
    public JAXBElement<Gundam> createGundam(Gundam value) {
        return new JAXBElement<Gundam>(_Gundam_QNAME, Gundam.class, null, value);
    }

}
