/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;
import java.sql.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author HiruK
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gundam", propOrder = {
    "name",
    "detailLink",
    "avatar",
    "price",
    "statusId",
    "typeId",
    "hostId",
    "id",})
public class GundamDTO implements Serializable {

    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private String detailLink;

    @XmlElement(required = true)
    private String avatar;

    @XmlElement(required = true)
    private String price;

    @XmlElement(required = true)
    private String statusId;

    @XmlElement(required = true)
    private String typeId;

    @XmlElement(required = true)
    private String hostId;

    @XmlElement(required = true)
    private String id;

  

    public GundamDTO() {
    }

    public GundamDTO(String name, String detailLink, String avatar, String price, String statusId, String typeId, String hostId, String id) {
        this.name = name;
        this.detailLink = detailLink;
        this.avatar = avatar;
        this.price = price;
        this.statusId = statusId;
        this.typeId = typeId;
        this.hostId = hostId;
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the detailLink
     */
    public String getDetailLink() {
        return detailLink;
    }

    /**
     * @param detailLink the detailLink to set
     */
    public void setDetailLink(String detailLink) {
        this.detailLink = detailLink;
    }

    /**
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the statusId
     */
    public String getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the hostId
     */
    public String getHostId() {
        return hostId;
    }

    /**
     * @param hostId the hostId to set
     */
    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

}
