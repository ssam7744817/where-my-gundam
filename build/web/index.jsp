<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
        <title>Home Page</title>

        <style>
            /* Style the body */
            body {
                font-family: Poppins,sans-serif;
               
                margin: 0;
                padding: 0;
            }


            .img {
                float:  left;
                width:  100px;
                height: 100px;
                background-size: cover;




            }
            /*Phần header*/
            .topnav {
                overflow: hidden;
                background-color: #343A40;
            }

            .topnav a {
                float: left;
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }

            .topnav a:hover {
                background-color:#f3f3f3 ;
                color: black;
            }

            .topnav a.active {
                background-color: #2196F3;
                color: white;
            }

            .topnav .search-container {
                float: right;
            }

            .topnav input[type=text] {
                padding: 6px;
                margin-top: 8px;
                font-size: 17px;
                border: none;
            }

            .topnav .search-container button {
                float: right;
                padding: 6px 10px;
                margin-top: 8px;
                margin-right: 16px;
                background: #ddd;
                font-size: 17px;
                border: none;
                cursor: pointer;
            }

            .topnav .search-container button:hover {
                background: green;
            }

            @media screen and (max-width: 600px) {
                .topnav .search-container {
                    float: none;
                }
                .topnav a, .topnav input[type=text], .topnav .search-container button {
                    float: none;
                    display: block;
                    text-align: left;
                    width: 100%;
                    margin: 0;
                    padding: 14px;
                }
                .topnav input[type=text] {
                    border: 1px solid #ccc;  
                }
            }

            /*Phần card*/
            .card {
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                width: 40%;
                border-radius: 5px;
            }

            .card:hover {
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            }

            img {
                border-radius: 5px 5px 0 0;
            }

            .container {
                padding: 2px 16px;
            }
            /*Phần footer*/
            .footer {
                position: relative;
                left: 0;
                bottom: 0;
                height: 50px;
                width: 100%;
                padding-top: 10px;
                background-color: #343A40;
                color: white;
                text-align: center;
            }

            /*Phần container*/
            .container {
                display: flex;
                flex-flow: row wrap;
                position: relative;
            }

            .item {
                flex: 0 1 calc(20% - 8px); /* <-- adjusting for margin */
            }

            /*Đây là phần slide show*/
            * {box-sizing: border-box;}
            body {font-family: Verdana, sans-serif;}
            .mySlides {display: none;}
            img {vertical-align: middle;}

            /* Slideshow container */
            .slideshow-container {
                max-width: 1000px;
                position: relative;
                margin: auto;
            }

            /* Caption text */
            .text {
                color: #f2f2f2;
                font-size: 15px;
                padding: 8px 12px;
                position: absolute;
                bottom: 8px;
                width: 100%;
                text-align: center;
            }

            /* Number text (1/3 etc) */
            .numbertext {
                color: #f2f2f2;
                font-size: 12px;
                padding: 8px 12px;
                position: absolute;
                top: 0;
            }

            /* The dots/bullets/indicators */
            .dot {
                height: 15px;
                width: 15px;
                margin: 0 2px;
                background-color: #bbb;
                border-radius: 50%;
                display: inline-block;
                transition: background-color 0.6s ease;
            }

            .active {
                background-color: #717171;
            }

            /* Fading animation */
            .fade {
                -webkit-animation-name: fade;
                -webkit-animation-duration: 1.5s;
                animation-name: fade;
                animation-duration: 1.5s;
            }

            @-webkit-keyframes fade {
                from {opacity: .4} 
                to {opacity: 1}
            }

            @keyframes fade {
                from {opacity: .4} 
                to {opacity: 1}
            }

            /* On smaller screens, decrease text size */
            @media only screen and (max-width: 300px) {
                .text {font-size: 11px}
            </style>
        </head>

        <body onload="numberWithCommas()">

            <div class="topnav">
                <a href="#">Welcome pilot, choose your Anime</a>
                <div class="search-container" style="display: none">
                    <!--                    <form name="searchForm" action="DispatcherController" style="display: inline-block" onsubmit="return checkValueSearch()">
                                            <input type="text" placeholder="Search.." name="txtSearch">
                                            <input type="hidden" name="txtPage" value="search"/>
                                            <button type="submit" name="btAction" value="Search" class="btn-dark">Search</button>
                    
                                        </form>-->
                    <!--<a href="list.jsp"><img  id="guestlist" src="img/icons8_list.png" style="display: inline-block;width: 30px;height: 30px;margin-right: 10px;color: green"/> </a>-->   
                </div>
            </div>

            <!-- Page Content -->
            <div class="" style="margin-bottom: 20px">

                <div class="row">



                    <div style="margin-left: 10px">
                        <h3 class="my-4">Góc quảng cáo</h3>
                        <p style="font-family: monospace;font-size: 14px">
                            Bạn gặp khó khăn trong việc tìm kiếm những mẫu gundam hot nhất??? Hãy gọi đến chúng tôi thông qua số
                            điện thoại <p style="display: inline-block;font-size: 16px;font-weight: bold">09xxxxx</p>
                        </p>
                    </div>


                    <!--Ý tưởng hồi trước là sẽ show những top gundam ở màn hình chính, BÂY H KO DÙNG NỮA, code cũ lưu trong new text doc(2)-->



                    <!--Bây ha ta sẽ show những anime rồi để người dùng lựa chọn-->
                    <div style="text-align: center">
                        <p><b>----Hãy chọn anime mà bạn biết----</b></p>
                        <c:set var="icon" value=""/>
                        <c:set var="hostName" value=""/>
                        <c:forEach var="item" items="${applicationScope.ANIMES}">
                            <div class="card" style="display: inline-block;width: 200px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #007bff">
                                <div style="text-align: center;width: 200px;height: 200px;padding: 5px">
                                    <c:url var="gotosearch" value="DispatcherController">
                                        <c:param name="avatar" value="${item.avatar}"/>
                                        <c:param name="name" value="${item.name}"/>
                                        <c:param name="animeId" value="${item.id}"/>
                                        <c:param name="btAction" value="Search"/>
                                        <c:param name="anime" value="yes"/>
                                        <c:param name="txtPage" value="search"/>
                                    </c:url>
                                    <a href="${gotosearch}"><img src="${item.avatar}" alt="Avatar" style="width:100%;height: 100%"></a> 
                                </div>
                                <div>
                                    <b><a href="${gotosearch}" style="font-size: 12px;text-decoration: none">${item.name}</a></b>
                                </div>
                            </div>
                        </c:forEach>
                    </div>

                    <!--Mở đầu phần slide show-->
                    <div id="gundamslideshow" style="display: none">
                        <div class="slideshow-container">
                            <div class="mySlides fade">
                                <div class="numbertext">1 / 3</div>
                                <img src="img/1.png" style="width:100%">
                            </div>

                            <div class="mySlides fade">
                                <div class="numbertext">2 / 3</div>
                                <img src="img/2.jpg" style="width:100%">
                            </div>

                            <div class="mySlides fade">
                                <div class="numbertext">3 / 3</div>
                                <img src="img/3.jpg" style="width:100%">
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center">
                            <span class="dot"></span> 
                            <span class="dot"></span> 
                            <span class="dot"></span> 
                        </div>
                    </div>


                    <!--Kết thúc phần slide show-->

                    <!--Phần div gợi ý những mô hình bạn có thể thích-->
                    <div style="text-align: center;margin-top: 50px;display: none">
                        <p><b>----Có thể bạn quan tâm----</b></p>
                    </div>
                    <div style="text-align: center;display: none">
                        <c:set var="icon" value=""/>
                        <c:set var="hostName" value=""/>
                        <c:forEach var="item" items="${applicationScope.TESTTOP}">
                            <c:choose>
                                <c:when test="${item.hostId==2}">
                                    <c:set var="hostName" value="k2tgundam.com"/>
                                    <c:set var="icon" value="${pageContext.request.contextPath}/img/k2t.PNG"/>
                                </c:when>

                                <c:when test="${item.hostId==3}">
                                    <c:set var="hostName" value="ymsgundam.com"/>
                                    <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>
                                </c:when>
                            </c:choose> 
                            <div class="card" style="display: inline-block;width: 200px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #007bff">
                                <div style="text-align: center">
                                    <img src="${item.avatar}" alt="Avatar" style="width:135px">
                                </div>
                                <div>
                                    <b><a style="font-size: 12px;text-decoration: none"href="${item.link}">${item.name}</a></b>
                                    <h5 class="money" style="color: red">${item.price}</h5>
                                </div>
                                <p id="${item.type}" style="display: none"></p>
                                <c:if test="${hostName =='k2tgundam.com' }">
                                    <div style="background-color: #17a2b8;text-align: center;color: white;padding: 5px">
                                        ${hostName}
                                    </div>
                                </c:if>
                                <c:if test="${hostName =='ymsgundam.com' }">
                                    <div style="background-color: indianred;text-align: center;color: white;padding: 5px">
                                        ${hostName}
                                    </div>
                                </c:if>
                            </div>
                        </c:forEach>
                    </div>



                    <!--/.row--> 

                </div>
            </div>

            <div class="footer">
                <p>Copyright &copy; Nguyễn Khắc Sâm 2020</p>
            </div>

        </body>
        <script>

            /**
             * Hàm này sẽ nhận id của cái đc click và loại đc click
             * @param {type} id
             * @param {type} type
             * @returns {undefined}
             */
            function saveView(id, differ) {
                var xmlHttp = new XMLHttpRequest();
                var url = "SaveClickAndViewServlet?id=" + id + "&differ=" + differ+"&type=view";
                xmlHttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        console.log(xmlHttp.responseText);
                    }
                };
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null); //Bắt đầu gửi request đi
            }

            /**
             * Ý tưởng ban đầu là lấy id của những item đc click, rồi 1 cách nào đó đếm số lần id đó xuất hiện
             * rồi query những item đó thành top những item đc tìm kiếm nhiều nhất
             * 
             * Ý tưởng thứ 2 là cứ mỗi lần get 1 id về thì sẽ tạo 1 node id, chuyển nó thành xml
             * 
             * Bây h thì mỗi lần lưu có id mới đc thêm vào thì ta sẽ lưu nó vào local storage
             * 
             * @returns {undefined}
             */
            function getId(ID) {
                var item = document.createElement("item");
                var id = document.createElement("id"); //tạo ra node id
                var dateInXml = document.createElement("date");//tạo ra node date
                var idTextNode = document.createTextNode(ID); //tạo ra text node cho id
                var dateTextNode = document.createTextNode(dateString);//tạo ra text node cho date

                id.appendChild(idTextNode); //gắn value vào node id
                dateInXml.appendChild(dateTextNode);//gắn value vào node date 

                item.appendChild(id);
                item.appendChild(dateInXml);

                root.appendChild(item);


                sendToServlet();//Gửi xml request đến server

                console.log(id.childNodes[0].nodeValue); //Thành công rùi
                console.log("Đây là bên trong local storage= " + localStorage.getItem("idlist"));
            }

            /**
             * Chuyển đổi số thành định dạng đẹp hơn
             * @param {type} x
             * @returns {String}
             */
            function numberWithCommas() {
                var money = document.getElementsByClassName("money");
                for (i = 0; i < money.length; i++) {
                    if (money[i].textContent === "0") {
                        money[i].textContent = "Liên hệ";
                    } else {
                        money[i].textContent = money[i].textContent.replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "đ";
                    }
                }
            }
            /**
             * Hàm này sẽ kiểm tra nội dung search của người dùng, hạn chế search ra quá nhiều record
             * @returns {undefined}
             */
            function checkValueSearch() {
                var searchInput = document.forms["searchForm"]["txtSearch"].value.toLowerCase();
                console.log(searchInput);
                if (searchInput === "gundam") {
                    alert("Bạn đưa ra từ quá rộng, hay cụ thể hơn bằng tên");
                    return false;
                }
            }


            hideAndShowSlide();
            /**
             * Hàm này sẽ show ra cái slide show nếu như ta chưa có top id nào hết
             * ĐÃ THÀNH CÔNG
             * @returns {undefined}
             */
            function hideAndShowSlide() {
                console.log(check);

                var check2 = '${applicationScope.TESTTOP}';

                var slide = document.getElementById("gundamslideshow");
                //Dòng này nghĩa là nếu như ko có top id thì sẽ thay thế bằng slide show này
                if (check2 === "[]") {
                    console.log(slide.style.display);
                    if (slide.style.display === "none") {
                        slide.style.display = "block";
                    }
                } else {
                    console.log("éo rỗng");
                    slide.style.display = "none";
                }
            }
            /**
             * Hàm tạo ra slide show
             * ĐÃ THÀNH CÔNG
             * @type Number
             */
            var slideIndex = 0;
            showSlides();


            function showSlides() {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > slides.length) {
                    slideIndex = 1
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
                setTimeout(showSlides, 3000); // Change image every 2 seconds
            }
        </script>
    </html>
