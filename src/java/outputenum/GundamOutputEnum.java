/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outputenum;

/**
 *
 * @author HiruK
 */
public enum GundamOutputEnum {
    YMS("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputyms.xml"),
    C3("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml"),
    K2T("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputk2t.xml")
    ;
    private String output;

    private GundamOutputEnum(final String value) {
        this.output = value;
    }

    //toString
    public String toString() {
        return output;
    }
}
