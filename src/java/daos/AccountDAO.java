/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.AccountDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author HiruK
 */
public class AccountDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public AccountDTO getUserByLogin(String username, String pass) throws Exception {
        AccountDTO user = new AccountDTO();
        try {
            conn = MyConnection.getConnection();
            preStm = conn.prepareStatement("SELECT \n"
                    + "      [username]\n"
                    + "  FROM [dbo].[account]\n"
                    + "  where name=? and password=?");
            preStm.setString(1, username);
            preStm.setString(2, pass);
            rs = preStm.executeQuery();
            if (rs.next()) {
                user=new AccountDTO();
                user.setUsername(rs.getString("username"));
            }
            return user;
        } finally {
            closeConnection();
        }
    }
}
