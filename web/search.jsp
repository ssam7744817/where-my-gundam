<%-- 
    Document   : search
    Created on : Feb 20, 2020, 8:49:34 PM
    Author     : HiruK
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Search Page</title>

        <style>
            /*Phần header*/

            .topnav {
                overflow: hidden;
                background-color: #343A40;
            }

            .topnav a {
                float: left;
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }

            .topnav a:hover {
                background-color:#f3f3f3 ;
                color: black;
            }

            .topnav a.active {
                background-color: #2196F3;
                color: white;
            }

            .topnav .search-container {
                float: right;
            }

            .topnav input[type=text] {
                padding: 6px;
                margin-top: 8px;
                font-size: 17px;
                border: none;
            }

            .topnav .search-container button {
                float: right;
                padding: 6px 10px;
                margin-top: 8px;
                margin-right: 16px;
                background: #ddd;
                font-size: 17px;
                border: none;
                cursor: pointer;
            }

            .topnav .search-container button:hover {
                background: green;
            }

            @media screen and (max-width: 600px) {
                .topnav .search-container {
                    float: none;
                }
                .topnav a, .topnav input[type=text], .topnav .search-container button {
                    float: none;
                    display: block;
                    text-align: left;
                    width: 100%;
                    margin: 0;
                    padding: 14px;
                }
                .topnav input[type=text] {
                    border: 1px solid #ccc;  
                }
            }

            /*phân body*/
            /* Style the body */
            body {
                font-family: Arial;
                margin: 0;
                padding: 0;
            }


            /*Phần card*/
            .card {
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                transition: 0.3s;
                width: 40%;
                border-radius: 5px;
            }

            .card:hover {
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
            }



            .container {
                padding: 2px 16px;

            }
            .nonActive{ box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);}


            /*Phần chỉnh paging nation*/
            .pagination {
                display: inline-block;
            }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }

            .pagination a.active {
                background-color: #0062cc;
                color: white;
            }

            .pagination a:hover:not(.active) {background-color: #ddd;}

        </style>
        <!--<link href="WEB-INF/css/slideshow.css" rel="stylesheet"/>-->
    </head>
    <body onload="numberWithCommas(), showDefaultBigAvatar()">


        <!--Phần header-->
        <div class="topnav">
            <a class="" href="HomeServlet">Welcome pilot, choose your Gundam</a>
        </div>

        <!--Phần div cho ta biết ta đang search theo hoạt hình nào-->
        <div>
            <div class="card" style="display: inline-block;width: 200px;margin: 10px;border: 1px solid #007bff">
                <div style="text-align: center;width: 200px;height: 200px">
                    <img id="imageA" src="${pageContext.request.contextPath}/${sessionScope.avatar}" alt="Avatar" style="width:100%;height: 100%">
                </div>
                <div style="padding: 5px">
                    <b><a id="nameA" style="font-size: 12px;text-decoration: none">${sessionScope.name}</a></b>
                </div>
            </div>

            <!--Phần div cho ta biết những gundam có trong anime-->
            <div style="vertical-align: top;display: inline-block;margin-top: 10px">
                <c:forEach var="item" items="${requestScope.GUNDAMINANIME}">
                    <div style="margin-right: 10px;width: 100px;height: 90px;display: inline-block">


                        <!--Cách để sử dụng jstl url để viết link 1 cách đơn giản-->

                        <!--                        Biến var: tên của cái link
                                                Biến value: tên của cái servlet mà link này sẽ đến
                                                c:param name: tên của parameter
                                                c:param value: là nơi mình đưa giá trị của param đc khai báo-->
                        
                        
                        <!--Luồng hoạt động của phần logic ăn tiền của project xml bắt đầu ở đây-->
                        
                        <!--(1): Khi người dùng click vào tấm ảnh thì sẽ đc đưa đến trang SearchServlet
                        Cứ đọc theo thứ tự tôi đánh dấu, mấy cái còn lại chỉ là xử lí đơn thuần, ko nên quan
                        tâm để tránh mơ hồ
                        -->
                        
                        
                        <c:url var="auto" value="DispatcherController">
                            <c:param name="txtPage" value="search"/>
                            <c:param name="toppagging" value="${requestScope.currentTopPage}"/>
                            <c:param name="btAction" value="Search"/>
                            <c:param name="autoSearch" value="yes"/>
                            <c:param name="shortkey" value="${item.shortKey}"/>
                            <c:param name="anime" value="yes"/>
                        </c:url>


                        <!--Cái link đc bỏ vào trong ảnh đây
                        Nếu như ko sử dụng c:url thì viết queryString sẽ rất cực
                        Còn nếu dùng thì ta chỉ cần gọi tên nó là xong
                        Tên của cái link là "auto"
                        -->
                        <a href="${auto}"> <img  src="${item.avatar}" style="width: 80%;height: 100%" onclick="saveClick('${item.id}', '', '', 'anime')"/></a>
                        <!--showName('${item.name}'),-->
                        <p style="font-size: 10px">${item.name}</p>
                    </div>
                </c:forEach>
                <!--<b> <p id="nameshow" style="width: max-content;background: gainsboro;padding: 5px;">Name</p></b>-->

                <!--Phần phân trang, ta phải truyền 1 đống tham số như vậy để đảm bảo tính đồng nhất mỗi khi load
                dữ liệu từ server lên, đang ở phân trang nào thì để ở phân trang ấy-->

            </div>

        </div>
        <c:if test="${not empty requestScope.noOfTopPages}">
            <div class="pagination" style="text-align: center;margin-top: 10px">
                <c:url var="animelinkminus" value="DispatcherController">
                    <c:param name="toppagging" value="${currentTopPage - 1}"/>
                    <c:param name="txtPage" value="search"/>
                    <c:param name="btAction" value="Search"/>
                    <c:param name="anime" value="yes"/>
                    <c:param name="txtSearch" value="${param.txtSearch}"/>
                    <c:param name="pagging" value="${requestScope.currentPage}"/>
                    <c:param name="anime" value="yes"/>
                </c:url>
                <c:url var="animelinkplus" value="DispatcherController">
                    <c:param name="toppagging" value="${currentTopPage + 1}"/>
                    <c:param name="txtPage" value="search"/>
                    <c:param name="btAction" value="Search"/>
                    <c:param name="anime" value="yes"/>
                    <c:param name="txtSearch" value="${param.txtSearch}"/>
                    <c:param name="pagging" value="${requestScope.currentPage}"/>
                    <c:param name="anime" value="yes"/>
                </c:url>
                <a href="${animelinkminus}">&laquo;</a>
                <c:forEach begin="1" end="${requestScope.noOfTopPages}" var="i">
                    <c:choose>
                        <c:when test="${requestScope.currentTopPage eq i}">
                            <a class="active">${i}</a>
                        </c:when>
                        <c:otherwise>
                            <c:url var="animelink" value="DispatcherController">
                                <c:param name="toppagging" value="${i}"/>
                                <!--tạo toppaging để khi load lại trang vẫn biết đang ở page số mấy-->
                                <c:param name="txtPage" value="search"/>
                                <c:param name="btAction" value="Search"/>
                                <c:param name="anime" value="yes"/>
                                <c:param name="txtSearch" value="${param.txtSearch}"/>
                                <!--txtSearch cho ta có lại những giá trị đã search-->
                                <c:param name="pagging" value="${requestScope.currentPage}"/>
                                <!--pagging cũng có ý nghĩa tương tự-->
                                <c:param name="anime" value="yes"/>
                            </c:url>
                            <!--<a href="DispatcherController?toppagging=${i}&txtPage=search&btAction=Search&id=${param.id}&anime=yes">${i}</a>-->
                            <a href="${animelink}">${i}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <!--<a href="DispatcherController?toppagging=${currentTopPage + 1}&txtPage=search&btAction=Search&id=${param.id}&anime=yes">&raquo;</a>-->
                <a href="${animelinkplus}">&raquo;</a>
            </div>
        </c:if>

        <c:set var="hostName" value=""/>
        <c:set var="icon" value=""/>
        <!--<input type="button" value="Submit" onclick="sendToServlet()" />-->
        <div class="container" style="margin-bottom: 20px;width: 100%;margin-top: 20px">
            <p>----Tìm chỗ bán----</p>
            <div>
                <c:url var="gosearch" value="DispatcherController">
                    <c:param name="txtSearch" value="${param.txtSearch}"/>
                    <c:param name="txtPage" value="search"/>
                    <c:param name="anime" value="yes"/>
                    <c:param name="toppagging" value="${requestScope.currentTopPage}"/>
                    <c:param name="btAction" value="Search"/>
                </c:url>
                <form name="searchForm" action="DispatcherController" style="display: inline-block">
                    <input type="text" placeholder="Search.." name="txtSearch" style="height: 30px;width: 200px">
                    <input type="hidden" name="txtPage" value="search"/>
                    <input type="hidden" name="anime" value="yes"/>
                    <input type="hidden" name="toppagging" value="${requestScope.currentTopPage}"/>
                    <button type="submit" name="btAction" value="Search" class="btn-dark" style="height: 35px;border: 0;background: green;color: white">Search</button>
                </form>
            </div>
            <!--phần div bên trái-->
            <div style="width: 40%;display: inline-block;vertical-align: top;padding: 20px">
                <c:forEach items="${applicationScope.TYPELIST}" var="type" varStatus="counter">
                    <div class="nonActive" id="${counter.count}" style="padding: 5px;margin-top: 2px;width: 30%">
                        <p id="${counter.count}p" style="font-size: 14px;margin: 0;font-weight: bold" onclick="getActive('${counter.count}'), fillType('${type.name}')">${type.name}</p>
                    </div> 

                </c:forEach>

                <!--Phần dive hiện ảnh và detail-->
                <div id="outsideDetail">

                </div>
            </div>

            <!--Phần div bên phải-->
            <div id="rightdiv" style="width: 50%;display: inline-block;vertical-align: top">
                <c:if test="${empty requestScope.SEARCH}">
                    <c:if test="${not empty requestScope.NOTICE}">
                        <div style="text-align: center">
                            <p style="font-size: 17px;font-weight: bold;color: gray;font-family: monospace">
                                ${requestScope.NOTICE}
                            </p>
                            <img src="img/sad_robot.jpg" style="width: 200px;height: 200px">
                        </div>

                    </c:if>
                </c:if>
                <c:if test="${ not empty requestScope.SEARCH}">
                    <div>
                        <c:set var="icon" value=""/>
                        <c:set var="hostName" value=""/>

                        <c:forEach var="item" items="${requestScope.SEARCH}">
                            <c:choose>
                                <c:when test="${item.hostId==2}">
                                    <c:set var="hostName" value="k2tgundam.com"/>
                                    <c:set var="icon" value="${pageContext.request.contextPath}/img/k2t.PNG"/>

                                </c:when>
                                <c:when test="${item.hostId==3}">
                                    <c:set var="hostName" value="ymsgundam.com"/>
                                    <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>

                                </c:when>
                                <c:when test="${item.hostId==1}">
                                    <c:set var="hostName" value="C3.com"/>
                                    <c:set var="icon" value="${pageContext.request.contextPath}/img/yms.PNG"/>

                                </c:when>
                            </c:choose> 
                            <div class="cardsearch" style="display: inline-block;width: 200px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #007bff">
                                <div style="text-align: center">
                                    <img src="${item.avatar}" alt="Avatar" style="width:135px">
                                </div>
                                <div class="container">
                                    <c:url var="detaillink" value="GetDetailImageServlet">
                                        <c:param name="gundamId" value="${item.id}"/>
                                        <%--<c:param name="txtSearch" value="${param.txtSearch}"/>--%>
                                        <%--<c:param name="txtPage" value="search"/>--%>
                                        <%--<c:param name="pagging" value="${requestScope.currentPage}"/>, ko cần 2 cái này nữa vì mình ko load trang, ko làm mất phân trang--%>
                                        <%--<c:param name="toppagging" value="${requestScope.currentTopPage}"/>--%>
                                        <%--<c:param name="anime" value="yes"/>--%>
                                    </c:url>
                                    <!--<b><a style="font-size: 12px;text-decoration: none;margin: 0" href="GetDetailImageServlet?gundamId=${item.id}&txtSearch=${param.txtSearch}&txtPage=search&pagging=${requestScope.currentPage}&anime=yes">${item.name}</a></b>-->
                                    <c:url  var="save" value="SaveClickAndViewServlet">
                                        <c:param name="id" value="${item.id}"/>
                                        <c:param name="type" value="click"/>
                                        <c:param name="gundamtype" value="${item.typeId}"/>
                                        <c:param name="gundamhost" value="${item.hostId}"/>
                                        <!--var url = "SaveClickAndViewServlet?id=" + id+"&type="+type"&gundamtype="+gundamtype+"&gundamhost="+gundamhost;-->

                                    </c:url>
                                    <!--<b><a style="font-size: 12px;text-decoration: none;margin: 0" href="${detaillink}" onclick="saveClick('${item.id}','click','${item.typeId}','${item.hostId}')">${item.name}</a></b>-->
                                    <b><a style="font-size: 12px;text-decoration: none;margin: 0" href="#" onclick="saveClick('${item.id}', '${item.typeId}', '${item.hostId}', 'shop')">${item.name}</a></b>



                                    <!--Khi ta nhấn vào tên của gundam ta sẽ xem đc chi tiết-->
                                    <h5 class="money" style="color: red;margin: 0;margin-top: 5px;margin-bottom: 10px">${item.price}</h5>
                                </div>
                                <p id="${item.typeId}" style="display: none"></p>
                                <c:if test="${hostName =='k2tgundam.com' }">
                                    <div style="background-color: #17a2b8;text-align: center;color: white;padding: 5px">
                                        ${hostName}
                                    </div>
                                </c:if>
                                <c:if test="${hostName =='ymsgundam.com' }">
                                    <div style="background-color: indianred;text-align: center;color: white;padding: 5px">
                                        ${hostName}
                                    </div>
                                </c:if>
                                <c:if test="${hostName =='C3.com' }">
                                    <div style="background-color: #009966;text-align: left;color: white;padding: 5px">
                                        ${hostName}
                                    </div>
                                </c:if>
                            </div>
                        </c:forEach>
                        <!--Phần phân trang-->

                        <c:if test="${not empty requestScope.noOfPages}">
                            <div class="pagination" style="text-align: center">
                                <c:url var="pagelinkminus" value="DispatcherController">
                                    <c:param value="${currentPage - 1}" name="pagging"/>
                                    <c:param value="${requestScope.currentTopPage}" name="toppagging"/>
                                    <c:param value="search" name="txtPage"/>
                                    <c:param value="Search" name="btAction"/>
                                    <c:param value="yes" name="anime"/>
                                    <c:param name="autoSearch" value="yes"/>
                                    <c:param name="shortkey" value="${requestScope.shortkey}"/>
                                    <c:param value="${param.txtSearch}" name="txtSearch"/>
                                </c:url>
                                <c:url var="pagelinkadd" value="DispatcherController">
                                    <c:param value="${currentPage + 1}" name="pagging"/>
                                    <c:param value="${requestScope.currentTopPage}" name="toppagging"/>
                                    <c:param value="search" name="txtPage"/>
                                    <c:param value="Search" name="btAction"/>
                                    <c:param name="autoSearch" value="yes"/>
                                    <c:param name="shortkey" value="${requestScope.shortkey}"/>
                                    <c:param value="yes" name="anime"/>
                                    <c:param value="${param.txtSearch}" name="txtSearch"/>
                                </c:url>
                                <!--<a href="DispatcherController?pagging=${currentPage - 1}&txtPage=search&btAction=Search&txtSearch=${param.txtSearch}">&laquo;</a>-->
                                <a href="${pagelinkminus}">&laquo;</a>
                                <c:forEach begin="1" end="${requestScope.noOfPages}" var="i">
                                    <c:choose>
                                        <c:when test="${requestScope.currentPage eq i}">
                                            <a class="active">${i}</a>
                                        </c:when>
                                        <c:otherwise>
                                            <!--<a href="DispatcherController?pagging=${i}&txtPage=search&btAction=Search&txtSearch=${param.txtSearch}">${i}</a>-->
                                            <c:url var="pagelinknonactive" value="DispatcherController">
                                                <c:param value="${i}" name="pagging"/>
                                                <c:param value="${requestScope.currentTopPage}" name="toppagging"/>
                                                <c:param value="search" name="txtPage"/>
                                                <c:param value="Search" name="btAction"/>
                                                <c:param name="autoSearch" value="yes"/>
                                                <c:param name="shortkey" value="${requestScope.shortkey}"/>
                                                <c:param value="yes" name="anime"/>
                                                <c:param value="${param.txtSearch}" name="txtSearch"/>
                                            </c:url>
                                            <a href="${pagelinknonactive}">${i}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <!--<a href="DispatcherController?pagging=${currentPage + 1}&txtPage=search&btAction=Search&txtSearch=${param.txtSearch}">&raquo;</a>-->
                                <a href="${pagelinkadd}">&raquo;</a>
                            </div>
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${ empty requestScope.SEARCH}">
                    <c:if test="${not empty requestScope.EMPTY}">
                        <div style="text-align: center">
                            <p style="font-size: 17px;font-weight: bold;color: gray;font-family: monospace">
                                ${requestScope.EMPTY}
                            </p>
                            <img src="img/sad_robot.jpg" style="width: 200px;height: 200px">
                        </div>
                    </c:if>
                </c:if>
            </div>

        </div>
    </body>
    <script>




        /**
         * Biến search thành xmlHttpRequest,KO DÙNG HÀM NÀY
         * @param {type} url
         * @returns {undefined}
         */
        function goGetDeitalImage(urlsearch) {
            var xmlHttp = new XMLHttpRequest();
            var doc;
            var url = urlsearch;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    doc = xmlHttp.responseXML;
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null); //Bắt đầu gửi request đi
        }

        /**
         * Hàm gửi 1 xmlhttpRequest yêu cầu server trả về 1 DOMResult để render( từ render ám chỉ việc tạo giao diện thôi, chả to tát gì cả
         * nếu như ta phải viết giao diện bằng html thì dùng cách nào ta sẽ viết giao diện bằng javascript) giao diện
         * @param {type} id: id của sản phầm
         * @param {type} gundamtype: loại mô hình gundam
         * @param {type} gundamhost: trang bán mô hình gundam
         * @@param {type} differ: loại gundam trong shop hoặc anime 
         * @returns {undefined}
         */
        function saveClick(id, gundamtype, gundamhost, differ) {
            var xmlHttp = new XMLHttpRequest();//tạo biến XMLHttpRequest
            
            //tạo ra url, nơi mà xử lí requset này, cách viết của nó giống như viết một queryString
            var url = "SaveClickAndViewServlet?id=" + id + "&interactionType=click" + "&gundamtype=" + gundamtype + "&gundamhost=" + gundamhost + "&differ=" + differ;

            xmlHttp.open("GET", url, true);//Khi kết thúc dòng này thì ta quay lại trang saveClickAndView 1 tí và đọc bắt đầu từ chỗ có chữ "tiếp tục"
            
            //Sau khi đọc xong trang saveCLickAndView rồi thì đây là bước ta nhận kết quả
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {//Phần if này kiểm tra xem status của request này đến server thế nào, nói chung cứ dùng thôi, trăm hàm như một
                    //Kết quả nhận về server thì chi chấp nhận 2 loại là responseXML và respnoseText
                    //Trong project này thì dùng responseText vì nó ít lỗi, dễ kiểm tra và nó chỉ là 1 chuỗi string
                    if (differ === "shop") {//dòng if này chỉ là phần xử lí trang web của tôi, ko cần quan tâm
                        console.log(xmlHttp.responseText);//Dòng này tôi dùng để test coi kết quả có đúng ý tôi ko
                        
                        /**
                         * xmlHttp.responseText chính là cái string có tên resultString mà ta xử lí hồi nãy
                         * 
                         * @type Element 
                         */
                        
                        
                        
                        //Phần dùng cái string trả về để render lên giao diện
                        var outside = document.getElementById("outsideDetail");

                        removeAllResultSearch(outside);//Hàm này dùng để clear giao diện cũ, chỉ cần chép lại y chang, ko cần hiểu
                        
                        /**
                         * bước cuối trong việc gắn xml lên trang jsp để nó render thành giao diện
                         * Để hiểu bước này thì phải hiểu rõ DOM, học kĩ bài đó
                         */
                        outside.innerHTML = xmlHttp.responseText;
//                        outside.appendChild(inside);
                    }
                }
                ;
            }
            xmlHttp.send(null); //Bắt đầu gửi request đi, tham số luôn là null nếu ta gửi kiểu GET, POST thì khác 1 tí nhưng tôi ko dùng
        }
        /**
         * Hàm lấy xsl lên client, KO DÙNG HÀM NÀY NỮA
         */
        function getXsl(xml) {
            var xmlHttp = new XMLHttpRequest();
            var url = "loitest.xsl";

            xmlHttp.open("GET", url, true);
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    var outside = document.getElementById("outsideDetail");
                    var xsl = xmlHttp.responseXML;

                    var xsltProcessor = new XSLTProcessor();
                    xsltProcessor.importStylesheet(xsl);
                    var resultHtml = xsltProcessor.transformToFragment(xml, document);//render xml đã apply stylesheet thành html

                    removeAllResultSearch(outside);//This function will remove old result
                    outside.appendChild(resultHtml);

                }
                ;
            }
            xmlHttp.send(null); //Bắt đầu gửi request đi
        }


        /**
         * Hàm dùng để xóa mấy cái node cũ để mấy cái node mới đi vào,KO DÙNG HÀM NÀY NỮA
         * @param {type} div
         * @returns {undefined}
         */
        function removeAllResultSearch(div) {
            let child = div.lastElementChild;
            while (child) {
                div.removeChild(child);
                child = div.lastElementChild;
            }
        }

        /**
         * Hàm này sẽ nhận id của cái đc click và loại đc click
         * @param {type} id
         * @param {type} type
         * @returns {undefined}
         */
//        function saveView(realUrl) {
        function saveView(id, gundamhost) {
            var xmlHttp = new XMLHttpRequest();
            var url = "SaveClickAndViewServlet?id=" + id + "&differ=shop" + "&interactionType=view" + "&gundamhost=" + gundamhost;
//            var url = realUrl;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(xmlHttp.responseText);
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null); //Bắt đầu gửi request đi
        }



        /**
         * Hàm này sẽ lưu lại tên và ảnh của anime để mỗi khi load trong nó sẽ ko bị mất
         * @returns {undefined}
         */
//       function reloadBig(){
//           console.log("reload big")
//           var animeName='<c:out value="${param.name}"/>';
//           var avatar='${pageContext.request.contextPath}'+'/'+'<c:out value="${param.avatar}"/>';
//           console.log(avatar);
//           if(animeName==="" && avatar==="" ){
//               localStorage.setItem('name',animeName);
//               localStorage.setItem('avatar',avatar);
//           }
//           document.getElementById("imageA").src=localStorage.getItem("avatar");
//           document.getElementById("nameA").textContent=localStorage.getItem("name");
//       }

        /**
         * Hàm này sẽ show ra tên của mối tấm hình để người dùng dựa vào đó để search tên,KO DÙNG NỮA 
         * @param {type} id
         * @param {type} name
         * @returns {undefined}
         */
        function showName(name) {
            var picturename = document.getElementById("nameshow");
            picturename.textContent = name;
        }

        /**
         * Bây h hàm này sẽ ajax, KO DÙNG HÀM NÀY
         * @returns {undefined}
         */
        function loadInfoInAvatar(id, search) {
            var go = document.getElementById("getLink");
            var id = '${item.id}';
            var search = '${param.txtSearch}';
            go.href = "GetDetailImageServlet?id=" + id + "&txtSearch=" + search;
            document.getElementById("gundamName").innerHTML = "địt mẹ cược đười";

        }

        /**
         * Hàm này có tác dụng show tấm hình đầu tiên lên thành tấm bự nhất khi click vào
         * tên của mô hình gundam ở "Phần div bên phải"
         * @returns {undefined}
         */
        function showDefaultBigAvatar() {
            var small = document.getElementById("smallAvatar");
            var big = document.getElementById("bigAvatar");
            big.src = small.src;
        }
        /**
         * Hàm này có tách dụng khi ta bấm vào hình nhỏ thì hình lớn sẽ show ra, KO DÙNG HÀM NÀY
         * @returns {undefined}
         */
        function showBigAvatar(linkpicture) {
            var big = document.getElementById("bigAvatar");
            big.src = linkpicture;
        }


        /**
         * Hàm này sẽ kiểm tra nội dung search của người dùng, hạn chế search ra quá nhiều record
         * @returns {undefined}
         */
        function checkValueSearch() {
            var searchInput = document.forms["searchForm"]["txtSearch"].value;
            alert("Bạn đưa ra từ quá rộng, hay cụ thể hơn bằng tên");
            return false;
        }

        var flag
        /**
         * Hàm này sẽ đưa cái div đc chọn trở thành active để người dùng dễ nhìn 
         * @returns {undefined}
         */
        function getActive(id) {

            var div = document.getElementById(id);
            var allDiv = document.getElementsByClassName("nonActive");
            //Nếu như div đc click có class trùng với mấy 
            for (i = 0; i < allDiv.length; i++) {
                console.log(allDiv[i].className);
                if (div.id === allDiv[i].id) {
                    div.style.backgroundColor = "#0062cc";
                } else {
                    allDiv[i].style.backgroundColor = "#fefefe";
                }
            }

        }

        /**
         * Hàm này dùng để fill ra loại gundam mà người dùng muốn tìm kiếm
         * @returns {undefined}
         */
        function fillType(typeid) {
            console.log(typeid);
            //Lấy ra tất cả cái card
            var card = document.getElementsByClassName("cardsearch");
            console.log(card.length);
            //nếu card lấy ra mà có type giống thì show, còn ko thì giấu nó đi
            for (i = 0; i < card.length; i++) {
                var p = card[i].getElementsByTagName("p"); //lấy ra node p từ card
                if (typeid.trim() !== p[0].id) {//ta thêm trim() để loại bỏ hết khoảng trắng, vì chỉ có 1 p nên ta viết p[0] là ra
                    card[i].style.background = "#f5e79e";
                    card[i].style.background = "#fefefe";
                    //thay đổi thêm màu border cho dễ nhìn
                    card[i].style.border = "1px solid #007bff"; //màu border lúc này sẽ trùng với

                } else {
                    card[i].style.background = "#f5e79e";
                    card[i].style.border = "1px solid #f5e79e";
                }
            }
        }

        /**
         * ý tưởng hồi đó là sẽ bắt sự kiện chuyển trang, load lại trang hay đóng trang thì mới gửi xml xuông
         * Bây h ta ko dùng nó nũa, cứ 10 request thì ta sẽ thực hiện lưu xuống data base 1 lần
         * KO DÙNG HÀM NÀY NỮA
         * @param {type} e
         * @returns {undefined}
         */
        function ConfirmClose(e)
        {
            var evtobj = window.event ? event : e;
            if (evtobj == e)
            {
                //firefox
                //                    alert(e.clientX + ' * ' + e.clientY); You will find both the value is undefined.
                if (!evtobj.clientY)
                {
                    sendToServlet();
                    return;
                }
            } else
            {
                //IE
                //alert(e.clientX + ' * ' + e.clientY); The value will be your mouse last position.
                if (evtobj.clientY < 0)
                {
                    //                       sendToServlet();
                }
            }
        }



        window.localStorage.removeItem("idlist"); //Clear cái list id cũ trong localstorage đi
        //Tạo ra 1 node root
        var root = document.createElement("items");
        var xmlhttp = new getXmlHttpRequestObject(); //xmlhttp holds the ajax object
        var xhttp = new XMLHttpRequest();
        var id = document.createElement("id"); //tạo ra node id

        /**
         * Tạo ra 1 biến để lấy ngày và tháng
         * @param {type} ID
         * @returns {undefined}
         */
        var date = new Date();
        var dateString = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        /**
         * Ý tưởng ban đầu là lấy id của những item đc click, rồi 1 cách nào đó đếm số lần id đó xuất hiện
         * rồi query những item đó thành top những item đc tìm kiếm nhiều nhất
         * 
         * Ý tưởng thứ 2 là cứ mỗi lần get 1 id về thì sẽ tạo 1 node id, chuyển nó thành xml
         * 
         * Bây h thì mỗi lần lưu có id mới đc thêm vào thì ta sẽ lưu nó vào local storage
         * KO DÙNG HÀM NÀY NỮA
         * @returns {undefined}
         */
        function getId(ID) {
            var item = document.createElement("item");
            var id = document.createElement("id"); //tạo ra node id
            var dateInXml = document.createElement("date"); //tạo ra node date
            var idTextNode = document.createTextNode(ID); //tạo ra text node cho id
            var dateTextNode = document.createTextNode(dateString); //tạo ra text node cho date

            id.appendChild(idTextNode); //gắn value vào node id
            dateInXml.appendChild(dateTextNode); //gắn value vào node date 

            item.appendChild(id);
            item.appendChild(dateInXml);
            root.appendChild(item);
            sendToServlet(); //Gửi xml request đến server

            console.log(id.childNodes[0].nodeValue); //Thành công rùi
        }

        /**
         * Đã thành công =)_
         * @returns {undefined}
         */
        function sendToServlet() {
            if (xmlhttp) {
                xmlhttp.open("POST", "GetTheTopIDServlet", true);
                xmlhttp.onreadystatechange = handleServletPost;
                xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xmlhttp.send("idGet=" + root.outerHTML);
            }
        }


        /**
         * hàm tạo ra 1 XMLHttpRequest
         * @returns {XMLHttpRequest|ActiveXObject}
         */
        function getXmlHttpRequestObject()
        {
            var xmlHttp = false;
            if (window.XMLHttpRequest)
            {
                return new XMLHttpRequest(); //To support the browsers IE7+, Firefox, Chrome, Opera, Safari
            } else if (window.ActiveXObject)
            {
                return new ActiveXObject("Microsoft.XMLHTTP"); // For the browsers IE6, IE5 
            } else
            {
                alert("Error due to old verion of browser upgrade your browser");
            }
        }

        function handleServletPost() {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    //                        document.getElementById("message").innerHTML = xmlhttp.responseText;
                    console.log("THhis is OK");
                } else {
                    alert("Ajax calling error");
                }
            }
        }
        /**
         * Chuyển đổi số thành định dạng đẹp hơn
         * @param {type} x
         * @returns {String}
         */
        function numberWithCommas() {
            var money = document.getElementsByClassName("money");
            for (i = 0; i < money.length; i++) {
                if (money[i].textContent === "0") {
                    money[i].textContent = "Liên hệ";
                } else {
                    money[i].textContent = money[i].textContent.replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "đ";
                }
            }
        }
    </script>
    <script>
        /**
         * Hàm lấy những hình ảnh dựa trên id,KO DÙNG HÀM NÀY NỮA
         * Vì mấy cái xmlHttpRequest chỉ phù hợp với các loại công việc lưu xuống
         * Còn móc từ database lên nên load lại trang
         * @param {type} id
         * @returns {undefined}
         */
        function showImage(id) {
            var xmlHttp = new XMLHttpRequest();
            var url = "GetDetailImageServlet?id=" + id;
            xmlHttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(xmlHttp.responseText);
                }
            };
            xmlHttp.open("GET", url, true);
            xmlHttp.send(null); //Bắt đầu gửi request đi
        }
    </script>
</html>
