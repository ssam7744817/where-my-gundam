<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : gundam.xsl
    Created on : February 5, 2020, 10:58 AM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:t="http://xml.netbeans.org/schema/gundams"
                xmlns="http://xml.netbeans.org/schema/gundams"
                xmlns:xh="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="t"
>
    <xsl:output method="xml" omit-xml-declaration="yes" indent="no"/> 
    <xsl:template match="t:gundams">
        <xsl:variable name="listDoc" select="document(t:link//@hr)"/>
        <xsl:variable name="host" select="t:link//@host"/>
        <xsl:element name="gundams">
            
            <!--Lấy địa chỉ từng loại gundam_Bước số 1-->
            <xsl:for-each select="$listDoc//div[@id='menu']//li">
                <xsl:if test="contains(a//text(),'Bandai')">
                    <xsl:for-each select="div[@class='ab']//ul//li">
                        <xsl:variable name="firstLink" select="a//@href"/>
                        
                        <!--Lấy dữ liệu ở link đầu tiên-->
                        <xsl:call-template name="getGundamInfo">
                            <xsl:with-param name="hostName" select="$host"/>
                            <xsl:with-param name="imageLink" select="document($firstLink)"/>
                        </xsl:call-template>
                        
                        <!--Lấy dữ liệu ở link thứ 2,là những link nằm ở phân trang-->
                        <xsl:for-each select="document($firstLink)//div[@class='pagination']//div[@class='links']//a">
                            <xsl:variable name="page" select="@href"/>
                            <xsl:choose>
                                <xsl:when test=".//text()='&amp;gt;'">
                                </xsl:when>
                                <xsl:when test=".//text()='&amp;gt;|'">
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="getGundamInfo">
                                        <xsl:with-param name="hostName" select="$host"/>
                                        <xsl:with-param name="imageLink" select="document($page)"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
        </xsl:element>
        <!--Dùng hàm document với tham số đầu vào là attribute link của file gundam.xml-->
    </xsl:template>
    
    <xsl:template name="getGundamInfo">
        <xsl:param name="imageLink"/>
        <xsl:param name="hostName"/>
        <xsl:for-each select="$imageLink//div">
            <xsl:if test="div[@class='image']">
                <xsl:variable name="info" select="div[@class='image']"/>
                <xsl:variable name="price" select="div[@class='price']"/>
                <xsl:element name="gundam">
                    <xsl:if test="$info//a//img//@title!=''">
                        <xsl:element name="name">
                            <xsl:choose>
                                <xsl:when test="contains($info//a//@href,'hg')">
                                    <xsl:value-of select="concat('HG 1/144 ',$info//a//img//@title)"/>
                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'rg')">
                                    <xsl:value-of select="concat('RG 1/144 ',$info//a//img//@title)"/>
                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'mg')">
                                    <xsl:value-of select="concat('MG 1/100 ',$info//a//img//@title)"/>
                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'bb')">
                                    <xsl:value-of select="concat('SD ',$info//a//img//@title)"/>
                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'pg')">
                                    <xsl:value-of select="concat('PG 1/60 ',$info//a//img//@title)"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:element>
                   
                        <xsl:element name="detailLink">
                            <xsl:value-of select="$info//a//@href"/>
                        </xsl:element>
                        
                        <!--Bây h ta sẽ vào link chi tiết từng cái để lấy ảnh siêu đẹp-->
                        <xsl:element name="avatar">
                            <xsl:value-of select="$info//a//img//@src"/>
                        </xsl:element>

                        <xsl:element name="pictures">
                            <xsl:call-template name="getDetail">
                                <xsl:with-param name="detailLink" select="document($info//a//@href)"/>
                            </xsl:call-template>
                        </xsl:element>
                
                        <xsl:element name="price">
                            <xsl:choose>
                                <!--Kiểm tra xem có new price ko, có thì lấy-->
                                <xsl:when test="$price//span[@class='price-new']">
                                    <xsl:value-of select=".//span[@class='price-new']"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$price//text()"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:element>
                        <xsl:element name="status">
                            <xsl:if test="$info//a//span[@class='horizontal bottom_left small-db outofstock']">
                                <xsl:value-of select=".//text()"/>
                            </xsl:if>
                        </xsl:element>
                        <xsl:element name="type">
                            <xsl:choose>
                                <xsl:when test="contains($info//a//@href,'hg')">
                                    <xsl:text>HG</xsl:text>
                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'rg')">
                                    <xsl:text>RG</xsl:text>

                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'mg')">
                                    <xsl:text>MG</xsl:text>

                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'bb')">
                                    <xsl:text>SD</xsl:text>

                                </xsl:when>
                                <xsl:when test="contains($info//a//@href,'pg')">
                                    <xsl:text>PG</xsl:text>

                                </xsl:when>
                            </xsl:choose>
                        </xsl:element>
                        <xsl:element name="host">
                            <xsl:value-of select="$hostName"/>
                        </xsl:element>
                    </xsl:if>
                </xsl:element> 
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <!--Lấy avatar loại lớn, KO DÙNG NỮA-->
    <xsl:template name="getBigAvatar">
        <xsl:param name="detailLink"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='left']//div[@class='image']//a//img//@src"/>
        <xsl:value-of select="$outside"/>
    </xsl:template>
    
    <!--Ta sẽ lấy ảnh chi tiết cho từng loại gundam-->
    <xsl:template name="getDetail">
        <xsl:param name="detailLink"/>
        <xsl:variable name="bigAvatar" select="$detailLink//div[@class='left']//div[@class='image']//a//img//@src"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='image-additional']//a"/>
        <xsl:for-each select="$outside">
            <xsl:element name="picture">
                <xsl:value-of select="@href"/>
            </xsl:element>

        </xsl:for-each>
        <xsl:element name="picture">
            <xsl:value-of select="$bigAvatar"/>
        </xsl:element>
        
    </xsl:template>
    
</xsl:stylesheet>
