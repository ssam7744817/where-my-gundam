/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;

/**
 *
 * @author HiruK
 */
public class ClickInfoDTO implements Serializable{
    private int gundaminanimeid,type,host;
            private String gundamid;

    public ClickInfoDTO(int gundaminanimeid, String gundamid, int type, int host) {
        this.gundaminanimeid = gundaminanimeid;
        this.gundamid = gundamid;
        this.type = type;
        this.host = host;
    }

    public ClickInfoDTO() {
    }

    public int getGundaminanimeid() {
        return gundaminanimeid;
    }

    public void setGundaminanimeid(int gundaminanimeid) {
        this.gundaminanimeid = gundaminanimeid;
    }

    public String getGundamid() {
        return gundamid;
    }

    public void setGundamid(String gundamid) {
        this.gundamid = gundamid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getHost() {
        return host;
    }

    public void setHost(int host) {
        this.host = host;
    }
    
}
