/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.validation;

import constant.Constant;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import crawler.utils.XMLUtils;
import generate.gundams.Gundams;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.InputSource;

/**
 *
 * @author HiruK
 */
public class XMLValidate implements Serializable {

    static XPathFactory xdf = XPathFactory.newInstance();
    static XPath xpath = xdf.newXPath();
    public String c3Gundam = "https://c3gundam.com/";
    public String k2tGundam = "http://k2tgundam.com/";
    public String ymsGundam = "http://ymsgundam.com/";

    /**
     * Tham số là file xml cần đc validate
     *
     * @param output
     */
    public void prepareGundamObject(String output) {
        try {

            Document doc = XMLUtils.paserFileToDom(output);
            //Tạo 1 file doc đầu vào để xử lý, cụ thể ở đây là file xml có từ crawl nhưng dữ liệu chưa đc forrmat
            /**
             * lấy node có tên là gundam, phần //* là của wildcard, phần
             * [local-name()='gundam'] là điều kiện
             */
            String expression = "//*[local-name()='gundam']";
            //local name dùng để trả về node trùng với tên đc đưa ra
            //ta sử dụng local-name vì nhiều khi có nhiều tài liệu xml
            //kết hợp với nhau, mỗi cái có prefix riêng nhưng các node
//            có nhiệm vụ giống nhau như node "name" để chứa name
//                    ta dùng local-name để lấy hết tất cả node name trong tất cả
//                            file xml
            int count = 0;//biến để kiểm tra bao nhiêu node bị xóa
            String priceExpression = "*[local-name()='price']";
            String nameExpression = "*[local-name()='name']";
            String hostExpression = "*[local-name()='host']";
            String statusExpression = "*[local-name()='status']";
            String typeExpression = "*[local-name()='type']";//Lấy tất type element
            NodeList nodeList = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);
            System.out.println("List before: " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                //xóa node ko có dữ liệu
                Node node = nodeList.item(i);
                Node parent = node.getParentNode();
                if (node.hasChildNodes() == false) {
                    parent.removeChild(node);
                    count++;
                } else {
//                    Nếu như element đầy đủ thì kiểm tra type có rỗng ko, nếu rỗng thì bỏ đi
                    Node typeNode = (Node) xpath.evaluate(typeExpression, node, XPathConstants.NODE);
                    if (typeNode.getTextContent().equals("")) {
                        parent.removeChild(node);
                        count++;
                    } else {
//                        Nếu như type có value rồi thì format lại số cho đẹp
                        Node childNodeOfGundam = (Node) xpath.evaluate(priceExpression, node, XPathConstants.NODE);
                        Node hostNode = (Node) xpath.evaluate(hostExpression, node, XPathConstants.NODE);
                        Node statusNode = (Node) xpath.evaluate(statusExpression, node, XPathConstants.NODE);
                        //Bước làm đẹp lại số cho price
                        XMLValidate.formatNumber(childNodeOfGundam, hostNode.getTextContent());
                        //quy chuẩn lại giá trị cho node status
                        if (statusNode == null) {
                            System.out.println("Null mịa rồi");
                        }
                        fillEmptyStockNode(statusNode, hostNode.getTextContent());
                        System.out.println("Giá trị mới đc cập nhật: " + childNodeOfGundam.getTextContent());
                    }
                }
            }
            NodeList listAfter = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);
            System.out.println("List after validate= " + listAfter.getLength() + " and node have been removed= " + count);
            //Sau khi xử lí dữ liệu xong thì bắt đầu đưa DOM result mới vào 1 file mình muốn
            transformDOMToFile(doc, output);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tham số là file xml cần đc validate và đường dẫn file xsd để thực hiện
     * validate bằng schemaFactory
     *
     * @param output
     */
    /**
     * Đưa DOM thành dạng String, sử dụng marshaller
     *
     * @param lib
     * @return
     */
    private String marshallerToTransferToString(Gundams lib) {
        try {
            JAXBContext jaxbc = JAXBContext.newInstance(Gundams.class);
            Marshaller marshal = jaxbc.createMarshaller();
//            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            /**
             * Vì cái dòng JAXB_FORMATTED_OUTPUT đã làm cho nó hiển thị như 1
             * xml ấy
             */
            marshal.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshal.setProperty(Marshaller.JAXB_FRAGMENT, true);
            /**
             * Với JAXB_FRAGMENT ta sẽ loại bỏ đc dòng xml declaration
             */
            //Ta thuc hien them 1 buoc bien doi xml file thang string
            StringWriter sw = new StringWriter();
            marshal.marshal(lib, sw);
            System.out.println("Day la String marshall: " + sw.toString());
            return sw.toString();
        } catch (JAXBException ex) {
            Logger.getLogger(XMLValidate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void prepareGundamObjectWithDOMResult(DOMResult output) throws TransformerException {
        try {
            /**
             * lấy node có tên là gundam, phần //* là của wildcard, phần
             * [local-name()='gundam'] là điều kiện
             */
            String expression = "//*[local-name()='gundam']";
            //local name dùng để trả về node trùng với tên đc đưa ra
            //ta sử dụng local-name vì nhiều khi có nhiều tài liệu xml
            //kết hợp với nhau, mỗi cái có prefix riêng nhưng các node
//            có nhiệm vụ giống nhau như node "name" để chứa name
//                    ta dùng local-name để lấy hết tất cả node name trong tất cả
//                            file xml
            int count = 0;//biến để kiểm tra bao nhiêu node bị xóa
            String priceExpression = "*[local-name()='price']";
            String nameExpression = "*[local-name()='name']";
            String hostExpression = "*[local-name()='host']";
            String statusExpression = "*[local-name()='status']";
            String typeExpression = "*[local-name()='type']";//Lấy tất type element
            NodeList nodeList = (NodeList) xpath.evaluate(expression, output.getNode(), XPathConstants.NODESET);
            System.out.println("List before: " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                //xóa node ko có dữ liệu
                Node node = nodeList.item(i);
                Node parent = node.getParentNode();
                if (node.hasChildNodes() == false) {
                    parent.removeChild(node);
                    count++;
                } else {
//                  Nếu như element đầy đủ thì kiểm tra type có rỗng ko, nếu rỗng thì bỏ đi
                    Node typeNode = (Node) xpath.evaluate(typeExpression, node, XPathConstants.NODE);
                    if (typeNode.getTextContent().equals("")) {
                        parent.removeChild(node);
                        count++;
                    } else {
//                      Nếu như type có value rồi thì format lại số cho đẹp

                        Node childNodeOfGundam = (Node) xpath.evaluate(priceExpression, node, XPathConstants.NODE);
                        Node hostNode = (Node) xpath.evaluate(hostExpression, node, XPathConstants.NODE);
                        Node statusNode = (Node) xpath.evaluate(statusExpression, node, XPathConstants.NODE);
                        //Bước làm đẹp lại số cho price
                        XMLValidate.formatNumber(childNodeOfGundam, hostNode.getTextContent());
                        //quy chuẩn lại giá trị cho node status
                        if (statusNode == null) {
                            System.out.println("Null mịa rồi");
                        }
                        fillEmptyStockNode(statusNode, hostNode.getTextContent());
                        System.out.println("Gia tri moi truoc khi dc cap nhat: " + childNodeOfGundam.getTextContent());

                    }
                }
            }

            //Phần này để test
            NodeList listAfter = (NodeList) xpath.evaluate(expression, output.getNode(), XPathConstants.NODESET);
            System.out.println("List after validate= " + listAfter.getLength() + " and node have been removed= " + count);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepareGundamObjectWithDOMResultAndMakeAFile(DOMResult output) throws TransformerException {

        //Tạm thời lấy đường dẫn này, sau này có trang admin thì realPath ez lấy
        String xsdFilePath = "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\schema\\gundams.xsd";

        try {
//            if (validateWithSchema(output.getNode(), xsdFilePath) == false) {
//                System.out.println("Node fail");
//            } else {
//                System.out.println("Node ok");
//            }
//            Document doc = XMLUtils.paserFileToDom(output);
            //Tạo 1 file doc đầu vào để xử lý, cụ thể ở đây là file xml có từ crawl nhưng dữ liệu chưa đc forrmat
            /**
             * lấy node có tên là gundam, phần //* là của wildcard, phần
             * [local-name()='gundam'] là điều kiện
             */
            String expression = "//*[local-name()='gundam']";
            //local name dùng để trả về node trùng với tên đc đưa ra
            //ta sử dụng local-name vì nhiều khi có nhiều tài liệu xml
            //kết hợp với nhau, mỗi cái có prefix riêng nhưng các node
//            có nhiệm vụ giống nhau như node "name" để chứa name
//                    ta dùng local-name để lấy hết tất cả node name trong tất cả
//                            file xml
            int count = 0;//biến để kiểm tra bao nhiêu node bị xóa
            String priceExpression = "*[local-name()='price']";
            String nameExpression = "*[local-name()='name']";
            String hostExpression = "*[local-name()='host']";
            String statusExpression = "*[local-name()='status']";
            String typeExpression = "*[local-name()='type']";//Lấy tất type element
            NodeList nodeList = (NodeList) xpath.evaluate(expression, output.getNode(), XPathConstants.NODESET);
            System.out.println("List before: " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                //xóa node ko có dữ liệu
                Node node = nodeList.item(i);
                Node parent = node.getParentNode();
                if (node.hasChildNodes() == false) {
                    parent.removeChild(node);
                    count++;
                } else {
//                    Nếu như element đầy đủ thì kiểm tra type có rỗng ko, nếu rỗng thì bỏ đi
                    Node typeNode = (Node) xpath.evaluate(typeExpression, node, XPathConstants.NODE);
                    if (typeNode.getTextContent().equals("")) {
                        parent.removeChild(node);
                        count++;
                    } else {
//                        Nếu như type có value rồi thì format lại số cho đẹp

                        Node childNodeOfGundam = (Node) xpath.evaluate(priceExpression, node, XPathConstants.NODE);
                        Node hostNode = (Node) xpath.evaluate(hostExpression, node, XPathConstants.NODE);
                        Node statusNode = (Node) xpath.evaluate(statusExpression, node, XPathConstants.NODE);
                        //Bước làm đẹp lại số cho price
                        XMLValidate.formatNumber(childNodeOfGundam, hostNode.getTextContent());
                        //quy chuẩn lại giá trị cho node status
                        if (statusNode == null) {
                            System.out.println("Null mịa rồi");
                        }
                        fillEmptyStockNode(statusNode, hostNode.getTextContent());
                        System.out.println("Giá trị mới đc cập nhật: " + childNodeOfGundam.getTextContent());
                    }
                }
            }

            //Phần này để test
            NodeList listAfter = (NodeList) xpath.evaluate(expression, output.getNode(), XPathConstants.NODESET);
            System.out.println("List after validate= " + listAfter.getLength() + " and node have been removed= " + count);

            //Sau khi đã có 
            //Sau khi xử lí dữ liệu xong thì bắt đầu đưa DOM result mới vào 1 file mình muốn, ko còn dùng nữa
            transformDOMToFile(output.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tinh nó sẽ quăng từng node vào để kiểm tra, nếu có lỗi thì nó sẽ ở chỗ
     * catch, kết quả trả về là fail Nếu node nào là fail thì Tinh xóa đi
     *
     * @param node
     * @param context
     * @return
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    public static boolean validateWithSchema(Node node, String xsdFilePath) throws IOException, TransformerConfigurationException, TransformerException {
        try {
            System.out.println("file path= " + xsdFilePath);
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdFilePath));

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Source xmlSource = new DOMSource(node);
            Result outputTarget = new StreamResult(outputStream);
            TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
            InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
            Validator validator = schema.newValidator();
            validator.validate(new SAXSource(new InputSource(is)));

            return true;
        } catch (SAXException ex) {
            System.out.println("catch= " + ex.getMessage());
            return false;
        }
    }

    /**
     * Format lại số cho đẹp Tham số đầu là price node tham số thứ 2 là tên miền
     *
     * @param node
     * @param host
     */
    public static void formatNumber(Node node, String host) {
        if (!node.getTextContent().matches("[a-zA-Z]+")) {
            if (host.equals("http://ymsgundam.com/")) {
                String newNumber = node.getTextContent().replace("VNĐ", "").trim();
                node.setTextContent(newNumber);
            } else if (host.equals("https://c3gundam.com/")) {
                String newNumber = node.getTextContent().replace("₫", "").trim();
                node.setTextContent(newNumber);
            } else if (host.equals("http://k2tgundam.com/")) {
                String newNumber = node.getTextContent().replace("Giá:", "").replace("đ", "").trim();
                node.setTextContent(newNumber);
            }
            node.setTextContent(node.getTextContent().replace(".", ""));
            if (node.getTextContent().equalsIgnoreCase("Liên Hệ")) {
                node.setTextContent("0");
            }
        }
    }

    /**
     * Quy chuẩn lại giá trị cho Status node Tham số đầu tiên là status node
     * tham số thứ 2 là tên miền để phân biệt
     *
     * @param node
     * @param hostName
     */
    public void fillEmptyStockNode(Node node, String hostName) {
        if (hostName.equals(ymsGundam)) {
            if (node.getTextContent().equals("") || node.getTextContent() == null) {
                node.setTextContent("Còn hàng");
            }
        } else if (hostName.equals(c3Gundam)) {
            if (node.getTextContent().equals("Mua ngay")) {
                node.setTextContent("Còn hàng");
            }
        }
    }

    /**
     * Tạm thời ko dùng hàm này Hàm này dùng để giải quyết mấy trường hợp thế
     * này arr.add("SDSS TAM QUỐC SANGOKU SOKETSUDEN FREEDOM GUNDAM - GIA
     * C&amp;#193;T LƯỢNG"); arr.add("HGAC 1/144 228 GUNDAM SANDROCK &amp;amp;
     * GUNDAM BEARKER MOBILE PRODUCT CODE SET"); arr.add("SDCS RX-0 UNICORN
     * GUNDAM (DESTROY MODE)"); arr.add("HGUC 215 1/144 AMS-123X-X MOON
     * GUNDAM");
     *
     * @param withOutSpace
     */
    public static void cutName(Node node, String hostName) {
        String withOutSpace = node.getTextContent();
        int location = 0;
        int flag = 0;
        String newString = "";
        if (hostName.equals("https://c3gundam.com/")) {
            if (withOutSpace.contains("&amp;")) {
                flag = 1;
                if (withOutSpace.contains("-")) {
                    if (withOutSpace.contains("&")) {
                        System.out.println("Đã vào");
                        location = withOutSpace.indexOf('-');
                    }
                } else if (withOutSpace.contains("(")) {
                    if (withOutSpace.contains("&")) {
                        location = withOutSpace.indexOf('(');
                    }
                }
            } else {
                if (withOutSpace.contains("-")) {
                    if (withOutSpace.contains("&")) {
                        System.out.println("Đã vào");
                        location = withOutSpace.indexOf('-');
                    }
                } else if (withOutSpace.contains("(")) {
                    if (withOutSpace.contains("&")) {
                        location = withOutSpace.indexOf('(');
                    }
                }
            }
            if (location != 0) {
                if (flag == 1) {
                    System.out.println("Đã vào trường hợp flag=1");
                    newString = withOutSpace.substring(0, location).replace("&amp;", "AND");
                    node.setTextContent(newString);
                } else {
                    newString = withOutSpace.substring(0, location);
                    node.setTextContent(newString);
                }
            } else {
                if (flag == 1) {
                    System.out.println("Đã vào trường hợp flag=1");

                    newString = withOutSpace.replace("&amp;", "AND");
                    node.setTextContent(newString);
                }
            }
        } else {
            System.out.println("Đây là tên: " + withOutSpace);
            node.setTextContent(withOutSpace.replace("&amp;", "&"));
        }
        System.out.println(newString);
    }

    /**
     * Hàm này cũng như cutName, nhưng chỉ để test thôi
     *
     * @param withOutSpace
     */
    public static void cutNameV2(String withOutSpace) {
        int location = 0;
        int flag = 0;
        String newString = "";
        if (withOutSpace.contains("&amp;")) {
            flag = 1;
            if (withOutSpace.contains("-")) {
                if (withOutSpace.contains("&")) {
                    System.out.println("Đã vào");
                    location = withOutSpace.indexOf('-');
                }
            } else if (withOutSpace.contains("(")) {
                if (withOutSpace.contains("&")) {
                    location = withOutSpace.indexOf('(');
                }
            }
        } else {
            if (withOutSpace.contains("-")) {
                if (withOutSpace.contains("&")) {
                    System.out.println("Đã vào");
                    location = withOutSpace.indexOf('-');
                }
            } else if (withOutSpace.contains("(")) {
                if (withOutSpace.contains("&")) {
                    location = withOutSpace.indexOf('(');
                }
            }
        }
        if (location != 0) {
            if (flag == 1) {
                newString = withOutSpace.substring(0, location).replace("&amp;", "AND");
//                node.setTextContent(newString);
            } else {
                newString = withOutSpace.substring(0, location);
//                node.setTextContent(newString);
            }
        } else {
            if (flag == 1) {
                newString = withOutSpace.replace("&amp;", "AND");
            }
//            node.setTextContent(newString);
        }
        System.out.println(newString);
    }

    /**
     * Lấy DomResult từ crawl
     *
     * @param dom
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    public void transformDOMToFile(Node node, String output)
            throws TransformerConfigurationException, TransformerException {

        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            StreamResult streamResult = new StreamResult(new FileOutputStream(output));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(node), streamResult);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLValidate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Lấy ra số lượng node đang hiện có
     */
    public static void getSize() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse("E:\\XML_jaxb_bo_o_day\\Stax_medicine_parser_web_version\\src\\java\\xsl\\gundam_rs.xml");
            //Sau khi kết thúc 4 đoạn trên, sẽ có 1 file mới sau khi xử lí
            String expression = "//*[local-name()='gundam']";//địt cụ đã lấy đc

            NodeList nodeList = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);
            System.out.println("List of graduant student: total - " + nodeList.getLength());
        } catch (Exception e) {
        }

    }

    /**
     * Hàm sinh ra với mục đích kiểm tra
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            XMLValidate utils = new XMLValidate();
//            DOMResult rs = ParserUltimate.crawl("E:\\XML_jaxb_bo_o_day\\Stax_medicine_parser_web_version\\src\\java\\xml\\gundam.xml", "E:\\XML_jaxb_bo_o_day\\Stax_medicine_parser_web_version\\src\\java\\xsl\\gundam.xsl");
//            List<String> list = new ArrayList<>();
//            list.add("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputyms.xml");
//            list.add("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml");
//            list.add("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputk2t.xml");
//            for (String string : list) {
//            }

            utils.prepareGundamObject(Constant.OUTPUTSOURCE);

//            List<String> arr = new ArrayList<>();
//            arr.add("SDSS TAM QUỐC SANGOKU SOKETSUDEN FREEDOM GUNDAM - GIA C&amp;#193;T LƯỢNG");
//            arr.add("HGAC 1/144 228 GUNDAM SANDROCK &amp;amp; GUNDAM BEARKER MOBILE PRODUCT CODE SET");
//            arr.add("SDCS RX-0 UNICORN GUNDAM (DESTROY MODE)");
//            arr.add("HGUC 215 1/144 AMS-123X-X MOON GUNDAM");
//            arr.add("SDTQ 351 T&amp;#212;N KI&amp;#202;N BẠCH HỔ");
//            arr.add("SDSS TAM QUỐC SAMGOKU SOKETSUDEN LUBU SINANJU &amp;amp; CHITUMA - LỮ BỐ &amp;amp; X&amp;#205;CH THỐ");
//            for (String string : arr) {
//                utils.cutNameV2(string);
//
//            }
        } catch (Exception ex) {
            Logger.getLogger(XMLValidate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*
    Đã xóa đc mấy cái node ko có dữ liệu, nhưng quá mất công khi tao 2 lần file, cần 
    xem lại đẻ tối ưu chạy 1 lần thu, để ý cái DOM result
     */
}
