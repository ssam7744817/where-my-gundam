/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import constant.Constant;
import crawler.jaxb.JAXBMarshalling;
import crawler.utils.XMLUtils;
import crawler.validation.XMLValidate;
import daos.GundamDAO;
import generate.report.Report;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaclass.ExportPDF;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;

/**
 * Class này có chức năng là tạo ra 1 file pdf
 *
 * @author HiruK
 */
public class ConvertPDFServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            GundamDAO dao = new GundamDAO();
            Report report = new Report();
            dao.getListHaveViewAndClickPDFversion(report);//Lấy full report rùi nha

            JAXBMarshalling marshalling = new JAXBMarshalling();

            String path = getServletContext().getRealPath("/");
            String fileName = path + "WEB-INF/fo/statistic.fo";

            DOMResult result = marshalling.marshallerToTransferToDOmResutl(report, Report.class);//Biến object thống kê thành xml

//            Apply stylsheet vao xml thống kê
            XMLUtils.parseDomToPDF(result, path + Constant.pdf, fileName);

//            XMLValidate validate=new XMLValidate();
//            validate.transformDOMToFile(result.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\pdftest.xml");
            ExportPDF exportPDF = new ExportPDF();
            response.setContentType("application/pdf");
            exportPDF.exportToPDF(response, fileName);//Chuyển thống kê xml sau khi apply thanh pdf

        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Chỉ dành cho mục đích test
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            GundamDAO dao = new GundamDAO();
            Report report = new Report();
            dao.getListHaveViewAndClickPDFversion(report);//Lấy full report rùi nha

            //Bắt đầu biến nó thành xml
            JAXBMarshalling marshalling = new JAXBMarshalling();

            DOMResult result = marshalling.marshallerToTransferToDOmResutl(report, Report.class);//Đã biến thành xml

            //Kiểm tra coi dữ liệu có ổn áp ko bằng cách chuyển thành file
            XMLValidate validate = new XMLValidate();
//            validate.transformDOMToFile(result.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\pdftest.xml");

            //Bắt đầu apply stylesheet vào
            DOMResult finalResult = XMLUtils.parseToDOMNoRecolverWIthDomresultInput(result, "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xsl\\testTable.xsl");
            //In nó ra để kiểm tra
            validate.transformDOMToFile(finalResult.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\pdftest.xml");

            //Bắt đầu đưa thành pdf bằng cách kết hợp fo và apply stylesheet
//            XMLUtils.parseDomToPDF(result, "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xsl\\pdf.xsl", "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\fo\\statistic.fo");
        } catch (SQLException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ConvertPDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
