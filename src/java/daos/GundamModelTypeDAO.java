/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.GundamModelTypeDTO;
import generate.gundams.Gundam;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HiruK
 */
public class GundamModelTypeDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    private List<GundamModelTypeDTO> list = new ArrayList<GundamModelTypeDTO>();

    public List<GundamModelTypeDTO> getList() {
        return list;
    }

    public void setList(List<GundamModelTypeDTO> list) {
        this.list = list;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public void getModelTypeName() throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [typeId]\n"
                    + "      ,[name]\n"
                    + "  FROM [dbo].[gundam_model_type]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("typeId");
                String name = rs.getString("name");
                GundamModelTypeDTO dto=new GundamModelTypeDTO(id, name);
                if(list==null){
                    list=new ArrayList<>();
                }
                list.add(dto);
            }
        } finally {
            closeConnection();
        }
    }
}
