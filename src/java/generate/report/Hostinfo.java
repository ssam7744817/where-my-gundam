
package generate.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hostinfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hostinfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hostName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clickCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="viewCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hostinfo", propOrder = {
    "hostName",
    "clickCount",
    "viewCount"
})
public class Hostinfo {

    @XmlElement(required = true)
    protected String hostName;
    @XmlElement(required = true)
    protected String clickCount;
    @XmlElement(required = true)
    protected String viewCount;

    /**
     * Gets the value of the hostName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Sets the value of the hostName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostName(String value) {
        this.hostName = value;
    }

    /**
     * Gets the value of the clickCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClickCount() {
        return clickCount;
    }

    /**
     * Sets the value of the clickCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClickCount(String value) {
        this.clickCount = value;
    }

    /**
     * Gets the value of the viewCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewCount() {
        return viewCount;
    }

    /**
     * Sets the value of the viewCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewCount(String value) {
        this.viewCount = value;
    }

}
