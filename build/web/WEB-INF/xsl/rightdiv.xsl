<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : rightdiv.xsl
    Created on : April 1, 2020, 10:50 PM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:g="http://xml.netbeans.org/schema/gundam"
    exclude-result-prefixes="g"
    version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:template match="g:gundam">
        <div id="inside">
            <!--Nếu có hình thì mới show cái div-->
            <div style="width: 100%;background-color: whitesmoke;margin-top: 20px;height: max-content;padding: 5px">
                <!--Phần ảnh lớn-->
                <div style="width: 250px;height: 250px;background-color: pink;border: 2px solid black;display: inline-block">
<!--                    <xsl:attribute name="style">
                        <xsl:value-of select="'width: 250px;height: 250px;background-color: pink;border: 2px solid black;display: inline-block'"/>
                    </xsl:attribute>  
                    <xsl:value-of select="dsdsd"/>  -->
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="//g:avatar/text()"/>
                        </xsl:attribute>
                        <xsl:attribute name="id">
                            <xsl:value-of select="'bigAvatar'"/>
                        </xsl:attribute>
                        <xsl:attribute name="width">
                            <xsl:value-of select="'100%'"/>
                        </xsl:attribute>
                        <xsl:attribute name="height">
                            <xsl:value-of select="'100%'"/>
                        </xsl:attribute>
                    </xsl:element>
                </div>
                
                <div style="display: inline-block;vertical-align: top;margin-left: 10px">
                    <p id="gundamName" style="color: blue;font-weight: bold;font-size: 13px">
                        <xsl:value-of select="//g:name/text()"/>
                    </p>
                    <p id="gundamType">
                        <xsl:value-of select="//g:type/text()"/>
                    </p>
                    <p class="money" style="color: red;font-weight: bold">
                        <xsl:value-of select="//g:price/text()"/>
                    </p>
                    <a style="text-decoration: none" href="{//g:detailLink/text()}" target="_blank" onclick="saveView('{//g:id/text()}','{//g:host/text()}')">
                        <p id="gundamLink">Go to shop page</p>
                    </a>
                </div>
                <div style="margin-top: 20px;padding-bottom:  30px;">
                    <!--Đây là chỗ hiện ảnh-->
                    <xsl:for-each select="//g:picture">
                        <div style="background-color: green;height: 70px;width: 70px;display: inline-block;margin-left: 5px;border: 1px solid black">
                            <img id="smallAvatar" onclick="showBigAvatar('{text()}')" src="{text()}" style="width: 100%;height: 100%"/>
                        </div>
                    </xsl:for-each>
                </div>
            </div>
        </div>
    </xsl:template>
     
</xsl:stylesheet>
