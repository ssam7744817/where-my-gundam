/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.crawlservice;

import constant.Constant;
import crawler.validation.XMLValidate;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.dom.DOMResult;
import crawler.jaxb.JAXBUnmarshallingGundams;
import crawler.utils.XMLUtils;
import daos.AnimeDAO;
import daos.GundamDAO;
import daos.GundamInAnimeDAO;
import dtos.GundamInAnimeDTO;
import generate.anime.Animes;
import generate.gundams.Gundam;
import generate.gundams.Gundams;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javaclass.AdvancedString;
import javaclass.RemoveDuplicate;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

/**
 *Class phụ trách việc cào dữ liệu về, mọi thứ bắt đầu từ đây
 * @author HiruK
 */
public class CrawlService {

    private DOMResult result;//luu tru DOMresult khi crawl ve

    public DOMResult getResult() {
        return result;
    }

    public void setResult(DOMResult result) {
        this.result = result;
    }

    /**
     * Đây là hàm crawl dũ liệu về, việc có đc shortkey là từ hàm này ra
     *
     * @param realPath
     * @return
     */
    public int crawl(String realPath) {
        Gundams gundams = new Gundams();
        try {
            Map<String, String> map = new HashMap<String, String>();
            map.put(realPath + Constant.K2Tsource, realPath + Constant.xslk2t);
            map.put(realPath + Constant.C3source, realPath + Constant.xslc3);
            map.put(realPath + Constant.YMSsource, realPath + Constant.xslyms);
            map.put(realPath + Constant.HoppySearchSource, realPath + Constant.xslhobby);
            DOMResult rs = new DOMResult();

            //Tạo ra 1 cây DOM
            for (Map.Entry<String, String> entry : map.entrySet()) {
                //Đây là phần cào dữ liệu là các bộ phim hoạt hình gundam và những gundam có trong hoạt hình đó
                if (entry.getKey().equals(realPath + Constant.HoppySearchSource)) {

                    //(1): Tôi lấy những dữ liệu mà tôi cần từ hàm parseToDOM, nhấn vào hàm để biết chi tiết
                    rs = XMLUtils.parseToDOM(entry.getKey(), entry.getValue());
                    crawler.jaxb.JAXBUnmarshallingGundams jaxb = new JAXBUnmarshallingGundams();

                    //(2): tôi biến nó thành java object để lưu vào DB, nhấp vào để xem chi tiết
                    Animes animes = jaxb.creatAnimeObject(rs, realPath + Constant.animeSchema);
                    System.out.println("size here= " + animes.getAnime().size());

                    AnimeDAO dao = new AnimeDAO();
                    dao.insert(animes);

                }

                /**
                 * (3) Crawl từ 3 web gundamc3, k2t và yms
                 */
                rs = XMLUtils.parseToDOM(entry.getKey(), entry.getValue());

                //Sau khi đã có đầu ra file XML, bắt đầu validate dữ liệu bên trong
                //Bản chất validate ko phải là biến thể dữ liệu thành chuẩn mực
                //mà nó đảm bảo tính đúng đắn của dữ liệu như kiểu dữ liệu số thì ko đc có string
                //Quy chuẩn lại dữ liệu trước khi validate
                XMLValidate validate = new XMLValidate();
                validate.prepareGundamObjectWithDOMResult(rs);

                //sau khi quy chuẩn xong thì tiến hành validate bằng schema
                //Thật ra bước schema ở đây là vì tôi muốn đảm bảo dữ liệu phải đúng hết rồi mới lưu trong DB
                /**
                 * Bỏ qua bước này cũng đc vì tôi cũng đã sử dụng schema trong
                 * bước JAXB marshalling thông qua 2 dòng lệnh là :
                 *
                 * u.setSchema(getSchema(schema)); u.setEventHandler(new
                 * JAXBGundamValidationHanlder());
                 *
                 * Gióng như crawl hoppysearch ở trên tôi ko dùng bước này vì
                 * tôi chắc chắn dữ liệu là đúng đắn nhờ vào việc kiểm tra
                 * DOMResult thông qua file Nếu thầy có hỏi thì nói em có
                 * validate schema trong JAXB là đủ rồi, bước validate ở dưới là
                 * tôi làm khi mới viết project chưa chắc chắn thứ gì cả =))
                 */
                boolean result = XMLValidate.validateWithSchema(rs.getNode(), realPath + Constant.schema);// Bước này có hay ko cũng đc

                //Nếu như ko có lỗi thì mới đưa vào database
                if (result == true) {
                    this.result = rs;//Luu domresult

                    /**
                     * Sau khi đã validate xong thì bắt đầu chuyển hóa thành
                     * java object để tiện import vào DB
                     *
                     */
                    crawler.jaxb.JAXBUnmarshallingGundams jaxb = new JAXBUnmarshallingGundams();

                    //Bắt đầu đưa vào DB 
                    GundamDAO dAO = new GundamDAO();
                    gundams = jaxb.creatGundamObjectWithDOM(rs, realPath + Constant.schema);

                    //trước đó hãy xóa đi những node mà text node của nó trùng hết tất cả nội dung bên trong
                    RemoveDuplicate.removeDuplicates(gundams.getGundam());

                    dAO.setList(gundams.getGundam());
                    System.out.println("list= " + dAO.getList().size());
                    for (Gundam gundam : dAO.getList()) {
                        dAO.insert(gundam);
                    }
                    //kết thúc việc đưa dữ liệu xml vào DB  

                }//Kết thúc quá trình parse DOM
            }

            //(4): 1 tí xử lí ăn tiền mà thầy Khánh muốn ở môn này
            /**
             * Web của ông dù có làm đẹp cỡ nào nhưng dữ liệu crawl về mà ko có
             * sự mới mẻ thì cao nhất cũng chỉ đc 5 "mới mẻ" : ở app tôi dữ liệu
             * ban đầu ở web hoppysearch chỉ có tên hoạt hình và những mô hình
             * gundam có trong hoạt hình đó và dữ liệu 3 web còn lại là các mô
             * hình gundam đc bán ở Việt Nam Và tôi đã xử lí sao cho từ mô hình
             * biết đc thông qua phim hoạt hình, tôi chỉ cần click vào tấm ảnh
             * thì tự động tìm ra nơi bán mô hình đó ở Viêt Nam Điểm giống nhau
             * duy nhất của dữ liệu có lẽ nằm ở cái tên nhưng tiếc thay tuy cùng
             * loại mô hình nhưng tên lại khác nhau do cách dặt tên của mỗi
             * trang web và tôi đã tìm ra cách để chúng có liên kết với nhau
             *
             * Nói lại cuối cùng la: dữ liệu cào về, phải từ dữ liệu đó tìm ra
             * một dữ liệu mới hay dễ hiểu hơn là tập dữ liệu A giao với tập dữ
             * liệu B, phần giao đó chính là phần ăn tiền của project Ví dụ:
             * Crawl về tên các loại cá và crawl về tên các đại dương và bạn
             * phải làm sao để khi người ta tìm đến đại dương nào thì đồng thời
             * biết đại dương đó có cá gì.Ta có tập dữ liệu cá giao với tập dữ
             * liệu đại dương, phần giao chính là tập các con cá chỉ thuộc 1 đại
             * dương cụ thể như cá ở đại tây dương ko thể có mặt ở thái bình
             * dương
             *
             * Ở bài project của tôi, tôi đã sinh ra 1 dữ liệu gọi là shortkey,
             * nhờ cái shortkey này tôi có thể biết mô hình mà tôi thích trong
             * hoạt hình nào đó có bán ở VIệt Nam hay ko
             */
            List<String> list = new ArrayList<>();

            List<GundamInAnimeDTO> gundamInAnimeList = new ArrayList<>();
            GundamInAnimeDAO gundamInAnimeDao = new GundamInAnimeDAO();
            gundamInAnimeDao.getAllGundamInAnime(gundamInAnimeList);
            for (int i = 0; i < gundamInAnimeList.size(); i++) {
                /**
                 * 2 hàm cutString và findTheShortKey là cách tôi tìm ra
                 * shortkey đó
                 *
                 */
                String[] strings = AdvancedString.cutString(gundamInAnimeList.get(i).getName());
                String shoString = AdvancedString.findTheShortKey(strings);
                if (shoString != null && shoString != "") {
                    /**
                     * Làm thế này để loại những gundam in anime ko có shortkey
                     */
                    gundamInAnimeDao.insertShortkey(shoString, gundamInAnimeList.get(i).getId());
                    list.add(shoString + " and gundam name= ");
                }
            }
            for (String strng : list) {
                System.out.println("This is short key= " + strng);
            }
        } catch (TransformerException ex) {
            Logger.getLogger(CrawlService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CrawlService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(CrawlService.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CrawlService.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        return gundams.getGundam().size();
    }

    /**
     * HÀM NÀY DÙNG ĐỂ TEST THÔI, nếu bạn muốn kiểm tra xem DOMResult có đúng
     * bạn như mong muốn hay ko thì hãy thưc hiện từng bước ở dưới. Kết quả là
     * sẽ sinh ra 1 file xml in ra DOMResult
     */
    public void testCrawl() {
        String reString = "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\build\\web\\";
        try {
            Map<String, String> map = new HashMap<String, String>();
//            map.put(reString + Constant.K2Tsource, reString + Constant.xslk2t);
            map.put("G:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\perfume.xml", "G:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xsl\\perfume.xsl");
//            map.put(reString + Constant.HoppySearchSource, reString + Constant.xslhobby);
//            map.put(reString + Constant.C3source, reString + Constant.xslc3);
//            map.put(reString + Constant.YMSsource, reString + Constant.xslyms);
//            map.put("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\book.xml", "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xsl\\newstylesheet.xsl");
            DOMResult rs = new DOMResult();

            //(1) Tạo ra DOMResult
            for (Map.Entry<String, String> entry : map.entrySet()) {
                rs = XMLUtils.parseToDOM(entry.getKey(), entry.getValue());
                //Kết thúc tạo domresult

                //CHuyển nó thành dạng file thông qua hàm transformDOMToFile
                XMLValidate validate = new XMLValidate();

                /**
                 * tham số đầu tiên là domresult muốn chuyển tham số thứ 2 là
                 * nơi cái file bạn muốn in cái domresult đó ra
                 */
                validate.transformDOMToFile(rs.getNode(), "G:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\perfumeoutput.xml");

                System.out.println("Thành công rùi");
            }//kết thúc quá trình kiểm tra bằng file

            /**
             * Thực ra class này chạy đầu tiên để có tât cả mọi dữ liệu
             * Dữ liệu đc cào về và xử lí xong xuôi đều ở đây hết, đối với bài toán của tôi là như vậy
             * Còn các project khác thì ko nên cứng nhắc bỏ hết ở đây
             * 
             * Tôi có 1 gợi ý cho project của các bạn là crawl về các loại thực phẩm và số calo
             * của từng loại thưc phẩm đó. Và ta yêu cầu người dùng nhập số calo mà người đó chỉ muốn tiêu thụ trong ngày, hay tuần
             * ví dụ: Trong tuần này mỗi ngày chỉ tiêu thụ 2000 calo thôi
             * Rồi yêu yêu cầu người dùng nhập các loại thưc phẩm mà ngừi đó nghĩ sẽ ăn trong hôm nay
             * Nếu cộng số calo quá calo tiêu thụ thì sẽ đề xuất những món ăn ít calo hơn dựa trên việc cắt bớt vài những thực phẩm đc nhập vào
             * Còn không thì thông báo bữa ăn quá tốt.
             * 
             * Nhảy qua trang SaveClickAndViewServlet.java để hiểu rõ sủ dụng xmlhttpr4equest và nhận về kết quả
             * là 1 xml để render giao diện
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CrawlService craw = new CrawlService();
        craw.testCrawl();
    }
}
