/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author HiruK
 */
public class ShortKeyDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    /**
     * Đưa vào những short key tốt nhất ở bảng short key và gundam
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void insertShortkey(String shorkey, String gundamID) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "insert into short_key(shortkey)values(?)";//Đưa shorkey vào bảng short_key
            String gundamInShopUpdateShortKey = "update gundam set shortkey=?  where id=?";//Tiện thể update cái short key của bảng gundam luôn
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, shorkey);
            int result = preStm.executeUpdate();//Sau khi insert vào bản short_key ròi thì đến bảng gundam
            preStm = conn.prepareStatement(gundamInShopUpdateShortKey);
            preStm.setString(1, shorkey);
            preStm.setString(1, gundamID);
            int resul2 = preStm.executeUpdate();
            if (result > 0 && resul2 > 0) {
                System.out.println("Insert shortky ok");
            }

        } finally {
            closeConnection();
        }
    }

    /**
     * Cập nhật short key vào từng gundam để mỗi lần tìm kiếm thì tự động kiếm
     * trên đó trước
     *
     * @param shorkey
     */
    public void updateShortkeyToGundam(String shorkey) {

    }
}
