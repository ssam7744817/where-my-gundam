/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import crawler.jaxb.JAXBUnmarshallingGundams;
import dtos.AnimeDTO;
import dtos.GundamInAnimeDTO;
import dtos.GundamDTO;
import generate.anime.Anime;
import generate.anime.Animes;
import generate.anime.Gundam;
import generate.anime.Gundaminanime;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;

/**
 *
 * @author HiruK
 */
public class AnimeDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    List<AnimeDTO> animes = new ArrayList<>();

    public List<AnimeDTO> getAnimes() {
        return animes;
    }

    public void setAnimes(List<AnimeDTO> animes) {
        this.animes = animes;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public int insert(Animes animes) throws ClassNotFoundException, SQLException {
        int result = 0;
        try {
            conn = MyConnection.getConnection();
            String animeSql = "insert into anime(id,name)values(?,?)";
            String gundamInAnimesql = "insert into gundam_in_anime(name,scale,avatar,animeId)values(?,?,?,?)";

            for (Anime anime : animes.getAnime()) {
                preStm = conn.prepareStatement(animeSql);
                String id = UUID.randomUUID().toString() + "-anime";
                preStm.setString(1, id);
                preStm.setString(2, anime.getAnimeName());
                preStm.executeUpdate();
                for (Gundaminanime gundam : anime.getGundaminanime()) {
                    preStm = conn.prepareStatement(gundamInAnimesql);
                    preStm.setString(1, gundam.getName());
                    preStm.setString(2, gundam.getScale());
                    preStm.setString(3, gundam.getAvatar());
                    preStm.setString(4, id);
                    result = preStm.executeUpdate();
                }
            }

            if (result > 0) {
                System.out.println("Thành công");
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    /**
     * Lấy hết tất cả các loại anime và ảnh đại diện cho mỗi anime
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void getAll() throws ClassNotFoundException, SQLException {
        AnimeDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[avatar]\n"
                    + "  FROM [dbo].[anime]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new AnimeDTO();
                dto.setId(rs.getString("id"));
                dto.setName(rs.getString("name"));
                dto.setAvatar(rs.getString("avatar"));
                animes.add(dto);
            }
        } finally {
            closeConnection();
        }
    }

    public static void main(String[] args) {
        try {
            crawler.jaxb.JAXBUnmarshallingGundams jaxb = new JAXBUnmarshallingGundams();

            Animes animes = jaxb.creatAnimeObjectFileVersion("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\hobbyouput.xml", "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\schema\\anime.xsd");
            System.out.println("size here= " + animes.getAnime().size());
            AnimeDAO dao = new AnimeDAO();
            dao.insert(animes);
        } catch (JAXBException ex) {
            Logger.getLogger(AnimeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(AnimeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AnimeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AnimeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
