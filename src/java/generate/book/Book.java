
package generate.book;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "book", propOrder = {
    "name",
    "author",
    "publicationdate"
})
@XmlRootElement(name = "book")
public class Book {
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String author;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar publicationdate;

    public String getName() {
        return name;
    }
    public void setName(String value) {
        this.name = value;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String value) {
        this.author = value;
    }

    public XMLGregorianCalendar getPublicationdate() {
        return publicationdate;
    }
    public void setPublicationdate(XMLGregorianCalendar value) {
        this.publicationdate = value;
    }

}
