/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import constant.Constant;
import crawler.jaxb.JAXBMarshalling;
import crawler.utils.XMLUtils;
import crawler.validation.XMLValidate;
import daos.ClickInfoDAO;
import daos.PictureDAO;
import daos.ViewDAO;
import dtos.GundamDTO;
import generate.gundam.Gundam;
import generate.gundam.Pictures;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;

/**
 * Class có tác dụng lưu lại những lượt click và view của người dùng
 * 
 * 
 * Sau khi đọc xong CrawlService.java thì đọc những phần tôi đánh dấu ở dưới để hiểu rõ cách thực hiện xmlHttprequest
 * nhằm tránh load lại trang. Quay lại trang search.jsp và tìm hàm saveClick() để bắt đầu đọc
 * @author HiruK
 */
public class SaveClickAndViewServlet extends HttpServlet {

    final String click = "click";
    final String view = "view";
    final String anime = "anime";
    final String shop = "shop";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        //Lấy value của  parameter
        String id = request.getParameter("id");
        String type = request.getParameter("interactionType");
        String differ = request.getParameter("differ");
        String gundamType = request.getParameter("gundamtype");
        String gundamHost = request.getParameter("gundamhost");
        ClickInfoDAO dao = new ClickInfoDAO();
        ViewDAO viewDao = new ViewDAO();

        //Lấy ra realpath
        ServletContext context = this.getServletContext();
        String realPath = context.getRealPath("/");//đường dẫn tuyệt đối của app ở phía server, dấu "/" là root
        String render = realPath + Constant.render;
        try {
            if (type != null) {
                //Nếu như id là số thì là của gundam in animes
                switch (type) {
                    case click:
                        dao.saveClick(id, gundamType, gundamHost);
                        break;
                    case view:
                        switch (differ) {
                            case anime:
                                viewDao.saveView(id, anime, gundamHost);
                                break;
                            case shop:
                                System.out.println("gundam host= " + gundamHost);
                                viewDao.saveView(id, shop, gundamHost);
                                break;
                        }
                        System.out.println("save view ok");
                        break;
                }

            }

            //Cái này chỉ chạy khi saveClick đc kich hoạt và ở gundam in shop
            if (type.equals(click) && !id.matches("[0-9]+")) {
                //Tiện thể móc cái detail lên luôn
                HttpSession session = request.getSession();
                List<GundamDTO> list = (List<GundamDTO>) session.getAttribute("SEARCH");
                System.out.println("size in session= " + list.size());
                PictureDAO pictureDao = new PictureDAO();//Cái này để lấy thêm ảnh gắn vào phần detail
                if (list != null) {
                    for (GundamDTO dto : list) {
                        if (id.equals(dto.getId())) {
                            Gundam gundam = new Gundam();
                            gundam.setId(dto.getId());
                            gundam.setAvatar(dto.getAvatar());
                            gundam.setDetailLink(dto.getDetailLink());
                            gundam.setHost(dto.getHostId());
                            gundam.setPrice(dto.getPrice());
                            gundam.setName(dto.getName());
                            gundam.setType(dto.getTypeId());
                            //Thực hiện lấy hình chi tiết, ghép vào phần detail và cũng là bước cuối để hoàn thiện tạo xml
                            pictureDao.getPictureListByID(id);
                            System.out.println("picture size= " + pictureDao.getList().size());

                            //Set picture vào trong nhé đcm
                            Pictures pictures = new Pictures();
                            for (String g : pictureDao.getList()) {
                                pictures.getPicture().add(g);
                            }
                            gundam.setPictures(pictures);
                            System.out.println("picture in gundam= " + gundam.getPictures().getPicture().size());
                            System.out.println("Detail already get it");

                            JAXBMarshalling marshalling = new JAXBMarshalling();

                            //Test trước cái đã, ĐÃ THÀNH CÔNG
                            DOMResult result = marshalling.marshallerToTransferToDOmResutl(gundam, Gundam.class);
                            
                            
                            //Tiếp tục, những dòng trên là xử lí riêng của tôi nên ko cần quan tâm, chỉ cần đọc những dòng dưới
                            /**
                             * Sau khi gửi request thì việc tiếp theo sẽ giống như những dòng dưới, và servlet đón nhận XMLHttpRequest cũng bình thường như cách mà đó nhận 1 request từ trang jps thôi
                             * Và làm sao để lấy parameter từ XMLhttpRequest?? Nó ko khác gì cách lấy parameter thông thưởng trước giờ làm bài
                             * Nhìn đống param ở trên là hiểu
                             */
                            
                            /*
                            //DÒng này là trả về 1 xml ở dạng string, cái string này chính là responseString
                            Và xmlHttpRequest chỉ nhận kết quả là string, ta ko dùng session hay request.setAttribute
                            */
                            String resultString = XMLUtils.parseDomToXMLString(result, render);
                            System.out.println("string xml= " + resultString);
                            
                            //Sau khi ta có string cần thiết, thì bước dưới là cách mà ta gắn string vào kết quả trả về, chỉ 1 cách duy nhất
                            //Với việc đón nhận XMLHttpRequest, ta ko đc viết dòng out.close() thường thấy trong các bài java web, hay request.sendredirect hay requestdispatcher
                            //Nói chung servlet đón nhận xmlHttpRequest có cấu trúc như class này
                            response.getWriter().write(resultString);//Dòng này là kết thúc quá trình đưa lên jsp để render, quay lại trang search.jsp tiếp tục hàm saveClick để đọc phần kết
                        }
                    }
                }
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SaveClickAndViewServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SaveClickAndViewServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(SaveClickAndViewServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SaveClickAndViewServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
/**
 * Phần này chỉ dùng để test
 * @param args 
 */
    public static void main(String[] args) {
        try {
            /**
             * Ở dưới là quá trình test
             */
            XMLValidate validate = new XMLValidate();
            //In ra file trước
//                            validate.transformDOMToFile(result.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\test.xml");
//Sau khi in ra file thì áp dụng stylesheet
            DOMResult html = XMLUtils.parseToDOMNoRecolver("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\test.xml", "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xsl\\rightdiv.xsl");
//In ra cái xml sau khi đã áp dụng stylesheet để kiểm tra
            validate.transformDOMToFile(html.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputhtml.xml");
        } catch (Exception ex) {
            Logger.getLogger(SaveClickAndViewServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
