/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import constant.Constant;
import crawler.crawlservice.CrawlService;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *Đây là class xử lí các yêu cầu từ trang admin
 * Admin.jsp gồm các chức năng chính mà thầy quan tâm sau:
 * 
 * + Crawl: Crawl dữ liệu về, chỉ cần click vào nút đó, đợi con quay dừng lại thì sẽ có 1 cái alert trên trang
 * web thông báo đã cào xong, ko cần làm gì thêm
 * + Clear all record: Xóa hết dữ liệu ở DB
 * + Export to execl: Cho ra 1 bảng báo ở dạng excel
 * + Export to PDF: Cho ra 1 bảng báo cáo ở dạng pdf
 * 
 * Những chức năng CRUD thày sẽ ko quan tâm
 * @author HiruK
 */
public class AdminServlet extends HttpServlet {

    final String showPage = "admin.jsp";
    final String xsd = "WEB-INF/schema/gundams.xsd";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

            ServletContext context = this.getServletContext();
            String realPath = context.getRealPath("/");//đường dẫn tuyệt đối của app ở phía server, dấu "/" là root
            
            System.out.println("real path= "+realPath+Constant.YMSsource);
            CrawlService service = new CrawlService();
            int count = service.crawl(realPath);//Biến count cho ta biết có bao nhiêu record đc cào về
            request.setAttribute("COUNT", count);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
