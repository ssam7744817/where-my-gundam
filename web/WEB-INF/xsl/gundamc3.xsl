<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : gundam_az.xsl
    Created on : February 14, 2020, 10:08 AM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:t="http://xml.netbeans.org/schema/gundams"
                xmlns="http://xml.netbeans.org/schema/gundams"
>
    <xsl:output method="xml" indent="yes" omit-xml-declaration="no"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="t:gundams">
        <xsl:element name="gundams">
            <xsl:for-each select="t:link">

                    
                
                
                <xsl:variable name="maxItems" select="27" />
                <xsl:variable name="sequence" select="document(@hr)"/>
                
                
                <!--Oke vậy là ta thay số đc-->
                <xsl:variable name="newlink">
                    <xsl:call-template name="string-replace-all">
                        <xsl:with-param name="text" select="@hr" />
                        <xsl:with-param name="replace" select="concat('page=','1')" />
                        <xsl:with-param name="by" select="2" />
                    </xsl:call-template>
                </xsl:variable>
                <!--                <xsl:element name="gundam">
                    <xsl:value-of select="$newlink"/>
                </xsl:element>-->
                
                <!--Chạy cái page đầu tiên trước đã-->
                <xsl:variable name="listDoc" select="document(@hr)"/>
                <xsl:call-template name="getGundamInfo">
                    <xsl:with-param name="link" select="$listDoc"/>
                    <xsl:with-param name="host" select="@host"/>
                </xsl:call-template>
                
                <!--Sau khi đã chạy đc 1 lần ta bắt đầu quét phân trang-->
                <xsl:call-template name="repeatable">
                    <xsl:with-param name="link" select="@hr"/>
                    <xsl:with-param name="host" select="@host"/>
                </xsl:call-template>
                
                
            </xsl:for-each>
            
        </xsl:element>
    </xsl:template>
    
    <!--Hàm lặp lại để crawl hết đống phân trang-->
    <xsl:template name="repeatable">
        <xsl:param name="link"/>
        <!--Biến link bây h đang chưa địa chỉ gốc-->
        <xsl:param name="host"/>
        
        <xsl:param name="index" select="1" />
        <xsl:param name="total" select="26" />
        
        

        <!--Tạo địa chỉ mới-->
        <xsl:variable name="newlink">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$link" />
                <xsl:with-param name="replace" select="concat('page=',$index)" />
                <xsl:with-param name="by" select="concat('page=',$index+1)" />
            </xsl:call-template>
        </xsl:variable>
        
        
        <!--Bắt đầu crawl địa chỉ mới-->
        <xsl:variable name="listDoc" select="document($newlink)"/>
        <xsl:call-template name="getGundamInfo">
            <xsl:with-param name="link" select="$listDoc"/>
            <xsl:with-param name="host" select="@host"/>
        </xsl:call-template>
        
        

        <!--Phần đệ quy để nó chạy tiếp vòng lăp-->
        <xsl:if test="not($index = $total)">
            <xsl:call-template name="repeatable">
                <xsl:with-param name="index" select="$index + 1" />
                <xsl:with-param name="link" select="$newlink"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <!--Hàm replace, hàm này giúp thay cái số trong phân trang-->
    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text"
                                    select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="getGundamInfo">
        <xsl:param name="link"/>
        <xsl:param name="host"/>
        <xsl:for-each select="$link//div[@class='col-xs-6 col-sm-4 col-md-3 col-lg-3']">
            <xsl:element name="gundam">
                <xsl:variable name="shortLinkImage" select="div[@class='evo-product-block-item']//div[@class='product_thumb']//a[@class='primary_img']"/>
                <xsl:variable name="nameGundam" select="div[@class='evo-product-block-item']//div[@class='product_content']//div[@class='product_name']//h4//a//text()"/>
                <xsl:variable name="stock" select="div[@class='evo-product-block-item']//div[@class='product_thumb']//form[@class='cart-wrap hidden-sm hidden-xs hidden-md variants form-nut-grid form-ajaxtocart']"/>
                <xsl:element name="name">
                    <xsl:value-of select="$nameGundam" disable-output-escaping="yes"/>
                </xsl:element>
                <xsl:element name="detailLink">
                    <xsl:value-of select="concat('https://c3gundam.com',$shortLinkImage//@href)" disable-output-escaping="yes"/>
                </xsl:element>
                <xsl:element name="avatar">
                    <xsl:value-of select="concat('http:',$shortLinkImage//img[@class='lazy']//@data-src)" disable-output-escaping="yes"/>
                </xsl:element>
                                        
                <xsl:element name="pictures">
                    <xsl:call-template name="getDetail">
                        <xsl:with-param name="detailLink" select="document(concat('https://c3gundam.com',$shortLinkImage//@href))"/>
                        <xsl:with-param name="host" select="$host"/>
                    </xsl:call-template>
                </xsl:element>
                <xsl:element name="price">
                    <xsl:value-of select="div[@class='evo-product-block-item']//div[@class='product_content']//div[@class='price-container']//span[@class='current_price']//text()"/>
                </xsl:element>
                <xsl:element name="status">
                    <xsl:if test="$stock//button[@class='action fas cart-now']//@title">
                        <xsl:value-of select="$stock//button[@class='action fas cart-now']//@title"/>
                    </xsl:if>
                    <xsl:if test="$stock//button[@class='action cart-button option-icons fas']//@title">
                        <xsl:value-of select="$stock//button[@class='action cart-button option-icons fas']//@title"/>
                    </xsl:if>
                </xsl:element>
                <xsl:element name="type">
                    <xsl:choose>
                        <xsl:when test="contains($nameGundam,'HG')">
                            <xsl:text>HG</xsl:text>
                        </xsl:when>
                        <xsl:when test="contains($nameGundam,'RG')">
                            <xsl:text>RG</xsl:text>
                        </xsl:when>
                        <xsl:when test="contains($nameGundam,'MG')">
                            <xsl:text>MG</xsl:text>
                        </xsl:when>
                        <xsl:when test="contains($nameGundam,'SD')">
                            <xsl:text>SD</xsl:text>
                        </xsl:when>
                        <xsl:when test="contains($nameGundam,'PG')">
                            <xsl:text>PG</xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </xsl:element>
                <xsl:element name="host">
                    <xsl:value-of select="$host"/>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <!--Lấy avatar loại lớn,KO DÙNG NỮA-->
    <xsl:template name="getBigAvatar">
        <xsl:param name="detailLink"/>
        <xsl:param name="host"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='slick2']//img//@src"/>
        <xsl:value-of select="concat($host,$outside)"/>
    </xsl:template>
    
    <!--Ta sẽ lấy ảnh chi tiết cho từng loại gundam-->
    <xsl:template name="getDetail">
        <xsl:param name="detailLink"/>
        <xsl:param name="host"/>
        <xsl:variable name="outside" select="$detailLink//div[@class='fixs']//img"/>
        <xsl:for-each select="$outside">
            <xsl:element name="picture">
                <xsl:value-of select="@data-src"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
