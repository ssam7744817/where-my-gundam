/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;

/**
 *
 * @author HiruK
 */
public class ClickDTO implements Serializable{
    private int gundaminanimeid;
    private String gundamid;

    public ClickDTO() {
    }

    public ClickDTO(int gundaminanimeid, String gundamid) {
        this.gundaminanimeid = gundaminanimeid;
        this.gundamid = gundamid;
    }

    public int getGundaminanimeid() {
        return gundaminanimeid;
    }

    public void setGundaminanimeid(int gundaminanimeid) {
        this.gundaminanimeid = gundaminanimeid;
    }

    public String getGundamid() {
        return gundamid;
    }

    public void setGundamid(String gundamid) {
        this.gundamid = gundamid;
    }
    
}
