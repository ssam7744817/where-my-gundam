<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : pdffirst.xsl
    Created on : April 4, 2020, 9:58 AM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
    <xsl:output method="xml" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <xsl:apply-templates select="Report"/>
    </xsl:template>
    <xsl:template match="Report">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="x" page-height="8.5in"
                                       page-width="11in" margin-top="0.5in"
                                       margin-bottom="1in" margin-right="0.2in"
                                       margin-left="0.2in"
                >
                    <fo:region-body margin-top="0.5in"/>
                    <fo:region-before margin-top="1in"/>
                    <fo:region-after margin-top=".75in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="x">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block 
                        font-size="18pt"
                        font-family="sans-serif"
                        line-height="24pt" background-color="orange"
                        space-after.optimum="15pt"
                        text-align="center"
                        padding-top="3pt"
                        margin-bottom="0.3in"
                    >
                        Statistical
                    </fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block 
                        font-size="18pt"
                        font-family="sans-serif"
                        line-height="24pt" background-color="#33CCCC"
                        space-after.optimum="15pt"
                        text-align="center"
                        padding-top="3pt">
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="left" background-color="#33CCCC" font-size="14pt" padding-top="2pt" padding-bottom="2pt">                       
                        List of most selected gundam models
                    </fo:block>
                    <fo:block margin-bottom="0.8in">
                        <fo:table border-collapse="separate" table-layout="fixed">

                            <!--<fo:table-column column-number="1"/>-->
                            <fo:table-column column-number="1" column-width="10cm"/>
                            <fo:table-column column-number="2"/>
                            <fo:table-column column-number="3"/>
                            <fo:table-column column-number="4" column-width="5cm"/>
                            <fo:table-column column-number="5"/>
                            <fo:table-column column-number="6"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            The name of gundam
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Price
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Type
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Page
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            The number of occurrences
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Number of visits to sales
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <xsl:for-each select="//ModelGundam">
                                    <fo:table-row>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="gundamName//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="price//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="type//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="page//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center" color="red">
                                                <xsl:value-of select="clickCount//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center" color="rgb(28,198,181)">
                                                <xsl:value-of select="viewCount//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block>
                        <fo:block text-align="left" background-color="rgb(90,243,167)" font-size="14pt" padding-top="2pt" padding-bottom="2pt">                       
                            Statistics of clicks and views to sales page on each gundam model
                        </fo:block>
                        <fo:table border-collapse="separate" table-layout="fixed">

                            <!--<fo:table-column column-number="1"/>-->
                            <fo:table-column column-number="1" column-width="10cm"/>
                            <fo:table-column column-number="2"/>
                            <fo:table-column column-number="3"/>
                            <fo:table-body>
 
                                <fo:table-row>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Gundam Host
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Number of clicks
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Number of page views
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <xsl:for-each select="//Host">
                                    <fo:table-row>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="hostName//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center" color="red">
                                                <xsl:value-of select="clickCount//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="viewCount//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block>
                        <fo:block text-align="left" background-color="rgb(90,243,167)" font-size="14pt" padding-top="2pt" padding-bottom="2pt" margin-top="0.2in">                       
                            Statistics of clicks and views to sales page on each gundam model
                        </fo:block>
                        <fo:table border-collapse="separate" table-layout="fixed">

                            <!--<fo:table-column column-number="1"/>-->
                            <fo:table-column column-number="1" column-width="10cm"/>
                            <fo:table-column column-number="2"/>
                            <fo:table-body>

                                <fo:table-row>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Gundam Type
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        border-color="black"
                                        border-width="0.5pt"
                                        border-style="solid"
                                        background-color="#FFCC00"
                                    >
                                        <fo:block text-align="center">
                                            Number of clicks
                                        </fo:block>
                                    </fo:table-cell>
                                   
                                </fo:table-row>
                                <xsl:for-each select="//Type">
                                    <fo:table-row>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center">
                                                <xsl:value-of select="typeName//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell
                                            border-color="rgb(234,234,234)"
                                            border-width="0.5pt"
                                            border-style="solid"
                                        >
                                            <fo:block text-align="center" color="red">
                                                <xsl:value-of select="clickCount//text()"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

</xsl:stylesheet>
