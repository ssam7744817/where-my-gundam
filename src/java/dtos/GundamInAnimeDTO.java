    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;

/**
 *
 * @author HiruK
 */
public class GundamInAnimeDTO implements Serializable{
    private String name,scale,avatar,animeId,shortKey;
    private int id;

    public GundamInAnimeDTO() {
    }

    public int getId() {
        return id;
    }

    public String getShortKey() {
        return shortKey;
    }

    public void setShortKey(String shortKey) {
        this.shortKey = shortKey;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GundamInAnimeDTO(String name, String scale, String avatar, String animeId,int id) {
        this.name = name;
        this.scale = scale;
        this.avatar = avatar;
        this.animeId = animeId;
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAnimeId() {
        return animeId;
    }

    public void setAnimeId(String animeId) {
        this.animeId = animeId;
    }
    
}
