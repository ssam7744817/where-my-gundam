/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constant;

/**
 *
 * @author HiruK
 */
public class Constant {

    public static final String TYPELIST = "typeList";
    public static final String SLIDELiST = "slideList";

    //xml
    public static final String K2T = "WEB-INF/xml/gundamk2t.xml";
    public static final String C3 = "WEB-INF/xml/gundamkc3.xml";
    public static final String YMS = "WEB-INF/xml/gundamyms.xml";

    //xsl
    public static final String K2TXSL = "WEB-INF/xsl/gundamk2t.xsl";
    public static final String C3XSL = "WEB-INF/xsl/gundamkc3.xsl";
    public static final String YMSXSL = "WEB-INF/xsl/gundamyms.xsl";

    //output
    public static final String OUTPUT = "WEB-INF/output/output.xml";
    public static final String OUTPUTHobby = "WEB-INF/output/hobbyouput.xml";
    public static final String TEMPXML = "WEB-INF/output/temp.xml";

    //Đường dẫn tại app
    public static final String K2Tsource = "WEB-INF/xml/gundamk2t.xml";
    public static final String PERFUME = "WEB-INF/xml/perfume.xml";
    public static final String HoppySearchSource = "WEB-INF/xml/hobbysearch.xml";
    public static final String C3source = "WEB-INF/xml/gundamc3.xml";
    public static final String YMSsource = "WEB-INF/xml/gundamyms.xml";
    public static final String OUTPUTSOURCE = "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\output\\output.xml";
    public static final String TEMPSOURCE = "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\output\\temp.xml";
    public final static String xslc3 = "WEB-INF/xsl/gundamc3.xsl";
    public final static String xslyms = "WEB-INF/xsl/gundamyms.xsl";
    public final static String xslk2t = "WEB-INF/xsl/gundamk2t.xsl";
    public final static String XSLPERFUME = "WEB-INF/xsl/perfume.xsl";
    public final static String xslhobby = "WEB-INF/xsl/hobbysearch.xsl";
    public final static String render = "WEB-INF/xsl/rightdiv.xsl";
    public final static String pdf = "WEB-INF/xsl/pdf.xsl";

    //schema
    public final static String schema = "WEB-INF/schema/gundams.xsd";
    public final static String animeSchema = "WEB-INF/schema/anime.xsd";

}
