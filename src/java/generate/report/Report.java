
package generate.report;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelGundam" type="{}detailinfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Host" type="{}hostinfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Type" type="{}typeinfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modelGundam",
    "host",
    "type"
})
@XmlRootElement(name = "Report")
public class Report {

    @XmlElement(name = "ModelGundam")
    protected List<Detailinfo> modelGundam=new ArrayList<>();
    @XmlElement(name = "Host")
    protected List<Hostinfo> host=new ArrayList<>();
    @XmlElement(name = "Type")
    protected List<Typeinfo> type=new ArrayList<>();

    /**
     * Gets the value of the modelGundam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modelGundam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModelGundam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Detailinfo }
     * 
     * 
     */
    public List<Detailinfo> getModelGundam() {
        if (modelGundam == null) {
            modelGundam = new ArrayList<Detailinfo>();
        }
        return this.modelGundam;
    }

    /**
     * Gets the value of the host property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the host property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Hostinfo }
     * 
     * 
     */
    public List<Hostinfo> getHost() {
        if (host == null) {
            host = new ArrayList<Hostinfo>();
        }
        return this.host;
    }

    /**
     * Gets the value of the type property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the type property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Typeinfo }
     * 
     * 
     */
    public List<Typeinfo> getType() {
        if (type == null) {
            type = new ArrayList<Typeinfo>();
        }
        return this.type;
    }

}
