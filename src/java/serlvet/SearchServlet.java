/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import daos.GundamDAO;
import daos.GundamInAnimeDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HiruK
 */
public class SearchServlet extends HttpServlet {

    private final String SEARCH = "search.jsp";
    private final String ADMIN = "admin.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String searchValue = request.getParameter("txtSearch");
        String page = request.getParameter("txtPage");
        String animeId = request.getParameter("animeId");
        String aniem = request.getParameter("anime");
        String name = request.getParameter("name");
        String avatar = request.getParameter("avatar");
        String autoSearch = request.getParameter("autoSearch");
        String url = "";
        HttpSession session = request.getSession();
        System.out.println("name= " + name + " avatar= " + avatar + " search value= " + searchValue + " animeId= " + animeId);

        String nameInSession = (String) session.getAttribute("name");
        String avatarInSession = (String) session.getAttribute("avatar");
        String animeIdInSession = (String) session.getAttribute("animeId");

        //Ta set cho 2 cái này vào session để nó ko mất mỗi khi phân trang
        if ((name != null) && (avatar != null)) {
            System.out.println("get it");
            if (!name.equals(nameInSession) && !avatar.equals(avatarInSession)) {
                session.setAttribute("name", name);
                session.setAttribute("avatar", avatar);
            }
        }
        if (animeId != null) {
            if (!animeId.equals(animeIdInSession)) {
                session.setAttribute("animeId", animeId);
            }
        }

        GundamDAO dao = new GundamDAO();
        try {

            if (page.equals("admin")) {
                url = ADMIN;
            } else {
                url = SEARCH;
            }
            if (aniem != null) {
                if (aniem.equals("yes")) {
//               Phân biệt kiểu search từ home với search page

                    GundamInAnimeDAO animeDao = new GundamInAnimeDAO();
                    //Phần này dành cho top pagging
                    int pagging = 1;
                    int recordsPerPage = 10;
                    if (request.getParameter("toppagging") != null) {
                        System.out.println("topppaging= " + request.getParameter("toppagging"));
                        pagging = Integer.parseInt(request.getParameter("toppagging"));
                    }
                    /**
                     * Bây h ta sẽ đưa ra những kết quả dựa mà đô ưu tiên xuất
                     * hiện của các item sẽ dựa trên lượt click Cái nào click
                     * nhiều thì sẽ đc hiển thị trước
                     */
                    System.out.println("page= " + pagging + " record per page= " + recordsPerPage);
                    if (animeIdInSession == null) {
                        //Câu lệnh này dùng để bắt trường hợp khi mới lần đầu khởi động web và session chả có g hết, nên ta sẽ dùng đến animeId
                        animeDao.getGundaminAnimeWithPagingPrioritize(
                                animeId,
                                (pagging - 1) * recordsPerPage,
                                recordsPerPage);
                    } else {
                        if (request.getParameter("toppagging") == null) {
                            animeDao.getGundaminAnimeWithPagingPrioritize(animeId, (pagging - 1) * recordsPerPage,
                                    recordsPerPage);
                        } else {
                            animeDao.getGundaminAnimeWithPagingPrioritize(animeIdInSession, (pagging - 1) * recordsPerPage,
                                    recordsPerPage);
                        }
                    }

                    int noOfRecords = animeDao.getPagingCount();//Tổng số record có trong list
                    int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                    System.out.println("number of gundam in anime= " + noOfPages);
                    //Bây h ta sẽ lưu nó trong seassion
                    request.setAttribute("GUNDAMINANIME", animeDao.getList());
                    request.setAttribute("noOfTopPages", noOfPages);
                    request.setAttribute("currentTopPage", pagging);
                    System.out.println("gundam in anime size= " + animeDao.getList().size());

                }
            }
            
            /**
             * Phần tiếp theo sau khi click và cái link ảnh
             * (2): từ shorkey mà tôi đã băm ra sau khi crawl dữ liệu về
             * Giờ đây, mỗi lần tôi click vào tấm ảnh nào thì nó sẽ tự động tìm ra
             * mô hình đó được bán ở cửa hàng Việt Nam nào
             * Toàn bộ code dưới đây chỉ là phần xử lí để hiển thị ra kết quả thôi, nên đừng quan tâm
             * Làm sao có đc shortkey thì ta xem tiếp trang CrawlService.java
             */
            if (autoSearch != null) {
                //Phần này dành cho paging
                String shortKey = request.getParameter("shortkey");
                System.out.println("short key= " + shortKey);
                int pagging = 1;
                int recordsPerPage = 9;
                if (request.getParameter("pagging") != null) {
                    pagging = Integer.parseInt(request.getParameter("pagging"));
                }
                if (shortKey != null) {
                    if (shortKey != "") {
                        dao.searchGundamWithPaging(shortKey, (pagging - 1) * recordsPerPage,
                                recordsPerPage);
                        int noOfRecords = dao.getPagingCount();//Tổng số record có trong list
                        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                        System.out.println("number of page= " + noOfPages);
                        //Bây h ta sẽ lưu nó trong seassion
                        request.setAttribute("SEARCH", dao.getJaxbList().getGundam());

                        //Mình sẽ lưu thêm 1 cái list search nữa để phục vụ cho việc tạo đẹp giá và hiện chi tiết, cái này sẽ đc dùng bên
                        //file GetDetailImageServlet
                        session.setAttribute("SEARCH", dao.getJaxbList().getGundam());

                        request.setAttribute("shortkey", shortKey);
                        request.setAttribute("noOfPages", noOfPages);
                        request.setAttribute("currentPage", pagging);
                    } else {
                        //Nếu như kết quả gợi ý mà null thì ta sẽ đưa cho họ 1 thông báo
                        request.setAttribute("NOTICE", "Có vẻ như chúng tôi ko tìm thấy được mô hình gundam trong anime này,"
                                + "Hãy thử với ô tìm kiếm ");
                    }
                }
            } else {
                if (searchValue != null) {
                    if (!searchValue.equals("")) {
                        //Phần này dành cho paging
                        int pagging = 1;
                        int recordsPerPage = 9;
                        if (request.getParameter("pagging") != null) {
                            pagging = Integer.parseInt(request.getParameter("pagging"));
                        }
                        dao.searchGundamWithPagingPrioprity(searchValue, (pagging - 1) * recordsPerPage,
                                recordsPerPage);
                        int noOfRecords = dao.getPagingCount();//Tổng số record có trong list
                        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                        System.out.println("number of page= " + noOfPages);
                        //Bây h ta sẽ lưu nó trong seassion
                        request.setAttribute("SEARCH", dao.getJaxbList().getGundam());

                        if (dao.getJaxbList().getGundam().isEmpty()) {
                            //Nếu như kết quả gợi ý mà null thì ta sẽ đưa cho họ 1 thông báo
                            request.setAttribute("EMPTY", "Không có kết quả nào được hiển thị");

                        }
                        //Mình sẽ lưu thêm 1 cái list search nữa để phục vụ cho việc tạo đẹp giá và hiện chi tiết, cái này sẽ đc dùng bên
                        //file GetDetailImageServlet
                        session.setAttribute("SEARCH", dao.getJaxbList().getGundam());

                        request.setAttribute("noOfPages", noOfPages);
                        request.setAttribute("currentPage", pagging);
                    } 
                }
            }

            request.getRequestDispatcher(url).forward(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
