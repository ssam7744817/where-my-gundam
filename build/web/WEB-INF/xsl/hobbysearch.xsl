<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hobbysearch.xsl
    Created on : March 26, 2020, 12:33 PM
    Author     : HiruK
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:ani="http://xml.netbeans.org/schema/animes"
                xmlns="http://xml.netbeans.org/schema/animes"
                version="1.0">
    <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="ani:animes">
        <xsl:variable name="listDoc" select="document(ani:link//@hr)" />
        <xsl:variable name="host" select="ani:link//@host" />
        <xsl:variable name="gundam_series" select="$listDoc//a[@class='LListTyp3']" />
        <xsl:element name="animes">
            <xsl:for-each select="$gundam_series">
                <xsl:variable name="link" select="concat($host,@href)"/>
                <xsl:variable name="name" select="div//text()"/>
                <xsl:choose>
                    <xsl:when test="$name='Gundam Build Fighters'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Gundam X'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Iron-Blooded Orphans'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Gundam W'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Gundam SEED / Destiny'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Gundam UC'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='The 08th MS Team'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='First Gundam'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Gundam 00'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='Z / ZZ Gundam'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='SD Gundam Sangokuden'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="$name='SD Gundam Sengokuden'">
                        <xsl:element name="anime">
                            <xsl:element name="anime_name">
                                <xsl:value-of select="$name"/>
                            </xsl:element>
                            <xsl:call-template name="getGundam">
                                <xsl:with-param name="linkdetail" select="$link"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    
    
    <xsl:template name="getGundam">
        <xsl:param name="linkdetail"/>    
        <xsl:param name="host"/>    
        <xsl:variable name="doc" select="document($linkdetail)"/>
        <xsl:variable name="parent" select="$doc//li[@class='parent']"/>
        <!--Đi vào từng cái filter type-->
        <xsl:for-each select="$parent">
            <xsl:if test="contains(a/text(),'Scale')">
                <xsl:for-each select="div/ul/li">
                    <!--Sau khi đã xác định được những li chứa type, ta tiến hành chọn ra những type mà ta cần-->
                    <xsl:choose>
                        <xsl:when test="contains(a/text(),'1/144')">
                            <xsl:call-template name="getInfo">
                                <xsl:with-param name="scaleListDoc" select="document(concat($host,a/@href))"/>
                                <xsl:with-param name="scale" select="'1/144'"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="contains(a/text(),'1/100')">
                            <xsl:call-template name="getInfo">
                                <xsl:with-param name="scaleListDoc" select="document(concat($host,a/@href))"/>
                                <xsl:with-param name="scale" select="'1/100'"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>

                        </xsl:when>
                        <xsl:when test="contains(a/text(),'1/60')">
                            <xsl:call-template name="getInfo">
                                <xsl:with-param name="scaleListDoc" select="document(concat($host,a/@href))"/>
                                <xsl:with-param name="scale" select="'1/60'"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>

                        </xsl:when>
                        <xsl:when test="contains(a/text(),'Non')">
                            <xsl:call-template name="getInfo">
                                <xsl:with-param name="scaleListDoc" select="document(concat($host,a/@href))"/>
                                <xsl:with-param name="scale" select="'non'"/>
                                <xsl:with-param name="host" select="$host"/>
                            </xsl:call-template>

                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <!--Hàm này có tác dụng crawl về từng con gundam-->
    <xsl:template name="getInfo">
        <xsl:param name="scaleListDoc"/>
        <!--Link chi tiết-->
        <xsl:param name="scale"/>
        <!--Biến scale-->
        <xsl:param name="host"/>
        
        <xsl:for-each select="$scaleListDoc//div[@class='tmbPicArea']">
            <xsl:element name="gundaminanime">
                <!--<xsl:value-of select="count($scaleListDoc//div[@class='tmbPicArea'])"/>-->
                <xsl:element name="name">
                    <xsl:value-of select="img/@title"/>
                </xsl:element>
                <xsl:element name="avatar">
                    <xsl:value-of select="concat($host,img/@src)"/>
                </xsl:element>
                <xsl:element name="scale">
                    <xsl:value-of select="$scale"/>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
        
        <!--Lấy ra link tiếp theo để cào-->
        <xsl:for-each select="$scaleListDoc//div[@class='list_kensu03']/a">
            <xsl:if test="img[@src='/images/list_kensu_yajirushi_r.gif']">
                <xsl:call-template name="getInfo">
                    <xsl:with-param name="scaleListDoc" select="document(concat($host,@href))"/>
                    <xsl:with-param name="scale" select="$scale"/>
                    <xsl:with-param name="host" select="$host"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
