/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.GundamModelTypeDTO;
import dtos.GundamSlideShowDTO;
import generate.gundams.Gundam;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HiruK
 */
public class GundamSlideShowDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    private List<GundamSlideShowDTO> list = new ArrayList<GundamSlideShowDTO>();

    public List<GundamSlideShowDTO> getList() {
        return list;
    }

    public void setList(List<GundamSlideShowDTO> list) {
        this.list = list;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public void getImageName() throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "  FROM [dbo].[gundam_slide_show]";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                GundamSlideShowDTO dto = new GundamSlideShowDTO(id, name);
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(dto);
            }
        } finally {
            closeConnection();
        }
    }

}
