/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.validation;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.transform.dom.DOMResult;

/**
 *
 * @author HiruK
 */
public class JAXBGundamValidationHanlder implements ValidationEventHandler{
    /**
     * Cách nào thiếu hiệu quả vì ko biêt cách nào để modify cái node cần sửa, vì bây h nó đang đọc nguyên 1 file
     * xml luôn, chứ ko phải từng node,
     * @param event
     * @return 
     */
    @Override
    public boolean handleEvent(ValidationEvent event) {
        if(event.getSeverity()==ValidationEvent.FATAL_ERROR|| event.getSeverity()==ValidationEvent.ERROR){
            ValidationEventLocator locator=event.getLocator();
            System.out.println("Invalid gundam document: "+locator.getURL());
            System.out.println("Error: "+event.getMessage());
            System.out.println("Error at column: "+locator.getColumnNumber()+", line"+locator.getLineNumber());
            
            //Xử lí lỗi ở đây luôn, ko nên xử lí lỗi ở đây
           //XMLValidate validate=new XMLValidate();
          //validate.prepareGundamObjectWithDOMResult(dom);
        }
        return true;
    }
    
}
