/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import outputenum.HostEnum;
import outputenum.TypeEnum;

/**
 *
 * @author HiruK
 */
public class TypeUtil {
        /**
     * Chuyển loại gundam thành số
     *
     * @param type
     * @return
     */
    public static int convertStringTypeToNumber(String type) {
        int number = 0;
        if (type.equals(TypeEnum.HG.toString())) {
            number = 1;
        } else if (type.equals(TypeEnum.MG.toString())) {
            number = 3;
        } else if (type.equals(TypeEnum.PG.toString())) {
            number = 5;
        } else if (type.equals(TypeEnum.RG.toString())) {
            number = 2;
        } else if (type.equals(TypeEnum.SD.toString())) {
            number = 4;
        }
        return number;
    }
    /**
     * Chuyển type số thành chữ
     * @param number
     * @return 
     */
    public static String convertNumberTypeToString(int number){
        String type="";
        switch(number){
            case 1:
                type=TypeEnum.HG.toString();
                break;
            case 3:
                type=TypeEnum.MG.toString();
                break;
            case 5:
                type=TypeEnum.PG.toString();
                break;
            case 2:
                type=TypeEnum.RG.toString();
                break;
            case 4:
                type=TypeEnum.SD.toString();
                break;
        }
        return type;
    }
    
    /**
     * Chuyển host number thành định dạng đầy đủ
     * @param number
     * @return 
     */
    public static String convertNumberHostToString(int number){
        String host="";
        switch(number){
            case 1:
                host=HostEnum.C3.toString();
                break;
            case 2:
                host=HostEnum.K2T.toString();
                break;
            case 3:
                host=HostEnum.YMS.toString();
                break;
        }
        return host;
    }
}
