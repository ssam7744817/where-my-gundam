USE [master]
GO
/****** Object:  Database [gundams]    Script Date: 4/6/2020 1:19:32 PM ******/
CREATE DATABASE [gundams]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'gundams', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SAMMSQLSERVER\MSSQL\DATA\gundams.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'gundams_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SAMMSQLSERVER\MSSQL\DATA\gundams_log.ldf' , SIZE = 139264KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [gundams] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [gundams].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [gundams] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [gundams] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [gundams] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [gundams] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [gundams] SET ARITHABORT OFF 
GO
ALTER DATABASE [gundams] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [gundams] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [gundams] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [gundams] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [gundams] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [gundams] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [gundams] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [gundams] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [gundams] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [gundams] SET  DISABLE_BROKER 
GO
ALTER DATABASE [gundams] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [gundams] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [gundams] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [gundams] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [gundams] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [gundams] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [gundams] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [gundams] SET RECOVERY FULL 
GO
ALTER DATABASE [gundams] SET  MULTI_USER 
GO
ALTER DATABASE [gundams] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [gundams] SET DB_CHAINING OFF 
GO
ALTER DATABASE [gundams] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [gundams] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [gundams] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'gundams', N'ON'
GO
ALTER DATABASE [gundams] SET QUERY_STORE = OFF
GO
USE [gundams]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [gundams]
GO
/****** Object:  Table [dbo].[account]    Script Date: 4/6/2020 1:19:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[name] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[username] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[anime]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[anime](
	[id] [nvarchar](50) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[avatar] [nvarchar](max) NULL,
 CONSTRAINT [PK_anime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[click_info]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[click_info](
	[gundaminanimeid] [int] NULL,
	[gundamid] [nvarchar](50) NULL,
	[type] [int] NULL,
	[host] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam](
	[name] [nvarchar](max) NOT NULL,
	[detailLink] [nvarchar](max) NOT NULL,
	[avatar] [nvarchar](max) NOT NULL,
	[price] [int] NOT NULL,
	[statusId] [int] NOT NULL,
	[typeId] [int] NOT NULL,
	[hostId] [int] NOT NULL,
	[id] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_gundam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam_in_anime]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam_in_anime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[scale] [nvarchar](max) NOT NULL,
	[avatar] [nvarchar](max) NOT NULL,
	[animeId] [nvarchar](50) NOT NULL,
	[shortkey] [nvarchar](max) NULL,
 CONSTRAINT [PK_gundam_in_anime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam_model_type]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam_model_type](
	[typeId] [int] NOT NULL,
	[name] [nchar](10) NOT NULL,
 CONSTRAINT [PK_gundamModelType] PRIMARY KEY CLUSTERED 
(
	[typeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam_slide_show]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam_slide_show](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam_status]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam_status](
	[statusId] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_gundamStatus] PRIMARY KEY CLUSTERED 
(
	[statusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gundam_web_host]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gundam_web_host](
	[hostId] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_gundamwebhost] PRIMARY KEY CLUSTERED 
(
	[hostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[picture]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[picture](
	[id] [nvarchar](100) NOT NULL,
	[picture] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[search_value]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[search_value](
	[searchvalue] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[topId]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[topId](
	[id] [nvarchar](50) NOT NULL,
	[dateAppearance] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[viewpage]    Script Date: 4/6/2020 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[viewpage](
	[animeid] [nvarchar](50) NULL,
	[gundaminshopid] [nvarchar](50) NULL,
	[host] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[gundam]  WITH CHECK ADD  CONSTRAINT [FK_gundam_gundamModelType] FOREIGN KEY([typeId])
REFERENCES [dbo].[gundam_model_type] ([typeId])
GO
ALTER TABLE [dbo].[gundam] CHECK CONSTRAINT [FK_gundam_gundamModelType]
GO
ALTER TABLE [dbo].[gundam]  WITH CHECK ADD  CONSTRAINT [FK_gundam_gundamStatus] FOREIGN KEY([statusId])
REFERENCES [dbo].[gundam_status] ([statusId])
GO
ALTER TABLE [dbo].[gundam] CHECK CONSTRAINT [FK_gundam_gundamStatus]
GO
ALTER TABLE [dbo].[gundam]  WITH CHECK ADD  CONSTRAINT [FK_gundam_gundamWebHost] FOREIGN KEY([hostId])
REFERENCES [dbo].[gundam_web_host] ([hostId])
GO
ALTER TABLE [dbo].[gundam] CHECK CONSTRAINT [FK_gundam_gundamWebHost]
GO
ALTER TABLE [dbo].[gundam_in_anime]  WITH CHECK ADD  CONSTRAINT [FK_gundam_in_anime_anime] FOREIGN KEY([animeId])
REFERENCES [dbo].[anime] ([id])
GO
ALTER TABLE [dbo].[gundam_in_anime] CHECK CONSTRAINT [FK_gundam_in_anime_anime]
GO
ALTER TABLE [dbo].[picture]  WITH CHECK ADD  CONSTRAINT [FK_picture_gundam] FOREIGN KEY([id])
REFERENCES [dbo].[gundam] ([id])
GO
ALTER TABLE [dbo].[picture] CHECK CONSTRAINT [FK_picture_gundam]
GO
USE [master]
GO
ALTER DATABASE [gundams] SET  READ_WRITE 
GO
