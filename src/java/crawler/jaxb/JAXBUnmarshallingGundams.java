/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.jaxb;

import crawler.validation.JAXBGundamValidationHanlder;
import generate.anime.Animes;
import generate.gundams.Gundams;
import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.dom.DOMResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author HiruK
 */
public class JAXBUnmarshallingGundams {

    public static Schema getSchema(String xsdFile) throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema sc = factory.newSchema(new File(xsdFile));
        return sc;
    }

    /**
     * Hàm tạo object với value từ file xml, với tham số là file xml sau khi đã
     * validate dữ liệu
     *
     * @return
     * @throws JAXBException
     */
    public Gundams creatGundamObject(String input, String schema) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Gundams.class);
        Unmarshaller u = jc.createUnmarshaller();
        u.setSchema(getSchema(schema));
        u.setEventHandler(new JAXBGundamValidationHanlder());// set 1 event validation
        File fi = new File(input);
        Gundams gundam = (Gundams) u.unmarshal(fi);
        return gundam;
    }

    /**
     * Tạo ra Anime object sử dụng file để test, CHỈ DÙNG CHO MỤC ĐÍCH TESTING
     *
     * @return
     * @throws JAXBException
     */
    public Animes creatAnimeObjectFileVersion(String input, String schema) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Animes.class);
        Unmarshaller u = jc.createUnmarshaller();
        u.setSchema(getSchema(schema));
        u.setEventHandler(new JAXBGundamValidationHanlder());// set 1 event validation
        File fi = new File(input);
        Animes type = (Animes) u.unmarshal(fi);
        return type;
    }
    /**
     * Biến xml thành java object, đối vỡi mỗi java object thì cứ viết 1 hàm riêng
     * kiểu tôi có class Anime, Gundam thì tôi sẽ viết ra 2 hàm là creatAnimeObject và creatGundamObject
     * Có cách chỉ cần viết 1 hàm là có thể áp dụng cho mọi class nhưng tôi ko biết
     * @param schema
     * @return
     * @throws JAXBException
     * @throws SAXException 
     */
    public Animes creatAnimeObject(DOMResult rs,String schema) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Animes.class);
        Unmarshaller u = jc.createUnmarshaller();
        u.setSchema(getSchema(schema));//Gắn schema để validate dữ liệu
        u.setEventHandler(new JAXBGundamValidationHanlder());// set 1 event validation
        Animes type = (Animes) u.unmarshal(rs.getNode());
        return type;
    }

    /**
     * Hàm tạo object với value từ file xml, với tham số là file xml sau khi đã
     * validate dữ liệu
     *
     * @return
     * @throws JAXBException
     */
    public Gundams creatGundamObjectWithDOM(DOMResult input, String schema) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Gundams.class);
        Unmarshaller u = jc.createUnmarshaller();
        u.setSchema(getSchema(schema));
        u.setEventHandler(new JAXBGundamValidationHanlder());
// set 1 event validation
        Gundams gundam = (Gundams) u.unmarshal(input.getNode());
        return gundam;
    }

    /**
     * Dùng phần này chỉ để test
     *
     * @param args
     */
    public static void main(String[] args) {
//        try {
//            Gundams gundam = creatGundamObject();
//            for (int i = 0; i < gundam.getGundam().size(); i++) {
//                Gundam gundamDetai = gundam.getGundam().get(i);
//                System.out.println("this is name: " + gundamDetai.getName());
//                System.out.println("this is price: " + gundamDetai.getPrice());
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
