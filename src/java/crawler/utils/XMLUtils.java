/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.utils;

import constant.Constant;
import crawler.resolver.UltimateURIResolver;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author HiruK
 */
public class XMLUtils {

    /**
     *Bản chất của việc cào dữ liệu là biến source thành result, source ở đây có thể là 1 file, 1 chuỗi
     * result trả về thường là DOMResult, từ DOMResult ta có thể biến nó thành String hay file để tiện cho việc
     * kiểm tra.Làm thế nào để kiểm tra DOMResult của mình có như ý mình hay ko tôi cũng sé hướng dẫn ở dưới
     * 
     *
     * Tôi sử dụng xslt để làm parser sử dụng state machine để wellform, 
     * Các ông có thể sử dụng lại hàm này của tôi, tôi đã fix hết lỗi rồi
     * Cái khác biệt duy nhất là trang web để cào và file stylesheet mà tôi sẽ nói rỗ
     * @param configPath: file xml đầu vào, gồm các file tên gundamc3.xml, gundamk2t.xml, gundamyms.xml và hoppysearch.xml nằm trong WEBINF/xml
     * @param xslPath: file stylesheet để định dạng cây DOM, tôi hời lười viết tên nhưng chỗ tôi dùng hàm này cái phần map ak
     * tôi dùng để đưa param vào, là tên luôn đó
     * @return
     * @throws Exception
     * 
     */
    public static DOMResult parseToDOM(String configPath, String xslPath) throws Exception {
        StreamSource xslCate = new StreamSource(xslPath);
        InputStream is = new FileInputStream(configPath);

        TransformerFactory factory = TransformerFactory.newInstance();
        DOMResult dOMResult = new DOMResult();
        UltimateURIResolver resolver = new UltimateURIResolver();//Đây là phần wellform html

        factory.setURIResolver(resolver);
        if (factory.getURIResolver() != null) {
            /**
             * Transformer hay còn gọi là Trax để parse source thành result
             * Ngoài ra còn có JAXB cũng có chức năng tương tự nhưng chỉ dùng đẻ
             * biến java object thành xml và ngược lại
             */
            Transformer transformer = factory.newTransformer(xslCate);
            transformer.transform(new StreamSource(is), dOMResult);
            return dOMResult;//Kết quả trả về là 1 DOMResult, từ đây các ông muốn biến nó thành string hay file đều đc, tôi cũng có viết các hàm đó ra, yên tâm
        }
        return null;
    }

    /**
     * Phiên bản để ứng dụng stylesheet vào xml để trình bày giao diện trên xml
     * Nói đơn giản hơn là cái này dùng cho mục đích là gắn xml lên trang jsp, render thành giao diện
     *
     * @param configPath
     * @param xslPath
     * @return
     * @throws Exception
     */
    public static DOMResult parseToDOMNoRecolver(String configPath, String xslPath) throws Exception {
        System.out.println("Đã vào parse");
        StreamSource xslCate = new StreamSource(xslPath);
        InputStream is = new FileInputStream(configPath);

        TransformerFactory factory = TransformerFactory.newInstance();
        DOMResult dOMResult = new DOMResult();
        Transformer transformer = factory.newTransformer(xslCate);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new StreamSource(is), dOMResult);
        return dOMResult;
    }

    /**
     * Chức năng cũng tương tự như hàm parseToDOMNoRecolver nhưng đầu vào là 1 domresult
     *
     * @param configPath
     * @param xslPath
     * @return
     * @throws Exception
     */
    public static DOMResult parseToDOMNoRecolverWIthDomresultInput(DOMResult configPath, String xslPath) throws Exception {
        System.out.println("Đã vào parse");
        StreamSource xslCate = new StreamSource(xslPath);
        DOMSource source = new DOMSource(configPath.getNode());

        TransformerFactory factory = TransformerFactory.newInstance();
        DOMResult dOMResult = new DOMResult();

        Transformer transformer = factory.newTransformer(xslCate);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, dOMResult);

        return dOMResult;//1 domresult mới sau khi apply stylesheet
    }

    /**
     * Chuyển đổi xml có applystylesheet thành string
     * Đây chính là hàm chuyển đổi DOMResult thành string
     * Các ông có thể dùng đầu ra để kiểm tra xem có đúng ý mình hay ko
     * Nhưng rất khó nhìn, sử dụng file dễ nhìn hơn nhiều
     *
     * @param configPath
     * @param xslPath
     * @return
     * @throws Exception
     */
    public static String parseDomToXMLString(DOMResult configPath, String xslPath) throws Exception {
        System.out.println("Đã vào parse");
        StreamSource xslCate = new StreamSource(xslPath);
        DOMSource source = new DOMSource(configPath.getNode());

        //Mở string writter
        StringWriter writer = new StringWriter();

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(xslCate);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, new StreamResult(writer));

        return writer.getBuffer().toString();//1 domresult mới sau khi apply stylesheet
    }

    /**
     * Biến dom thành fie pdf và lưu dưới dạng string
     * @param xml
     * @param xsl
     * @param output
     * @throws FileNotFoundException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public static void parseDomToPDF(DOMResult xml, String xsl, String output) throws FileNotFoundException, TransformerConfigurationException, TransformerException {
        System.out.println("Đã vào parse");
        StreamSource xslCate = new StreamSource(xsl);
        DOMSource source = new DOMSource(xml.getNode());
         
        //Mở string writter
//        StringWriter writer = new StringWriter();
        StreamResult htmlFile=new StreamResult(new FileOutputStream(output));//Đầu ra thành file pdf

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(xslCate);//apply stylesheet vào
        transformer.transform(source, htmlFile);
//        return writer.getBuffer().toString();
    }

    public static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(xmlString.getBytes("UTF-8"))));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Hàm biến DOMRessult thành file để tiện cho việc kiểm tra
     * Tuyệt đối ko áp dụng file cho bài project, thầy cho 0 điểm
     * Chỉ dùng cho mục đích testing để hiểu bài thôi
     *
     * @param result
     * @param output
     * @throws TransformerConfigurationException
     * @throws FileNotFoundException
     * @throws TransformerException
     */
    public static void TransformDOMToFile(DOMResult result, String output) throws TransformerConfigurationException, FileNotFoundException, TransformerException {
        Source source = new DOMSource(result.getNode());//Đầu vào

        //Tạo ra 1 instance của transformer
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        //tạo ra 1 đâu ra 
        StreamResult streamResult = new StreamResult(new FileOutputStream(output));
        transformer.transform(source, streamResult);
    }

    /**
     * Tạo ra element,ÉO DÙNG NỮA
     *
     * @param doc
     * @param elementName
     * @param elementValue
     * @param attribute
     * @return
     */
    public static Element creatElement(Document doc, String elementName, String elementValue, Map<String, String> attribute) {
        if (doc != null) {
            Element element = doc.createElement(elementName);
            if (elementValue != null) {
                element.setTextContent(elementValue);
            }
            if (attribute != null) {
                if (!attribute.isEmpty()) {
                    for (Map.Entry<String, String> entruy : attribute.entrySet()) {
                        element.setAttribute(entruy.getKey(), entruy.getValue());
                    }
                }
            }
            return element;
        }
        return null;
    }

    /**
     * Chuyển file thành cây DOM
     *
     * @param xmlFilePath
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document paserFileToDom(String xmlFilePath) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dom = factory.newDocumentBuilder();
        Document doc = dom.parse(xmlFilePath);
        return doc;
    }

    public static XPath getXpath() {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();
        return xPath;
    }

    /**
     * Tạo ra 1 file xml chứa các dữ liệu đc crawl từ web về, KO DÙNG HÀM NÀY
     * NỮA
     *
     * @param rs : Cây DOM result chứa các dữ liệu
     */
//    public static void concatNode(DOMResult rs) {
    public static void concatNodeWithOutFile(DOMResult rs) {
        try {
            //Lấy ra DOM từ file output tổng
            Document output = paserFileToDom(Constant.OUTPUTSOURCE);//đầu ra cuối cùng là file output
            //Kiểm tra xem có node nào bên trong file ko

            NodeList gundam = output.getElementsByTagName("gundam");
            if (gundam.getLength() == 0) {
                //Nếu như chưa có node nào thì

                // parse cây DOM ra thành FIle
                XMLUtils.TransformDOMToFile(rs, Constant.OUTPUTSOURCE);
            } else {

                //làm mới file temp
//                clearXML(input);
                // parse cây DOM ở file temp(tạm thời)
                XMLUtils.TransformDOMToFile(rs, Constant.TEMPSOURCE);
                Document input = XMLUtils.paserFileToDom(Constant.TEMPSOURCE);//Đầu vào là file temp

                //Chuyển file xml thành DOM
                //2. tiến hành nối dữ liệu
                //Tạo xpath object
                XPath xPath = getXpath();

                //Tạo xpath expression
                String nameXp = "*[local-name()='name']";
                String gundamXp = "//*[local-name()='gundam']";
                String detailLinkXp = "*[local-name()='detailLink']";
                String avatarXp = "*[local-name()='avatar']";
                String statuXps = "*[local-name()='status']";
                String priceXp = "*[local-name()='price']";
                String typXpe = "*[local-name()='type']";
                String hostXp = "*[local-name()='host']";

                //Lấy ra node gundam của file temp
                NodeList list = input.getElementsByTagName("gundam");
                NodeList listXpath = (NodeList) xPath.evaluate(gundamXp, input, XPathConstants.NODESET);
                System.out.println("do dai cua temp= " + list.getLength());
                //Lấy ra node root của output
                NodeList original = output.getElementsByTagName("gundams");

                //Bắt đầu tạo node 
                for (int i = 0; i < list.getLength(); i++) {
                    Node node = list.item(i);
                    String name = (String) xPath.evaluate(nameXp, node, XPathConstants.STRING);
                    String detailLink = (String) xPath.evaluate(detailLinkXp, node, XPathConstants.STRING);
                    String avatar = (String) xPath.evaluate(avatarXp, node, XPathConstants.STRING);
                    String status = (String) xPath.evaluate(statuXps, node, XPathConstants.STRING);
                    String price = (String) xPath.evaluate(priceXp, node, XPathConstants.STRING);
                    String type = (String) xPath.evaluate(typXpe, node, XPathConstants.STRING);
                    String host = (String) xPath.evaluate(hostXp, node, XPathConstants.STRING);

                    Element gundamE = XMLUtils.creatElement(output, "gundam", null, null);
                    Element nameE = XMLUtils.creatElement(output, "name", name, null);
                    Element detailLinkE = XMLUtils.creatElement(output, "detailLink", detailLink, null);
                    Element avatarE = XMLUtils.creatElement(output, "avatar", avatar, null);
                    Element statusE = XMLUtils.creatElement(output, "status", status, null);
                    Element priceE = XMLUtils.creatElement(output, "price", price, null);
                    Element typeE = XMLUtils.creatElement(output, "type", type, null);
                    Element hostE = XMLUtils.creatElement(output, "host", host, null);

                    gundamE.appendChild(nameE);
                    gundamE.appendChild(detailLinkE);
                    gundamE.appendChild(avatarE);
                    gundamE.appendChild(statusE);
                    gundamE.appendChild(priceE);
                    gundamE.appendChild(typeE);
                    gundamE.appendChild(hostE);

                    original.item(0).appendChild(gundamE);
                }//Kêt thúc tạo node

                //Đưa DOM mới vào file xml ouput
                TransformDOMToFile(new DOMResult(output), Constant.OUTPUTSOURCE);
                //Kết thúc quá trình in ra file

                System.out.println("list length= " + list.getLength() + "\n"
                        + "xpath legth= " + listXpath.getLength()
                        + "");
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Clear hết toàn bộ dữ liệu cũ đi KO SÀI HÀM NÀY
     *
     * @param doc
     */
    public static void clearXML(Document doc) {
        NodeList root = doc.getElementsByTagName("gundam");
        int count = 0;
        for (int i = 0; i < root.getLength(); i++) {
            Node parent = root.item(i).getParentNode();
            parent.removeChild(root.item(i));
            count++;
        }
        if (count == root.getLength()) {
            try {
                TransformDOMToFile(new DOMResult(doc), Constant.OUTPUTSOURCE);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Chỉ dùng cho mục đích testing
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            Document doc = XMLUtils.convertStringToXMLDocument("<listid><id>36cd2e8c-3c8f-452a-bb98-50a8651b7191</id><id>07acc2d0-e18c-435a-a94d-6036fa967b09</id></listid>");
            XPath xp = XMLUtils.getXpath();
            String exp = "//*[local-name()='id']";
            NodeList list = (NodeList) xp.evaluate(exp, doc, XPathConstants.NODESET);

            for (int i = 0; i < list.getLength(); i++) {
                System.out.println("Thong tin= " + list.item(i).getTextContent());
            }
//            Document output = paserFileToDom("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\output\\output.xml");//đầu ra cuối cùng là file output
//            clearXML(output);
//        try {
//            //Tạo xpath object
//            XPath xPath = getXpath();
//            String expression = "gundam";
//            String gundams = "gundams";
//
//            //Tạo xpath expression
//            String nameXp = "*[local-name()='name']";
//            String detailLinkXp = "*[local-name()='detailLink']";
//            String avatarXp = "*[local-name()='avatar']";
//            String statuXps = "*[local-name()='status']";
//            String priceXp = "*[local-name()='price']";
//            String typXpe = "*[local-name()='type']";
//            String hostXp = "*[local-name()='host']";
//
//            //Chuyển file xml thành DOM
//            Document input = XMLUtils.paserFileToDom("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputk2t.xml");
//            Document output = XMLUtils.paserFileToDom("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml");
//
//            //Lấy ra node gundam của file input
//            NodeList list = input.getElementsByTagName("gundam");
//
//            //Lấy ra node root của output
//            NodeList original = output.getElementsByTagName("gundams");
//
//            //Bắt đầu tạo node 
//            for (int i = 0; i < list.getLength(); i++) {
//                Node node = list.item(i);
//                Node name = (Node) xPath.evaluate(nameXp, node, XPathConstants.NODE);
//                Node detailLink = (Node) xPath.evaluate(detailLinkXp, node, XPathConstants.NODE);
//                Node avatar = (Node) xPath.evaluate(avatarXp, node, XPathConstants.NODE);
//                Node status = (Node) xPath.evaluate(statuXps, node, XPathConstants.NODE);
//                Node price = (Node) xPath.evaluate(priceXp, node, XPathConstants.NODE);
//                Node type = (Node) xPath.evaluate(typXpe, node, XPathConstants.NODE);
//                Node host = (Node) xPath.evaluate(hostXp, node, XPathConstants.NODE);
//
//                Element gundamE = XMLUtils.creatElement(output, "gundam", null, null);
//                Element nameE = XMLUtils.creatElement(output, "name", name.getTextContent(), null);
//                Element detailLinkE = XMLUtils.creatElement(output, "detailLink", detailLink.getTextContent(), null);
//                Element avatarE = XMLUtils.creatElement(output, "avatar", avatar.getTextContent(), null);
//                Element statusE = XMLUtils.creatElement(output, "status", status.getTextContent(), null);
//                Element priceE = XMLUtils.creatElement(output, "price", price.getTextContent(), null);
//                Element typeE = XMLUtils.creatElement(output, "type", type.getTextContent(), null);
//                Element hostE = XMLUtils.creatElement(output, "host", host.getTextContent(), null);
//
//                gundamE.appendChild(nameE);
//                gundamE.appendChild(detailLinkE);
//                gundamE.appendChild(avatarE);
//                gundamE.appendChild(statusE);
//                gundamE.appendChild(priceE);
//                gundamE.appendChild(typeE);
//                gundamE.appendChild(hostE);
//
//                original.item(0).appendChild(gundamE);
//            }//Kêt thúc tạo node
//
//            //Đưa DOM mới vào file xml ouput
//            TransformDOMToFile(new DOMResult(output), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputc3.xml");
//            System.out.println("doc length= " + list.getLength());
//
//        } catch (ParserConfigurationException ex) {
//            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SAXException ex) {
//            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (XPathExpressionException ex) {
//            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (TransformerException ex) {
//            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
//        }
        } catch (XPathExpressionException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

    }

    /**
     * Đưa DOM thành dạng String, sử dụng JAXB marshaller
     *
     * @param lib
     * @return
     */
    public String marshallerToTransferToString(DOMResult result, Class classes) {
        try {
            JAXBContext jaxbc = JAXBContext.newInstance(classes.getClass());
            Marshaller marshal = jaxbc.createMarshaller();
//            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            /**
             * Vì cái dòng JAXB_FORMATTED_OUTPUT đã làm cho nó hiển thị như 1
             * xml ấy
             */
            marshal.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshal.setProperty(Marshaller.JAXB_FRAGMENT, true);
            /**
             * Với JAXB_FRAGMENT ta sẽ loại bỏ đc dòng xml declaration
             */
            //Ta thuc hien them 1 buoc bien doi xml file thang string
            StringWriter sw = new StringWriter();
            marshal.marshal(result, sw);
            System.out.println("Day la String marshall: " + sw.toString());
            return sw.toString();
        } catch (PropertyException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}
