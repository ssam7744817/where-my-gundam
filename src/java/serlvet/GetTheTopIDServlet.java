/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import crawler.utils.XMLUtils;
import daos.GundamDAO;
import daos.TopIdDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *Class có tác dụng lưu những top id, KO DÙNG NỮA
 * 
 * @author HiruK
 */
public class GetTheTopIDServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("id in servlet= " + request.getParameter("idGet"));

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            GundamDAO daoTop = new GundamDAO();
            daoTop.getListHaveViewAndClick();
            
            //kiem tra cai list co bao nhieu phan tu
            System.out.println("top dao= "+daoTop.getJaxbList().getGundam().size());
            
            //Cập nhật nó lại vào trong context scope
            ServletContext context=this.getServletContext();
            context.setAttribute("TESTTOP",daoTop.getJaxbList().getGundam());//Kết thúc việc lấy lên danh sách mới nhất
            System.out.println("ok");
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetTheTopIDServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GetTheTopIDServlet.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            processRequest(request, response);
            String node = request.getParameter("idGet");
            if (node != null || node != "") {
                Document doc = XMLUtils.convertStringToXMLDocument(node);
                if (doc != null) {
                    XPath xp = XMLUtils.getXpath();

                    String expression = "//*[local-name()='item']";
                    String idExp = "*[local-name()='id']";
                    String firstDateExp = "*[local-name()='date']";

                    NodeList list = (NodeList) xp.evaluate(expression, doc, XPathConstants.NODESET);
                    for (int i = 0; i < list.getLength(); i++) {
                        Node currentNode = list.item(i);
                        String id = (String) xp.evaluate(idExp, currentNode, XPathConstants.STRING);
                        String date = (String) xp.evaluate(firstDateExp, currentNode, XPathConstants.STRING);

                        //Thực hiện việc cập nhật thêm những giá trị mới vào trong những gundam
                        System.out.println("this is id= " + list.item(i).getTextContent());
                        System.out.println("this is date= " + date);

                        TopIdDAO dao = new TopIdDAO();
                        dao.saveTopID(id, Date.valueOf(date));
                        //Kết thúc save id vào 
                    }
                }
            }
        } catch (XPathExpressionException ex) {
            Logger.getLogger(GetTheTopIDServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GetTheTopIDServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetTheTopIDServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
