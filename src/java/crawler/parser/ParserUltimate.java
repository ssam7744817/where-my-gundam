/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.parser;

import java.io.FileInputStream;
import java.io.InputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;
import crawler.resolver.UltimateURIResolver;

/**
 *
 * @author HiruK
 */
public class ParserUltimate {
/**
 * Đây chính là con crawl với đầu vào là file xml với đường dẫn trang web cần crawl và 
 * file stylesheet để cấu trúc ra dữ liệu ta muốn
 * @param configPath
 * @param xslPath
 * @return
 * @throws Exception 
 */
    public static DOMResult parser(String configPath, String xslPath) throws Exception {
        StreamSource xslCate = new StreamSource(xslPath);
        InputStream is = new FileInputStream(configPath);

        TransformerFactory factory = TransformerFactory.newInstance();
        DOMResult dOMResult = new DOMResult();
        UltimateURIResolver resolver = new UltimateURIResolver();

        factory.setURIResolver(resolver);
        if (factory.getURIResolver()!=null) {
            Transformer transformer = factory.newTransformer(xslCate);

        transformer.transform(new StreamSource(is), dOMResult);
        return dOMResult;
        }
        return null;
    }
    
}
