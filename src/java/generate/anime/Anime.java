
package generate.anime;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anime complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="anime">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anime_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="gundaminanime" type="{http://xml.netbeans.org/schema/animes}gundaminanime" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "anime", propOrder = {
    "animeName",
    "gundaminanime"
})
public class Anime {

    @XmlElement(name = "anime_name", required = true)
    protected String animeName;
    @XmlElement(required = true)
    protected List<Gundaminanime> gundaminanime;

    /**
     * Gets the value of the animeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnimeName() {
        return animeName;
    }

    /**
     * Sets the value of the animeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnimeName(String value) {
        this.animeName = value;
    }

    /**
     * Gets the value of the gundaminanime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gundaminanime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGundaminanime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Gundaminanime }
     * 
     * 
     */
    public List<Gundaminanime> getGundaminanime() {
        if (gundaminanime == null) {
            gundaminanime = new ArrayList<Gundaminanime>();
        }
        return this.gundaminanime;
    }

}
