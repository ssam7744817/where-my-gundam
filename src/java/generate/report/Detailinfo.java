
package generate.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for detailinfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="detailinfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="gundamName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="detailLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="page" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clickCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="viewCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "detailinfo", propOrder = {
    "id",
    "gundamName",
    "detailLink",
    "price",
    "type",
    "page",
    "clickCount",
    "viewCount"
})
public class Detailinfo {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String gundamName;
    @XmlElement(required = true)
    protected String detailLink;
    @XmlElement(required = true)
    protected String price;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String page;
    @XmlElement(required = true)
    protected String clickCount;
    @XmlElement(required = true)
    protected String viewCount;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the gundamName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGundamName() {
        return gundamName;
    }

    /**
     * Sets the value of the gundamName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGundamName(String value) {
        this.gundamName = value;
    }

    /**
     * Gets the value of the detailLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailLink() {
        return detailLink;
    }

    /**
     * Sets the value of the detailLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailLink(String value) {
        this.detailLink = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice(String value) {
        this.price = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the page property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPage(String value) {
        this.page = value;
    }

    /**
     * Gets the value of the clickCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClickCount() {
        return clickCount;
    }

    /**
     * Sets the value of the clickCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClickCount(String value) {
        this.clickCount = value;
    }

    /**
     * Gets the value of the viewCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewCount() {
        return viewCount;
    }

    /**
     * Sets the value of the viewCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewCount(String value) {
        this.viewCount = value;
    }

}
