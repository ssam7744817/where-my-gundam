/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serlvet;

import crawler.jaxb.JAXBMarshalling;
import crawler.validation.XMLValidate;
import daos.PictureDAO;
import dtos.GundamDTO;
import generate.gundam.Gundam;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;

/**
 *Class có chức năng là khi người dùng click vào tên của mô hình gundam thì nó sẽ hiện chi tiết, KO DÙNG ĐẾN CLASS NÀY NỮA
 * @author HiruK
 */
public class GetDetailImageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String id = request.getParameter("gundamId");
        System.out.println("This is id= " + id);

        HttpSession session = request.getSession();
        List<GundamDTO> list = (List<GundamDTO>) session.getAttribute("SEARCH");
        System.out.println("size= " + list.size());

        PictureDAO dao = new PictureDAO();//Cái này để lấy thêm ảnh gắn vào phần detail

        if (list != null) {
            for (GundamDTO dto : list) {
                if (id.equals(dto.getId())) {
                    try {
                        Gundam gundam = new Gundam();
                        gundam.setId(dto.getId());
                        gundam.setDetailLink(dto.getDetailLink());
                        gundam.setHost(dto.getHostId());
                        gundam.setPrice(dto.getPrice());
                        gundam.setName(dto.getName());
                        //Thực hiện lấy hình chi tiết, ghép vào phần detail và cũng là bước cuối để hoàn thiện tạo xml
                        dao.getPictureListByID(id);

                        System.out.println("Detail already get it");
                        JAXBMarshalling marshalling = new JAXBMarshalling();

                        //Test trước cái đã
                        DOMResult result = marshalling.marshallerToTransferToDOmResutl(dto, Gundam.class);
                        String resultString = marshalling.marshallerToTransferToStringByTransform(dto, Gundam.class);
                        System.out.println("string xml= " + resultString);
                        XMLValidate validate = new XMLValidate();
                        validate.transformDOMToFile(result.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\test.xml");

                    } catch (TransformerException ex) {
                        Logger.getLogger(GetDetailImageServlet.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(GetDetailImageServlet.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(GetDetailImageServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
