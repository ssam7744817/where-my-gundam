/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.jaxb;

import com.sun.codemodel.JCodeModel;
import com.sun.tools.xjc.api.ErrorListener;
import com.sun.tools.xjc.api.S2JJAXBModel;
import com.sun.tools.xjc.api.SchemaCompiler;
import com.sun.tools.xjc.api.XJC;
import java.io.File;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

/**
 *Class này có tác dụng chuyển đổi schema thành java class
 * Và tên các schema đc sử dụng trong project này đc đặt ở WEBINF/schema:
 * anime, gundams
 * Những schema còn lại chỉ dùng cho mục đích testing
 * @author HiruK
 */
public class XJCGenerateJavaObj {

    public void generateJavaObj(String schemaPath) {
        try {
            String output = "src/java";
            SchemaCompiler sc = XJC.createSchemaCompiler();
            sc.setErrorListener(new ErrorListener() {
                @Override
                public void error(SAXParseException saxpe) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void fatalError(SAXParseException saxpe) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void warning(SAXParseException saxpe) {
                    saxpe.printStackTrace();
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void info(SAXParseException saxpe) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
            sc.forcePackageName("generate.report");
            File schema = new File(schemaPath);
            InputSource is = new InputSource(schema.toURI().toString());
            sc.parseSchema(is);
            S2JJAXBModel model = sc.bind();
            JCodeModel code = model.generateCode(null, null);
            code.build(new File(output));
            System.out.println("Finish");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        XJCGenerateJavaObj xJCGenerateJavaObj=new XJCGenerateJavaObj();
//        xJCGenerateJavaObj.generateJavaObj("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\schema\\anime.xsd");
        xJCGenerateJavaObj.generateJavaObj("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\schema\\topgundam.xsd");
    }
}
