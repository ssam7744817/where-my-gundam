/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outputenum;

/**
 *
 * @author HiruK
 */
public enum HostEnum {
    YMS("http://ymsgundam.com/"),
    C3("https://c3gundam.com/"),
    K2T("http://k2tgundam.com/");
    private String output;

    private HostEnum(final String value) {
        this.output = value;
    }

    //toString
    public String toString() {
        return output;
    }
}
