/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import generate.gundam.Gundam;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HiruK
 */
public class PictureDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    List<String> list=new ArrayList<String>();

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }

    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    /**
     * Hàm này sẽ lấy ra 1 danh sách cách hình ảnh dựa trên id của mô hình
     * gundam
     *
     * @param id
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void getPictureListByID(String id) throws SQLException, ClassNotFoundException {
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT [picture]\n"
                    + "  FROM [dbo].[picture]\n"
                    + "  where id=?";
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            rs = preStm.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("picture"));
            }
        } finally {
            closeConnection();
        }
    }
}
