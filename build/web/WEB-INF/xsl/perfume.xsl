<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : perfume.xsl
    Created on : June 5, 2020, 11:19 PM
    Author     : PC
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:t="http://xml.netbeans.org/schema/perfumes"
                xmlns="http://xml.netbeans.org/schema/perfumes"
                version="1.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="no"/>

    <xsl:template match="t:perfume">
        <xsl:element name="perfumes">
            <xsl:variable name="listDoc" select="document(t:link//@hr)"/>
            <xsl:variable name="host" select="t:link//@host"/>
            <xsl:variable name="list" select="$listDoc//*[@id='main-content']/div[1]/div[1]/div/div[4]/div/div[1]/div/div"/>
            <xsl:variable name="link" select="$list//div[@class='flex-child-auto']//a"/>
            <!--//*[@id="main-content"]/div[1]/div[1]/div/div[4]/div/div[1]/div/div[3]-->
            <xsl:for-each select="$link">
                <xsl:element name="perfume">
                    <xsl:value-of select="@href"/>
                </xsl:element> 
                <xsl:call-template name="getDetail">
                    <xsl:with-param name="host" select="$host"/>
                    <xsl:with-param name="detailLink" select="@href"/>
                </xsl:call-template>
                <!--Đã lấy đc từng link chi tiết-->
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    <xsl:template name="getDetail">
        <xsl:param name="host"/>
        <xsl:param name="detailLink"/>
        <xsl:variable name="detail" select="document(concat($host,$detailLink))"/>
        <xsl:variable name="justLink" select="concat($host,$detailLink)"/>
        <xsl:element name="DetailLink">
            <xsl:value-of select="$justLink"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
