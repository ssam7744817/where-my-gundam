/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import crawler.jaxb.JAXBUnmarshallingGundams;
import generate.gundams.Gundam;
import generate.gundams.Gundams;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;

/**
 *
 * @author HiruK
 */
public class RemoveDuplicate {

    // Function to remove duplicates from an ArrayList 
    public static void removeDuplicates(List<Gundam> list) {
        System.out.println("Before remove= " + list.size());

        //Dùng iterator để mình có thể modify cái collection này
        for (Iterator l = list.iterator(); l.hasNext();) {
            Gundam left = (Gundam) l.next();
            if (left.getName().equals("RG 1/144 Unicorn Full Armor")) {
                l.remove();
            }
        }

        // return the new list 
        System.out.println("After remove= " + list.size());
    }

    public static void main(String[] args) {
        try {
            crawler.jaxb.JAXBUnmarshallingGundams jaxb = new JAXBUnmarshallingGundams();

            Gundams gundams = jaxb.creatGundamObject("E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputk2t.xml", "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\schema\\gundams.xsd");

//            validate.transformDOMToFile(rs.getNode(), "E:\\XML_jaxb_bo_o_day\\GundamPriceCompare\\web\\WEB-INF\\xml\\outputk2t.xml");
            RemoveDuplicate.removeDuplicates(gundams.getGundam());
        } catch (JAXBException ex) {
            Logger.getLogger(RemoveDuplicate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(RemoveDuplicate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
