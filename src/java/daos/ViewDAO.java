/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import conn.MyConnection;
import dtos.ViewDTO;
import generate.report.Report;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author HiruK
 */
public class ViewDAO implements Serializable {

    Connection conn;
    PreparedStatement preStm;
    ResultSet rs;
    HashMap<ViewDTO, Integer> list = new HashMap<ViewDTO, Integer>();
    

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public HashMap<ViewDTO, Integer> getList() {
        return list;
    }

    public void setList(HashMap<ViewDTO, Integer> list) {
        this.list = list;
    }

    /**
     * Lưu lại view khi click vào anime và đi đến trang bán gundam ở Việt Nam
     *
     * @param id
     * @param type
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void saveView(String id, String type, String gundamHost) throws SQLException, ClassNotFoundException {
        try {
            System.out.println("type= " + type);
            conn = MyConnection.getConnection();
            String sql = "";
            if (type.equals("anime")) {
                sql = "INSERT INTO [dbo].[viewpage]\n"
                        + "           ([animeid]\n"
                        + "           )\n"
                        + "     VALUES (?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, id);
            } else {
                sql = "INSERT INTO [dbo].[viewpage]\n"
                        + "           (\n"
                        + "           [gundaminshopid]\n"
                        + "           ,[host])\n"
                        + "     VALUES(?,?)";
                preStm = conn.prepareStatement(sql);
                preStm.setString(1, id);
                preStm.setString(2, gundamHost);
            }
//            preStm = conn.prepareStatement(sql);
//            preStm.setString(1, id);
//            preStm.setString(2, id);
            preStm.executeUpdate();
            System.out.println("insert view ok");
        } finally {
            closeConnection();
        }
    }

    /**
     * Lấy ra thống kê lượt view trên từng page
     */
    public void getViewOnEachPage() throws ClassNotFoundException, SQLException {
        ViewDTO dto;
        try {
            conn = MyConnection.getConnection();
            String sql = "SELECT viewpage.host, count(viewpage.host) as hostcount\n"
                    + "  FROM [dbo].[viewpage]\n"
                    + "  where viewpage.host is not null\n"
                    + "  group by viewpage.host\n"
                    + "  order by hostcount desc";
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                dto = new ViewDTO();
                dto.setHost(rs.getInt("host"));
                list.put(dto, rs.getInt("hostcount"));
            }
            System.out.println("view in host= "+list.size());
        } finally {
            closeConnection();
        }
    }
}
