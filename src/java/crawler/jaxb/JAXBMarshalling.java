/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.jaxb;

import java.io.OutputStream;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.transform.dom.DOMResult;

/**
 *
 * @author HiruK
 */
public class JAXBMarshalling {

    /**
     * Đưa DOM thành dạng String, sử dụng transfomer hay còn gọi là Trax
     *
     * @param lib
     * @return
     */
    public String marshallerToTransferToStringByTransform(Object ob, Class type) {
        try {
            JAXBContext jaxbc = JAXBContext.newInstance(type);
            Marshaller marshaller = jaxbc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            

            StringWriter sw = new StringWriter();//Đây sẽ là nơi lữu trữ đoạn string

            marshaller.marshal(ob, sw);//xuất đầu ra cây DOM và lưu trữ dưỡi dạng String ở String writter

            //Ta thuc hien them 1 buoc bien doi xml file thang string
            return sw.toString();
        } catch (JAXBException ex) {
            Logger.getLogger(JAXBMarshalling.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Convert để render giao diện thì éo cần class, rất tiếc là ko thể làm đc vì jaxb hoạt động để convert qua lại xml và object
     * ko có class thì ko làm g đc, nếu muốn làm vậy thì dùng TRax là transformer Factory
     * @param ob
     * @param type
     * @return 
     */
    public String marshallerToTransferToStringByTransformNoClass(Object ob) {
        try {
            JAXBContext jaxbc = JAXBContext.newInstance();
            Marshaller marshaller = jaxbc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

            StringWriter sw = new StringWriter();//Đây sẽ là nơi lữu trữ đoạn string

            marshaller.marshal(ob, sw);//xuất đầu ra cây DOM và lưu trữ dưỡi dạng String ở String writter

            //Ta thuc hien them 1 buoc bien doi xml file thang string
            return sw.toString();
        } catch (JAXBException ex) {
            Logger.getLogger(JAXBMarshalling.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * CHuyển java object thành dom result
     *
     * @param ob
     * @param type
     * @return
     */
    public DOMResult marshallerToTransferToDOmResutl(Object ob, Class type) {
        DOMResult result = new DOMResult();
        try {
            JAXBContext jaxbc = JAXBContext.newInstance(type);
            Marshaller marshaller = jaxbc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(ob, result);
        } catch (PropertyException ex) {
            Logger.getLogger(JAXBMarshalling.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(JAXBMarshalling.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Chuyển đôi java object sang xml outputStream
     *
     * @param ob
     * @param type
     * @param out
     * @throws PropertyException
     * @throws JAXBException
     * @throws JAXBException
     */
    public void marshallerToOutputStream(Object ob, Class type, OutputStream out) throws PropertyException, JAXBException, JAXBException {
        JAXBContext jaxbc = JAXBContext.newInstance(type);
        Marshaller marshaller = jaxbc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.marshal(ob, out);
    }
}
