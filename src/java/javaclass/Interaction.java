/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import java.io.Serializable;

/**
 *
 * @author HiruK
 */
public class Interaction implements Serializable{
    private int view,click;

    public Interaction() {
    }

    public Interaction(int view, int click) {
        this.view = view;
        this.click = click;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }
    
}
