/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclass;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author HiruK
 */
public class UniqueID {

    private static AtomicLong idCounter = new AtomicLong();

    public static String createID() {
        return String.valueOf(idCounter.getAndIncrement());
    }
    public static void main(String[] args) {
        
        System.out.println("unique id"+UUID.randomUUID().toString());
    }

}
