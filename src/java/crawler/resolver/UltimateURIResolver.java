/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler.resolver;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;
import crawler.statemachinewellform.XmlSyntaxChecker;
import crawler.utils.HttpUtils;
import crawler.utils.FileUtils;

/**
 * Tiền xử lí dữ liệu trước khi đưa vào bộ paser, nói tóm lại là well-form
 *
 * @author HiruK
 */
public class UltimateURIResolver implements URIResolver {

    @Override
    public Source resolve(String href, String base) throws TransformerException {
        System.out.println("fref= " + href);
//        if (href != null && (href.indexOf("https://c3gundam.com/") == 0 || href.indexOf("http://k2tgundam.com/") == 0 || href.indexOf("http://ymsgundam.com/") == 0 || href.indexOf("https://www.1999.co.jp/") == 0)) {
//            System.out.println("Đã có hàng");
            try {
                String content = HttpUtils.getHttpContent(href);
                XmlSyntaxChecker checker = new XmlSyntaxChecker();
                if (!content.isEmpty()) {
                    content = checker.refineHtml(content);
                    content = checker.check(content);
                    System.out.println("");
                    System.out.println("Sau khi cắt: " + content);
//                    FileUtils.readTextContent(href);
                    InputStream stream = new ByteArrayInputStream(content.toString().getBytes(StandardCharsets.UTF_8));
                    return new StreamSource(stream);
                } else {
                    System.out.println("Ko có g cả");
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
//        } else {
//            System.out.println("Null rồi");
//        }
        return null;
    }

}
