/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;

/**
 *
 * @author HiruK
 */
public class ViewDTO implements Serializable{
    private String animeId,gundaminshopId;
    private int host;

    public ViewDTO() {
    }

    public ViewDTO(String animeId, String gundaminshopId) {
        this.animeId = animeId;
        this.gundaminshopId = gundaminshopId;
    }

    public int getHost() {
        return host;
    }

    public void setHost(int host) {
        this.host = host;
    }

    public String getAnimeId() {
        return animeId;
    }

    public void setAnimeId(String animeId) {
        this.animeId = animeId;
    }

    public String getGundaminshopId() {
        return gundaminshopId;
    }

    public void setGundaminshopId(String gundaminshopId) {
        this.gundaminshopId = gundaminshopId;
    }
    
    
}
